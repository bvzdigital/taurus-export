;(function(window, $, undefined) {
    'use strict';
    
    $('.item-question .icon').click(function(e){
        var elementClass = e.target.classList;
        if(elementClass.contains("down")){
            elementClass.remove("down");
            elementClass.add("up");
        }else{
            elementClass.remove("up");
            elementClass.add("down");
        }
        $(e.target.parentNode.parentNode).find(".item-answer").slideToggle();
    });

    $(".question-category").click(function() {
        $('.question-category.active').removeClass("active");
        $(this).addClass("active");
        var questionType = $(this).attr('data-type');

        $(".qNa-wrapper").each(function() {
            if ( $(this).attr("data-category") != questionType ) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });
    $(".question-category[data-type='General']").trigger("click");

    // CSSMap;
    $("#map-continents").CSSMap({
        "size": 850,
        "onClick": function(listItem) {
            var filtro = $(listItem).find("a").attr('data-filter');
            var filtroF = filtro.replace(".", "");
            var attrid = $(listItem).find("a").attr('href');
            attrid = '.'+attrid.replace("#", "");
            $('.item-active').removeClass('item-active');
            $('.active').removeClass('active');
            $(".distributors-nav > a"+attrid).trigger("click");
            reOrderCountrySelector(filtroF);
        },
    });
    // END OF THE CSSMap;

    $("#country-selector").on("change", function() {
        var selectedCountry = $(this).find(':selected').val();
        if(selectedCountry != 0) {
            $(".mix.box-distributors").each(function() {
                if ( $(this).attr("data-country") != selectedCountry ) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        } else {
            $(".mix.box-distributors").each(function() {
                $(this).show();
            });
        }
    });

    $("body > div.page-wrapper.main-content > div > form").submit(function(e) {
        e.preventDefault();
    });

    function validatecaptcha(block) {
        if (block.find('.captcha-checkbox-label').children("input[type=checkbox]").is(':checked')) {
            return true;
        }                

        return false;
    }

    var label = 'I am not a robot';
    var html = '<label id="captcha-checkbox" class="captcha-checkbox-label"><input type="checkbox" /><span></span><div>' + label + '</div></label>';

    $("body > div.page-wrapper.main-content > div > form > fieldset.contact-form__fieldset.contact-form__fieldset--full > label > textarea").after(html);
    $("#newsletter-formulary > fieldset > label:nth-child(2) > input").after(html);

    Module('TAURUS.globals', function(globals) {
        globals.$body = $('body');
        globals.$window = $(window);
        globals.noop = function() {};
    }, {});

    Module('TAURUS.utils', function(utils) {
        utils.scrollTo = function($el) {
            $('html, body').animate({
                scrollTop: $el.offset().top
            });
        };
    }, {});

    Module('TAURUS.externalLink', function(externalLink) {
        externalLink.init = function() {
            TAURUS.globals.$body.on('click', '[rel="external"]', function(e) {
                e.preventDefault();
                window.open($(this).attr('href'));
            });
        };
    }, {});

    Module('TAURUS.CustomSelect', function(CustomSelect) {
        CustomSelect.fn.initialize = function($el) {
            this.$el = $el;

            this.$el.addClass('custom-select--real-select');

            this.$wrap = this.$el.wrap('<div class="custom-select">').parent();
            this.$selectedArea = $('<div class="custom-select__selected">').
                appendTo(this.$wrap);

            this.$selectedArea.append(this.$el.find('option:selected').html());
            if(this.$el.val()) {
                this.$selectedArea.
                    addClass('custom-select__selected--has-value');
            }

            this.addEvents();
        };

        CustomSelect.fn.addEvents = function() {
            var that = this;

            this.$el.on('change', function() {
                that.$selectedArea.
                    removeClass('custom-select__selected--has-value').
                    html(that.$el.find('option:selected').html());

                if($(this).val()) {
                    that.$selectedArea.
                        addClass('custom-select__selected--has-value');
                }
            });
        };
    });

    Module('TAURUS.CustomFileInput', function(CustomFileInput) {

        CustomFileInput.fn.initialize = function($el) {
            this.$el = $el;
            this.noFileText = $el.data('no-file-selected');
        };

        CustomFileInput.fn.init = function() {
            this.getWrap();
            this.createFileInfo();
            this.setFileName();
            this.addEventListeners();

            return this;
        };

        CustomFileInput.fn.getWrap = function() {
            this.$wrap = this.$el.parent();
        };

        CustomFileInput.fn.createFileInfo = function() {
            this.$fileInfo = $('<div class="custom-file-input__info"></div>');
            this.$wrap.append(this.$fileInfo);
        };

        CustomFileInput.fn.setFileName = function() {
            var fileName = this.$el.val().replace(/^.*[\\\/]/, '');

            this.$fileInfo.html(fileName || this.noFileText);
        };

        CustomFileInput.fn._onFileChange = function() {
            this.setFileName();
        };

        CustomFileInput.fn.addEventListeners = function() {
            this.$el.on('change', _.bind(this._onFileChange, this));
        };

        CustomFileInput.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };
    });

    Module('TAURUS.Overlay', function(Overlay) {

        Overlay.fn.initialize = function($parent) {
            this.$parent = $parent || TAURUS.globals.$body;
        };

        Overlay.fn.init = function() {
            this.createOverlay();

            return this;
        };

        Overlay.fn.createOverlay = function() {
            this.$overlay = $('<div class="overlay">').appendTo(this.$parent);
        };

        Overlay.fn.show = function() {
            this.$overlay.fadeIn();
        };

        Overlay.fn.hide = function() {
            this.$overlay.fadeOut();
        };

        Overlay.fn.toggle = function() {
            this.$overlay.fadeToggle();
        };

        Overlay.fn.addClass = function(classList) {
            this.$overlay.addClass(classList);
        };

        Overlay.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.Spinner', function(Spinner) {

        Spinner.fn.initialize = function($parent) {
            this.$parent = $parent || TAURUS.globals.$body;
        };

        Spinner.fn.init = function() {
            this.createSpinner();

            return this;
        };

        Spinner.fn.createSpinner = function() {
            this.$spinner = $(
                '<div class="spinner">' +
                    '<div class="double-bounce1"></div>' +
                    '<div class="double-bounce2"></div>' +
                '</div>'
            ).appendTo(this.$parent);
        };

        Spinner.fn.show = function() {
            this.$spinner.fadeIn();
        };

        Spinner.fn.hide = function() {
            this.$spinner.fadeOut();
        };

        Spinner.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.PictureSlider', function(PictureSlider) {

        PictureSlider.fn.initialize = function($el) {
            this.$el = $el;
            this.width = this.$el.data('width');
            this.$children = this.$el.children();
            this.length = this.$children.length;
            this.animating = false;
        };

        PictureSlider.fn.setWidth = function() {
            this.$el.width(this.width);
        };

        PictureSlider.fn.createPaginationPanel = function() {
            var $pagination = $('<div class="picture-slider__pagination">'),
                paginationHTML = '',
                i = 0;

            for( ; i < this.length; i += 1) {
                paginationHTML += '<div class="picture-slider__pagination__item"></div>';
            }

            $pagination.
                width(this.$el.data('pagination-width')).
                html(paginationHTML).
                appendTo(this.$el).
                children().
                filter(':eq(0)').
                addClass('picture-slider__pagination__item--active');

            this.$pagination = $pagination;
        };

        PictureSlider.fn.addEventListeners = function() {
            var self = this;

            if(this.length > 1) {
                this.$pagination.on(
                    'click',
                    '.picture-slider__pagination__item',
                    function() {
                        self.moveTo($(this).index());
                    }
                );
            }
        };

        PictureSlider.fn.moveTo = function(index) {
            var self = this;

            if(!this.animating) {
                this.animating = true;
            } else {
                return false;
            }

            this.$pagination.
                children().
                removeClass('picture-slider__pagination__item--active').
                filter(':eq(' + index + ')').
                addClass('picture-slider__pagination__item--active');

            this.$wrap.fadeOut(function() {
                self.$wrap.css('text-indent', -index * self.width);
                self.$wrap.fadeIn(function() {
                    self.animating = false;
                });
            });
        };

        PictureSlider.fn.wrap = function() {
            this.$wrap = this.$el.wrapInner('<div>').
                children().filter(':eq(0)');
        };

        PictureSlider.fn.init = function() {
            this.wrap();
            this.setWidth();
            if(this.length > 1) {
                this.createPaginationPanel();
            }
            this.addEventListeners();
            return this;
        };

        PictureSlider.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.Form', function(Form) {

        Form.fn.initialize = function($el) {
            this.$el = $el;
            this.overlay = TAURUS.Overlay.create();
            this.spinner = TAURUS.Spinner.create();
        };

        Form.fn.init = function() {
            this.createMessageContainer();
            this.addEventListeners();
        };

        Form.fn.createMessageContainer = function() {
            this.$messageContainer = $(
                '<div class="default-form__message">' +
                    '<div class="table">' +
                        '<div class="table__cell table__cell--v-middle">' +
                        '</div>' +
                    '</div>' +
                '</div>'
            ).appendTo(this.$el);
            this.$message = this.$messageContainer.find('.table__cell');
        };

        Form.fn.addEventListeners = function() {
            this.$el.on('submit', _.bind(this._onSubmit, this))
                .on('click',
                    '.js-form-clear-message',
                    _.bind(this._onClearMessageClick, this)
                ).
                on('click',
                    '.js-form-reset-fields',
                    _.bind(this._onResetFieldsClick, this)
                );
        };

        Form.fn._onClearMessageClick = function(e) {
            e.preventDefault();
            this.$messageContainer.fadeOut();
        };

        Form.fn._onResetFieldsClick = function(e) {
            e.preventDefault();
            $('<button type="reset">').appendTo(this.$el).
                trigger('click').remove();
            this.$el.find('select').trigger('change');
        };

        Form.fn.showLoading = function() {
            this.overlay.show();
            this.spinner.show();
        };

        Form.fn.hideLoading = function() {
            this.overlay.hide();
            this.spinner.hide();
        };

        Form.fn._onSubmit = function() {
            if(this.validate() == false) {
                this.showLoading();
                this.send.call(
                    this,
                    _.bind(this.handleSendResponse, this)
                );
            } else {
                TAURUS.utils.scrollTo($('.js-validate-required:first'));
            }

            return false;
        };

        Form.fn.validate = function() {
            var $inputs = this.$el.find(':input:not(:button)'),
                $required = $inputs.filter('.js-validate-required'),
                hasErrors = false;

            var captchaSpan = '#captcha-checkbox > span';

            $inputs.removeClass('input-error');
            $(captchaSpan).removeClass('input-error');

            $required.each(function() {
                var $input = $(this);

                if(!$input.val()) {
                    $input.addClass('input-error');
                    hasErrors = true;
                }
            });

            if(!validatecaptcha(this.$el)) {
                $(captchaSpan).addClass('input-error');
                hasErrors = true;
            }

            return hasErrors;
        };

        Form.fn.send = function(cb) {
            var self = this,
                successFn = typeof cb === 'function' ? cb : TAURUS.globals.noop;

            this.$el.ajaxSubmit({
                url: this.$el.attr('data-url-action'),
                type: this.$el.attr('method') || 'post',
                dataType: 'json',
                success: successFn
            });
        };

        Form.fn.showMessage = function(message) {
            this.$message.html(message);
            this.$messageContainer.fadeIn();
        };

        Form.fn.hideMessage = function() {
            this.$messageContainer.fadeOut();
        };

        Form.fn.handleSendResponse = function(data) {
            this.showMessage(data.mensagem);
            this.hideLoading();
        };

        Form.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.FilterContent', function(FilterContent) {

        FilterContent.fn.initialize = function($el) {
            this.$el = $el;
            this.$filterSummary = $el.find('.js-filter-summary');
            this.filterSummaryTemplate = _.template(
                $el.find('.js-filter-summary-template').html()
            );
            this.$target = $($el.data('target'));
            this.marginRight = this.$target.
                filter(':eq(0)').css('margin-right');
            this.itemsToShow = $el.data('items-to-show');
        };

        FilterContent.fn.init = function() {
            this.addEventListeners();
        };

        FilterContent.fn.getSummaryJSON = function() {
            var summary = {};

            this.$el.find(':input:not(:button)').each(function() {
                var $input = $(this),
                    val;

                val = $input.val();

                if($input.prop('tagName') === 'SELECT') {
                    if(val) {
                        val = $input.find('option:selected').html();
                    }
                }

                summary[$input.attr('name')] = val;
            });

            return summary;
        };

        FilterContent.fn.updateSummary = function() {
            this.$filterSummary.html(
                this.filterSummaryTemplate(this.getSummaryJSON())
            );
        };

        FilterContent.fn.addEventListeners = function() {
            var self = this;

            this.$el.on(
                'change',
                ':input:not(:button)',
                function() {
                    self.filter();
                    self.updateSummary();
                }
            ).on('content-updated', function() {
                self.$target = $(self.$el.data('target'));
                self.filter();
            });
        };

        FilterContent.fn.getQuery = function() {
            var parts = [],
                $inputs = this.$el.find(':input:not(:button)');

            $inputs.each(function() {
                var val = $(this).val();

                if(val) {
                    parts.push('.' + $(this).val());
                }
            });

            return parts.join('');
        };

        FilterContent.fn.filter = function() {
            var q = this.getQuery(),
                self = this,
                $hide,
                $show,
                cb;

            $hide = this.$target.filter(':not(' + q + ')');
            $show = q ? this.$target.filter(q) : this.$target;

            cb = function() {
                var $marginAdjust;

                self.$target.css('margin-right', 0);
                $show.filter(function(index) {
                    return index % self.itemsToShow
                }).css('margin-right', self.marginRight);

                $show.fadeIn();
            };

            if($hide.length) {
                $.when($hide.fadeOut()).done(_.bind(cb, this));
            } else {
                cb();
            }

        };

        FilterContent.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.LoadMore', function(LoadMore) {

        LoadMore.fn.initialize = function($el) {
            this.$el = $el;
            this.$loadMoreContent = $el.find('.js-load-more-content');
            this.$loadMoreButton = $el.find('.js-load-more-button');
            this.overlay = new TAURUS.Overlay();
            this.spinner = new TAURUS.Spinner();
            this.cache = {};
            this.loadedURLs = [];
            this.currentURLUid = 0;
        };

        LoadMore.fn.getTemplate = function() {
            this.template = _.template(
                this.$el.find('.js-load-more-template').html()
            );
        };

        LoadMore.fn.init = function() {
            this.getTemplate();
            this.addEventListeners();
            this.overlay.init();
            this.spinner.init();
        };

        LoadMore.fn.doRequest = function(url) {
            var self = this;

            this.overlay.show();
            this.spinner.show();
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post'
            }).done(
                _.bind(this.handleRequestResponse, this, url, function(data) {
                    self.overlay.hide();
                    self.spinner.hide();
                    if(parseInt(data.next) !== -1) {
                        $('.js-load-more-button').attr('href', data.next);
                    } else {
                        $('.js-load-more-button').attr('disabled', 'disabled');
                    }
                })
            );
        };

        LoadMore.fn.handleRequestResponse = function(url, cb, data) {
            var downloadsHTML = '',
                self = this,
                $downloadsHTML;

            downloadsHTML += self.template({
                data: data
            });

            $downloadsHTML = $(downloadsHTML);

            this.cache[url] = $downloadsHTML;
            this.loadedURLs.push(url);
            this.addContent($downloadsHTML);

            if(data.next) {
                self.currentURLUid += 1;
                history.pushState && history.pushState({
                    id: self.currentURLUid
                }, "", url);
            }

            if(typeof cb === 'function') {
                cb(data);
            }
        };

        LoadMore.fn.addContent = function(html) {
            this.$loadMoreContent.append(html).trigger('content-updated');
        };

        LoadMore.fn.addEventListeners = function() {
            var self = this;

            this.$el.on('click', '.js-load-more-button', function(e) {
                var url = $(this).attr('href');

                if($(this).attr('disabled')) {
                    return false;
                }

                e.preventDefault();

                self.doRequest(url);
            });

            TAURUS.globals.$window.on('popstate', function(evt) {
                var url = location.href,
                    state = evt.originalEvent.state,
                    $html = self.cache[self.loadedURLs.pop()];

                if(/#/g.test(url)) {
                    return false;
                }

                if(!state || state.id < self.currentURLUid) { // 'back'
                    if(!$html) {
                        location.href = url;
                    }
                    $html.hide();
                    self.currentURLUid -= 1;
                } else {
                    $html = self.cache[url];
                    if(!$html) {
                        self.doRequest(url);
                    } else {
                        $html.show();
                    }
                }
            });
        };

        LoadMore.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.FooterNav', function(FooterNav) {

        FooterNav.fn.initialize = function($el) {
            this.$el = $el;
            this.$mainNav = this.$el.find('.js-footer-nav__main-nav');
            this.$toggle = this.$el.find('.js-footer-nav__toggle');
        };

        FooterNav.fn._onNavToggleClick = function(e) {
            this.toggleNav(e);
            this.toggleNavTrigger(e);
        };

        FooterNav.fn.addEventListeners = function() {
            this.$el.on(
                'click',
                '.js-footer-nav__toggle',
                _.bind(this._onNavToggleClick, this)
            );
        };

        FooterNav.fn.toggleNav = function(e) {
            e.preventDefault();
            this.$mainNav.slideToggle(function() {
                TAURUS.utils.scrollTo($(this));
            });
        };

        FooterNav.fn.toggleNavTrigger = function() {
            this.$toggle.each(function() {
                var $self = $(this),
                    oldText = $self.html();

                $self.toggleClass($self.data('toggle-class'));
                $self.html($self.data('toggle-text'));
                $self.data('toggle-text', oldText);
            });
        };

        FooterNav.fn.init = function() {
            this.addEventListeners();
        };

        FooterNav.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.Modal', function(Modal) {

        Modal.fn.initialize = function($parent) {
            this.$parent = $parent || TAURUS.globals.$body;
            this.overlay = new TAURUS.Overlay(this.$parent);
            this.$modal = this.createModal();
            this.$modalContent = this.$modal.find('.js-modal-content');
        };

        Modal.fn.setContent = function($content) {
            this.$modalContent.html($content);

            return this;
        };

        Modal.fn.init = function() {
            this.overlay.init();
            this.$modal.appendTo(this.$parent);
            this.addEventListeners();

            return this;
        };

        Modal.fn.addEventListeners = function() {
            this.$modal.on('click', '.js-modal-close', _.bind(this.hide, this));
        };

        Modal.fn.show = function() {
            this.overlay.show();
            this.$modal.fadeIn();
        };

        Modal.fn.hide = function() {
            this.overlay.hide();
            this.$modal.fadeOut();
        };

        Modal.fn.createModal = function() {
            return $(
                '<div class="modal-wrapper">' +
                    '<div class="modal">' +
                        '<div class="table table--fixed table--full">' +
                            '<div class="table__cell table__cell--v-middle">' +
                                '<div class="modal__close js-modal-close">X</div>' +
                                '<div class="js-modal-content"></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            ).hide();
        };

        Modal.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.VideoList', function(VideoList) {

        VideoList.fn.initialize = function($el) {
            this.$el = $el;
            this.modal = new TAURUS.Modal();
        };

        VideoList.fn.init = function() {
            this.modal.init();
            this.addEventListeners();
        };

        VideoList.fn.openVideo = function(href) {
            var videoId = /\?v=([^\?]*)/g.exec(href)[1];

            this.modal.setContent(
                '<iframe width="940" height="500" src="//www.youtube.com/embed/' + videoId + '" frameborder="0" allowfullscreen></iframe>'
            ).show();
        };

        VideoList.fn.addEventListeners = function() {
            var self = this;

            this.$el.on('click', '.js-video-list__item', function(e) {
                e.preventDefault();
                self.openVideo($(this).attr('href'));
            });
        };

        VideoList.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.ContentBannerGallery', function(ContentBannerGallery) {

        ContentBannerGallery.fn.initialize = function($el) {
            this.$el = $el;
            this.$children = this.$el.children();
            this.length = this.$children.length;
            this.current = 0;
            this.animating = false;
            this.rotate = this.$el.data('rotate');
            this.rotateInterval = 5000;
        };

        ContentBannerGallery.fn.init = function() {
            this.hideAll();
            this.showFirst();
            if(this.length > 1) {
                this.addNavigation();
            }
            if(this.rotate) {
                this.addRotation();
            }
            this.addEventListeners();
        };

        ContentBannerGallery.fn.addRotation = function() {
            var self = this;

            if(this.timeout) {
                clearTimeout(this.timeout);
            }

            this.timeout = setTimeout(function() {
                self.showNext();
                self.addRotation();
            }, this.rotateInterval);
        };

        ContentBannerGallery.fn.hideAll = function() {
            this.$children.hide();
        };

        ContentBannerGallery.fn.showFirst = function() {
            this.$children.filter(':eq(0)').show();
        };

        ContentBannerGallery.fn.addNavigation = function() {
            var navigationHTML =
                    '<div class="arrow-navigation arrow-navigation--bottom-left">' +
                        '<svg width="30" height="30" viewBox="0 0 20 30">' +
                            '<polyline ' +
                                'class="js-content-banner-gallery__prev"' +
                                'points="15 1, 1 14, 15 27"></polyline>' +
                        '</svg>' +
                        '<svg width="30" height="30" viewBox="0 0 20 30">' +
                            '<polyline ' +
                                'class="js-content-banner-gallery__next"' +
                                'points="1 1, 15 14, 1 27"></polyline>' +
                        '</svg>' +
                    '</div>';

            this.$el.append(navigationHTML);
        };

        ContentBannerGallery.fn.addEventListeners = function() {
            var self = this;

            this.$el.
                on(
                    'click',
                    '.js-content-banner-gallery__prev',
                    _.bind(this.showPrev, this)).
                on(
                    'click',
                    '.js-content-banner-gallery__next',
                    _.bind(this.showNext, this)).
                on('mouseenter', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    clearTimeout(self.timeout);
                }).
                on('mouseleave', function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    self.addRotation();
                });
        };

        ContentBannerGallery.fn.showPrev = function() {
            var oldCurrent = this.current;

            if(this.animating) {
                return;
            }

            if(!this.current) {
                this.current = this.length - 1;
            } else {
                this.current -= 1;
            }
            this.show(oldCurrent, this.current);
        };

        ContentBannerGallery.fn.showNext = function() {
            var oldCurrent = this.current;
            if(this.animating) {
                return;
            }

            this.current = (this.current + 1) % this.length;
            this.show(oldCurrent, this.current);
        };

        ContentBannerGallery.fn.show = function(oldIndex, index) {
            var $hide = this.$children.filter(':eq(' + oldIndex + ')'),
                $show = this.$children.filter(':eq(' + index + ')'),
                self = this;

            this.animating = true;

            $hide.fadeOut(function() {
                $show.fadeIn(function() {
                    self.animating = false;
                });
            });
        };

        ContentBannerGallery.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.BusinessBoxGallery', function(BusinessBoxGallery) {

        BusinessBoxGallery.fn.initialize = function($el) {
            this.$el = $el;
            this.$children = this.$el.children();
            this.current = 0;
            this.length = this.$children.length;
            this.animating = false;
            this.galleryURL = this.$el.data('gallery-url');
        };

        BusinessBoxGallery.fn.init = function() {
            this.addNavigation();
            this.addEventListeners();
            this.openSelected();
        };

        BusinessBoxGallery.fn.openSelected = function() {
            var currentSlug = $('#current_slug').val();

            if(currentSlug) {
                $('#negocio-' + currentSlug).trigger('open');
            }
        };

        BusinessBoxGallery.fn.addEventListeners = function() {
            var self = this;

            this.$el.parent().
                on(
                    'click',
                    '.js-business-box-gallery__prev',
                    _.bind(this.showPrev, this)
                ).
                on(
                    'click',
                    '.js-business-box-gallery__next',
                    _.bind(this.showNext, this)
                ).
                on(
                    'business-box:opened',
                    function(e, index) {
                        self.animating = false;
                        self.current = index;
                    }
                ).
                on(
                    'business-box:opening',
                    function() {
                        self.$navigation.fadeIn();
                    }
                ).
                on(
                    'business-box:closing',
                    function() {
                        self.$navigation.fadeOut();
                        history.replaceState && history.replaceState({}, {},
                            self.galleryURL);
                    }
                );
        };

        BusinessBoxGallery.fn.showPrev = function() {
            var oldCurrent = this.current;
            if(this.animating) {
                return false;
            }

            if(!this.current) {
                this.current = this.length - 1;
            } else {
                this.current -= 1;
            }

            this.show(oldCurrent, this.current);
        };

        BusinessBoxGallery.fn.showNext = function() {
            var oldCurrent = this.current;

            if(this.animating) {
                return false;
            }

            this.current = (this.current + 1) % this.length;

            this.show(oldCurrent, this.current);
        };

        BusinessBoxGallery.fn.show = function(oldIndex, index) {
            var $hide = this.$children.filter(':eq(' + oldIndex + ')'),
                $show = this.$children.filter(':eq(' + index + ')');

            this.animating = true;

            $hide.trigger('close');
            $show.trigger('open');
        };

        BusinessBoxGallery.fn.addNavigation = function() {
            var navigationHTML =
                '<div class="arrow-navigation arrow-navigation--bottom-right">' +
                    '<svg width="16" height="28" viewBox="0 0 16 28">' +
                        '<polyline ' +
                            'class="js-business-box-gallery__prev"' +
                            'points="15 1, 1 14, 15 27"></polyline>' +
                    '</svg>' +
                    '<svg width="16" height="28" viewBox="0 0 16 28">' +
                        '<polyline ' +
                            'class="js-business-box-gallery__next"' +
                            'points="1 1, 15 14, 1 27"></polyline>' +
                    '</svg>' +
                '</div>';

            this.$navigation = $(navigationHTML).hide();

            this.$el.parent().append(this.$navigation);
        };

        BusinessBoxGallery.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.BusinessBox', function(BusinessBox) {

        BusinessBox.fn.initialize = function($el) {
            this.$el = $el;
            this.$content = this.$el.find('.business-box__content');
        };

        BusinessBox.fn.init = function() {
            this.addOverlay();
            this.addCloseButton();
            this.copyMainImage();
            this.addEventListeners();
        };

        BusinessBox.fn.addCloseButton = function() {
            this.$content.append('<div class="business-box__close">X</div>');
        };

        BusinessBox.fn.addEventListeners = function() {
            var self = this;

            this.$el.
                on('click',
                    '.js-business-box__anchor', _.bind(this.open, this)).
                on('open', _.bind(this.open, this)).
                on('click',
                    '.business-box__close', _.bind(this.close, this, true)).
                on('close', _.bind(this.close, this));
        };

        BusinessBox.fn.open = function(e) {
            var target = [
                this.$overlay,
                this.$content,
                this.$mainImage
            ], self = this, count = 0;

            e.preventDefault();
            e.stopPropagation();

            history.replaceState && history.replaceState({}, {},
                this.$el.find('.js-business-box__anchor').attr('href'));

            this.$el.trigger('business-box:opening');

            _.forEach(target, function($el) {
                $el.fadeIn(function() {
                    count += 1;

                    if(count === target.length - 1) {
                        self.$el.trigger('business-box:opened', self.$el.index());
                    }
                });
            });
        }

        BusinessBox.fn.close = function(param1, param2) {
            var target = [
                this.$overlay,
                this.$content,
                this.$mainImage
            ], self = this, count = 0,
            e, trigger;

            if(param2 === undefined) {
                e = param1;
            } else {
                e = param2;
                trigger = param1;
            }

            trigger && this.$el.trigger('business-box:closing');

            e.stopPropagation();

            _.forEach(target, function($el) {
                $el.fadeOut(function() {
                    count += 1;

                    if(count === target.length - 1) {
                        self.$el.trigger('business-box:closed');
                    }
                });
            });
        };

        BusinessBox.fn.addOverlay = function() {
            this.$overlay = $('<div class="business-box__overlay">');

            this.$overlay.appendTo(this.$el);
        };

        BusinessBox.fn.copyMainImage = function() {
            this.$mainImage = this.$el.find('.js-business-box__main-image').clone();

            this.$el.append(
                this.$mainImage.addClass('business-box__main-image')
            );
        };

        BusinessBox.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.MainNav', function(MainNav) {

        MainNav.fn.initialize = function($el) {
            this.$el = $el;
            this.$mainNav = $el.find('.js-main-nav__nav');
            this.$mainNavTrigger = $el.find('.js-main-nav__trigger');
            this.overlay = new TAURUS.Overlay();

            this.$taurusLogo = $('.js-taurus-logo');
        };

        MainNav.fn.init = function() {
            this.overlay.init();
            this.overlay.addClass('overlay--dotted overlay--cover-page');
            this.addToggleArrows();
            this.setLogoZIndex();
            this.addEventListeners();
            this.selectCurrentURL();
        };

        MainNav.fn.selectCurrentURL = function() {
            var $target = this.$el.find('a[href="' + location.href + '"]');

            $target.addClass('main-nav__item--is-selected');

            if($target.hasClass('main-nav__item__subnav__item')) {
                $target.parent().parent().trigger('click');
            }
        };

        MainNav.fn.setLogoZIndex = function() {
            this.$taurusLogo.css('position', 'relative').data(
                'toggle-z-index',
                (parseInt(this.overlay.$overlay.css('z-index'), 10) || 0) + 1
            );
        };

        MainNav.fn.addToggleArrows = function() {
            var $target = this.$el.find('.main-nav__item__subnav'),
                arrowHTML =
                    '<svg class="main-nav__item__arrow" ' +
                        'viewbox="0 0 34 17" width="34" height="17">' +
                        '<polyline ' +
                            'points="0 1, 16 16, 32 0" ' +
                            'toggle-points="1 16, 16 1, 32 16"' +
                            '></polyline>'
                    '</svg>';

            $target.each(function() {
                $(this).parent().append(arrowHTML);
            });
        };

        MainNav.fn.toggleMainNavItem = function() {
            var $self = $(this),
                $title = $self.find('.main-nav__item__title'),
                $arrow = $self.find('.main-nav__item__arrow polyline'),
                $subNav = $self.find('.main-nav__item__subnav'),
                arrOldPoints;

            $subNav.slideToggle();
            if($title.prop('tagName') !== 'A') {
                $title.toggleClass('main-nav__item__title--active');
            }

            arrOldPoints = $arrow.attr('points');
            $arrow.attr('points', $arrow.attr('toggle-points'));
            $arrow.attr('toggle-points', arrOldPoints);
        };

        MainNav.fn.addEventListeners = function() {
            this.$el.on(
                'click',
                '.js-main-nav__trigger',
                _.bind(this.triggerMainNav, this)
            ).on(
                'click',
                '.main-nav__item',
                this.toggleMainNavItem
            ).on(
                'click',
                '.main-nav__item__subnav__item',
                function(e) {
                    var $link = $(this);

                    e.stopPropagation();
                    if($link.attr('rel') === 'external') {
                        window.open($link.attr('href'));
                        return false;
                    }
                    return true;
                }
            );
        };

        MainNav.fn.triggerMainNav = function(e) {
            var $target = this.$mainNavTrigger,
                $targetText = $target.find('.main-nav__trigger__text'),
                logoOldZIndex,
                oldText;

            this.overlay.toggle();
            this.$mainNav.fadeToggle();

            logoOldZIndex = this.$taurusLogo.css('z-index');
            this.$taurusLogo.css(
                'z-index',
                this.$taurusLogo.data('toggle-z-index')
            );
            this.$taurusLogo.data('toggle-z-index', logoOldZIndex);

            $target.toggleClass('main-nav__trigger--active');

            oldText = $targetText.html();
            $targetText.html($targetText.data('toggled-text'));
            $targetText.data('toggled-text', oldText);
        };

        MainNav.create = function() {
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        };

    });

    Module('TAURUS.SearchForm', function(SearchForm) {

        SearchForm.fn.initialize = function($el) {
            this.$el = $el;
            this.$tableCell = $el.closest('.table__cell');
            this.$input = $el.find('.search-form__input');
            this.$btn = $el.find('.search-form__submit');
        }

        SearchForm.fn.init = function() {
            this.addEventListeners();
        }

        SearchForm.fn.addEventListeners = function(){
            var me = this;
            this.$el.on('mouseenter', function(){
                me.showInput();
            });

            this.$el.on('mouseleave', function(){
                setTimeout(function(){
                    me.hideInput();
                }, 3000);
            });

            this.$input.on('focusout', function(){
                setTimeout(function(){
                    me.hideInput();
                }, 3000);
            });
        }

        SearchForm.fn.showInput = function(){
            var me = this;
            me.$tableCell.stop().animate({'width': 360}, 500);
            me.$input.stop().animate({'width': 172, 'padding': '8px'}, 500);
        }

        SearchForm.fn.hideInput = function(){
            var me = this;
            if(!me.$input.is(':focus') && !me.$el.is(':hover')){
                me.$tableCell.stop().animate({'width': 175}, 500);
                me.$input.stop().animate({'width': 0, 'padding': 0}, 500);
            }
        }

        SearchForm.create = function(){
            var instance = this.apply(null, [].slice.call(arguments));

            return instance.init();
        }
    });

    $('.search-form').each(function(){
        TAURUS.SearchForm.create($(this));
    });

    $('.js-footer-nav').each(function() {
        TAURUS.FooterNav.create($(this));
    });

    $('.js-main-nav').each(function() {
        TAURUS.MainNav.create($(this));
    });

    $('.js-business-box').each(function() {
        TAURUS.BusinessBox.create($(this));
    });

    $('.js-business-box-gallery').each(function() {
        TAURUS.BusinessBoxGallery.create($(this));
    });

    $('.js-content-banner-gallery').each(function() {
        TAURUS.ContentBannerGallery.create($(this));
    });

    $('.js-video-list').each(function() {
        TAURUS.VideoList.create($(this));
    });

    $('.js-load-more').each(function() {
        TAURUS.LoadMore.create($(this));
    });

    $('.js-picture-slider').each(function() {
        TAURUS.PictureSlider.create($(this));
    });

    $('.js-custom-select').each(function() {
        new TAURUS.CustomSelect($(this));
    })

    $('.js-form').each(function() {
        TAURUS.Form.create($(this));
    });

    $('.js-filter-content').each(function() {
        TAURUS.FilterContent.create($(this));
    });

    $('.js-custom-file-input').each(function() {
        TAURUS.CustomFileInput.create($(this));
    });

    TAURUS.externalLink.init();

    $( ".banner-nav-item" ).hover(function(e) {
        e.preventDefault();
        var classe = $(this).data('href');
        $('.active').removeClass('active');
        $(this).addClass('active');
        $('.banner-item-active').removeClass('banner-item-active');
        $(classe).addClass('banner-item-active').fadeIn();
    });
	
	function reOrderCountrySelector(region) {
        var array = [];
        array.push(region);
        $('#country-selector option').each(function() {
            if ($.inArray($(this).attr('data-region'), array) != -1) {
                $(this).css('display', 'inline-block');
            } else {
                $(this).css('display', 'none');
            }
        });
    }
	
	$('.distributors-nav a').click(function(e) {
        e.preventDefault();
        var url = $('.distributors-nav').data('url');
        var filtro = $(this).data('filter');
        var filtroF = filtro.replace(".", "");
        $('.item-active').removeClass('item-active');
        // history.pushState({}, {}, url + '/' + filtroF);
        reOrderCountrySelector(filtroF);
        $("#map-continents > ul > li.active-region").removeClass("active-region");
        $("#map-continents > ul > li[data-filter='."+filtroF+"']").addClass("active-region");
    });

    $('.downloads--filter').change(function(e) {
        e.preventDefault();
        var filter = $(this).val();
        $('.heading--downloads').html(filter);
    });
	
	// function changeMap(src) {
	// 	var image = $('.distributors-map img').attr('data-url');
	// 	$('.distributors-map img').attr('src',image + src +'.png')
	// }

    $(function(){
        var cl = '';
        var classeActive = $('.distributors-nav').data('active');
        if (classeActive) {
			cl = '.' + classeActive;
			// changeMap(classeActive);
			
		} else { 
			cl = 'all'; 
		}
        $('.distributors-page').mixItUp({
            load: {
                filter: cl
            }
        });
		
		$('.download-page').mixItUp();
		
        $('.fancybox').fancybox();

        $('.downloads--filter').on('change', function() {
            $('.downloads--filter option:selected').trigger('click'); 
        });
		
		var hash = window.location.hash.substring(1).toLowerCase();  
		
		$('.downloads--filter option').each(function(){				
			if($(this).val().toLowerCase() == hash ){				
				$(this).prop('selected', true).trigger('change');
			}
		});		
		
		if($('.banner-nav ul li').length > 0){			
			var max = $('.banner-nav ul li').length;
			var i = 0;
			setInterval(function(){ 
				$('.banner-nav ul li:eq('+i+') a').click();
				i++;
				if(i == max){					
					i = 0;
				}
			}, 8000);

			$('.banner-nav ul li a').click(function(){				
				i = $(this).parent().index();
			});			
		}
		
    });

    $(document).on('click', '.modal-pl, .modal-pl .modal-pl--inner span', function(e){
        if($(e.target).hasClass('modal-pl') || $(e.target).hasClass('close')){
            $('.modal-pl').fadeOut();
        }
    });

}(window, jQuery));


