<div class="page-wrapper main-content">

<div class="wrapper">
    <div class="a-b-columns a-b-columns--full sliced-content">
        <div class="a-b-columns__column default-content">
            <nav class="breadcrumb">
                <a href="http://192.168.0.20/taurus/pt">Home</a>
                <a href="http://192.168.0.20/taurus/pt/institucional/perfil_empresarial">Company</a>
            </nav>
            <h2 class="heading--alpha heading--inverse">Taurus Export</h2>
            <p>Forjas Taurus S.A. is a Brazilian public company based in Porto Alegre, Rio Grande do Sul. Founded in 1939, it currently has seven industrial plants: six in Brazil and one in Miami, USA.</p>

<p>On its 75th anniversary, Taurus is consolidated as one of largest Brazilian companies and one of the main representatives in the Global Defense and Security Market, exporting products to over 70 countries. Its prominence in the American market, one of the most demanding in the world, reflects its commercial.</p>

<p>Focused on people’s safety, Taurus provides products that are directly or indirectly present in your daily routine, which justifies the new concept: “Products that protect people.” With is range of weapons, the company has already received dozens of awards for its high standards of quality and innovation.</p>        </div>
        <div class="a-b-columns__column">
            <div class="picture-default"><div>
                <figure>
                    <img src="<?php echo base_url(); ?>assets/img/content/company.jpg">
                </figure>
            </div></div>
        </div>
    </div>
</div>