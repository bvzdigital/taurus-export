<div class="page-wrapper main-content">
<nav class="breadcrumb">
    <a href="<?php echo site_url(); ?>">
        <?php echo lang('about_the_company.home'); ?>
    </a>
    <a href="<?php el_url('about-the-company'); ?>">
        <?php echo lang('about_the_company.about'); ?>
    </a>
</nav>
<div class="wrapper">
    <div class="a-b-columns a-b-columns--full sliced-content">
        <div class="a-b-columns__column default-content">
            <h2 class="heading--alpha heading--inverse">Taurus Armas S.A.</h2>
            <?php echo $content->text; ?>
        </div>
        <div class="a-b-columns__column">
            <div class="picture-default">
                <figure>
                    <img src="<?php echo $content->image; ?>">
                </figure>
            </div>
        </div>
    </div>
</div>