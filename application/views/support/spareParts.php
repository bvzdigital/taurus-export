<div class="spare-parts-wrapper">
    <div class="wrapper">
        <nav class="breadcrumb">
            <a href="<?php echo site_url(); ?>">
                Home
            </a>
            <a href="#" id="breadcrumb-categories">
                Spare Parts
            </a>
        </nav>
    </div>

    <div class="spare-parts">
        <div class="spare-parts-inner">
            <div class="spare-parts-background"></div>
            <div class="spare-parts-page">
                <span>Feel free to check out our options of spare parts to your product.</span>
                <div class="button">
                    <a class="bt bt--primary" href="https://taurusarmas.com.br/catalogopecas/login" target="_blank">See more</a>
                </div>
            </div>
        </div>
    </div>
</div>