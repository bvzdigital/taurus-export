<div class="support-media-wrapper">
    <div class="wrapper">
        <nav class="breadcrumb">
            <a href="<?php echo site_url(); ?>">
                Home
            </a>
            <a href="<?php echo site_url(); ?>/en/support" id="breadcrumb-categories">
                Support
            </a>
            <a href="#" id="breadcrumb-categories">
                Media
            </a>
        </nav>
        <div class="support-media-content">
            <h2 class="heading--beta">Media</h2>
            <?php if (!empty($media_downloads)) { ?>
                <div class="media-content">
                    <?php foreach($media_downloads as $media) { ?>
                        <div class="support-download-wrapper">
                            <div class="support-download-inner">
                                <div class="support-download-image" style="background-image:url(<?= base_url() . 'assets/img/content/downloads/' . $media->image ?>)"></div>
                                <div class="support-download-title">
                                    <span><?= $media->name ?></span>
                                </div>
                                <div class="support-download-category">
                                    <span>Category: <?= $media->type ?></span>
                                </div>
                                <div class="support-download-button">
                                    <a class="bt bt--primary" href="<?= base_url() . 'assets/files/content/downloads/' . $media->file ?>" download>Get Content</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="no-support-downloads">
                    <strong>Sorry, no media downloads available.</strong>
                </div>
            <?php } ?>
        </div>
    </div>
</div>