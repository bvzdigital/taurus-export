<div class="support-media-wrapper">
    <div class="wrapper">
        <nav class="breadcrumb">
            <a href="<?php echo site_url(); ?>">
                Home
            </a>
            <a href="<?php echo site_url(); ?>/en/support" id="breadcrumb-categories">
                Support
            </a>
            <a href="#" id="breadcrumb-categories">
                Trademark
            </a>
        </nav>
        <div class="support-media-content">
            <h2 class="heading--beta">Trademark</h2>
            <?php if (!empty($trademark_downloads)) { ?>
                <div class="media-content">
                    <?php foreach($trademark_downloads as $trademark) { ?>
                        <div class="support-download-wrapper">
                            <div class="support-download-inner">
                                <div class="support-download-image" style="background-image:url(<?= base_url() . 'assets/img/content/downloads/' . $trademark->image ?>)"></div>
                                <div class="support-download-title">
                                    <span><?= $trademark->name ?></span>
                                </div>
                                <div class="support-download-category">
                                    <span>Category: <?= $trademark->type ?></span>
                                </div>
                                <div class="support-download-button">
                                    <a class="bt bt--primary" href="<?= base_url() . 'assets/files/content/downloads/' . $trademark->file ?>" download>Get Content</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="no-support-downloads">
                    <strong>Sorry, no trademark downloads available.</strong>
                </div>
            <?php } ?>
        </div>
    </div>
</div>