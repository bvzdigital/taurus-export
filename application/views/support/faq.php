<div class="faq-content">
    <div class="wrapper">
        <nav class="breadcrumb">
            <a href="<?php echo site_url(); ?>">
                Home
            </a>
            <a href="#" id="breadcrumb-categories">
                FAQ's
            </a>
        </nav>

        <div class="faq-content-inner">
            <div class="title">
                <h2 class="heading--alpha">FAQS</h2>
            </div>
            <div class="description">
                <strong>Answers to the most frequently asked questions about our products, purchasing and servicing details.</strong>
            </div>
        </div>

        <div id="question-type-selector-wrapper">
            <div class="question-type-inner">
                <div class="questions">
                    <div class="question-category" data-type="General">
                        <span>General</span>
                    </div>
                    <div class="question-category" data-type="Firearms">
                        <span>Firearms</span>
                    </div>
                    <div class="question-category" data-type="Repair">
                        <span>Repairs</span>
                    </div>
                    <div class="question-category" data-type="Misc">
                        <span>Misc</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="questions-and-answer">
            <?php foreach($faq as $qNa) { ?>
                <div class="qNa-wrapper" data-category="<?= $qNa->faq_category ?>">
                    <div class="question">
                        <?= $qNa->question ?>
                    </div>
                    <div class="answer">
                        <?= $qNa->answer ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>