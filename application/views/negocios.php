        <header class="main-header-full">
            <div class="top-bar">
                <div class="wrapper">
                    &nbsp
                </div>
            </div>

            <div class="wrapper">
                <div class="main-header__top-area table table--full">
                    <div class="table__cell table__cell--v-middle">
                        <a href="http://localhost/taurus/pt"><h1 class="js-taurus-logo taurus-logo main-header__logo taurus-logo--pt">Negócios</h1></a>
                    </div>
                    <div class="table__cell table__cell--v-middle table__cell--r-align">
                        <div class="main-header__nav-area">
                            <form class="search-form" action="http://localhost/taurus/pt/busca" method="POST">
                                <input class="search-form__input" type="search" placeholder="Busca" name="busca">
                                <button class="search-form__submit" type="submit">Busca</button>
                            </form>
                            <nav class="language-nav">
    <a class="language-nav__item language-nav__item--active" href="http://localhost/taurus/pt/">Port</a>
    <a class="language-nav__item " href="http://localhost/taurus/en/">Eng</a>
    <!-- <a class="language-nav__item " href="<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: es</p>
<p>Filename: partials/language-nav.php</p>
<p>Line Number: 4</p>

</div>">Esp</a> -->
</nav><nav class="js-main-nav main-nav">
    <span class="js-main-nav__trigger main-nav__trigger">
        <svg
            class="main-nav__trigger__icon"
            viewbox="0 0 15 13"
            width="15px"
            height="13px">
            <rect width="15" height="3" fill="white"></rect>
            <rect width="15" height="3" fill="white" y="5"></rect>
            <rect width="15" height="3" fill="white" y="10"></rect>
        </svg>
        <span
            class="main-nav__trigger__text"
            data-toggled-text="Fechar">Menu</span>
    </span>
    <div class="js-main-nav__nav main-nav__nav-wrapper">
        <div class="main-nav__nav-wrapper__title">Menu</div>
        <div class="main-nav__item">
            <span class="main-nav__item__title">Institucional</span>
            <nav class="main-nav__item__subnav">
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/institucional/perfil_empresarial">Perfil Empresarial</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/institucional/missao_visao_valores">Missão, Visão e Valores</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/institucional/historia">História</a>
                <!-- <a class="main-nav__item__subnav__item" href="#">Administração</a> -->
                <a class="main-nav__item__subnav__item" href="http://www.taurusri.com.br" rel="external">Relações com Investidores</a>
            </nav>
        </div>
        <div class="main-nav__item">
            <span class="main-nav__item__title">Negócios</span>
            <nav class="main-nav__item__subnav">
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/negocios/armas-e-acessorios">Armas e Acessórios</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/negocios/coletes">Coletes</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/negocios/capacetes-e-acessorios">Capacetes e Acessórios</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/negocios/conteineres-plasticos">Contêineres Plásticos</a>
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/negocios/mim">MIM (Metal Injection Molding)</a>
            </nav>
        </div>
        <div class="main-nav__item">
            <span class="main-nav__item__title">Sustentabilidade</span>
            <nav class="main-nav__item__subnav">
                <a class="main-nav__item__subnav__item" href="http://localhost/taurus/pt/sustentabilidade/fundacao_taurus">Fundação Taurus</a>
                <a class="main-nav__item__subnav__item" href="http://www.pubblicato.com.br/taurus_port_2013/index.html" rel="external">Relatório de Sustentabilidade</a>
            </nav>
        </div>
        <!-- <a class="main-nav__item main-nav__item__title" href="http://localhost/taurus/pt/downloads">Downloads</a> -->
        <a class="main-nav__item main-nav__item__title" href="http://localhost/taurus/pt/videos">Vídeos</a>
        <a class="main-nav__item main-nav__item__title" href="http://forjas-taurus-sa.infojobs.com.br/Microsite/ListVacancy.aspx?ic=160718&imic=1991" rel="external">Carreira</a>        <a class="main-nav__item main-nav__item__title" href="http://localhost/taurus/pt/imprensa">Assessoria de Imprensa</a>
        <a class="main-nav__item main-nav__item__title" href="http://localhost/taurus/pt/contato">Contato</a>
        <!--<div class="main-nav__item">
            <span class="main-nav__item__title">Acesso Restrito</span>
            <nav class="main-nav__item__subnav">
                <a class="main-nav__item__subnav__item" href="http://mail.taurus.com.br" rel="external">Webmail</a>
                <a class="main-nav__item__subnav__item" href="#">Representantes</a>
            </nav>
        </div> -->
    </div>
</nav>
                                                    </div>
                    </div>
                </div>
            </div>
        </header>

<div class="page-wrapper">
<div class="banner-full">
    <div class="wrapper js-content-banner-gallery">
                <div class="content-banner-box">
                    <article>
                        <h2 class="content-banner__title heading--beta heading--thin">Pistols</h2>
                        <p class="content-banner__cat"><span>Products</span></p>
                        <p>Mauris ligula nulla, adipiscing eu tempor sed, fermentum vitae risus. Aenean nec semper augue, id dictum sem... <a href="#">more</a></p>
                    </article>
                </div>
    </div>
    <div class="banner-nav">
        <div class="wrapper">
        <ul>
            <li><a href="#">Pistols</a></li>
            <li><a href="#">Pistols</a></li>
            <li><a href="#">Pistols</a></li>
            <li><a href="#">Pistols</a></li>
            <li><a href="#">Pistols</a></li>
            <li><a href="#">Pistols</a></li>
        </ul>
        </div>
    </div>
</div>



<div class="wrapper">
    <section class="brand-section">
        <header class="brand-section__header">
            <h2 class="heading--beta heading--no-margin">TAURUS, THE QUALITY IS IN THE BRAND.</h2>
            <p class="brand-section__text">Taurus is one of the three largest handgun manufacturers in the world with more than 70 years of history and more than 4,700 employees. The company produces a wide range of models of firearms, including Revolvers, Pistols, Shotguns, Rifles and Submachine Guns, all designed for Federal, State and local Law Enforcement as well as the Civilian. Taurus Firearms are exported and marketed in over 70 countries worldwide... <a href="#">more</a></p>
        </header>


    </section>
</div>


<div class="dotted-bg">
    <div class="wrapper">
        <section>

            <div class="product-showcase">
                                <article class="product-box">
                    <a href="http://localhost/taurus/pt/negocios/armas-e-acessorios">
                        <header class="product-box__header">
                           <figure class="product-box__figure">
                               <img src="assets/img/content/distributors.jpg" alt="Distributors">
                           </figure>
                           <h3 class="product-box__title">Armas e Acessórios</h3>
                        </header>
                        <p>A Taurus Armas e Acess&oacute;rios &eacute; uma das tr&ecirc;s maiores fabricantes de armas leves do mundo.</p>
                    </a>
                </article>
                                <article class="product-box">
                    <a href="http://localhost/taurus/pt/negocios/coletes">
                        <header class="product-box__header">
                           <figure class="product-box__figure">
                               <img src="assets/img/content/distributors.jpg" alt="Distributors">
                           </figure>
                           <h3 class="product-box__title">Distributors</h3>
                        </header>
                        <p>Atuando no mercado de coletes &agrave; prova de balas desde 1983, a Taurus &eacute; l&iacute;der em vendas no Brasil.</p>
                    </a>
                </article>
                                <article class="product-box">
                    <a href="http://localhost/taurus/pt/negocios/capacetes-e-acessorios">
                        <header class="product-box__header">
                           <figure class="product-box__figure">
                               <img src="assets/img/content/distributors.jpg" alt="Distributors">
                           </figure>
                           <h3 class="product-box__title">Capacetes e Acessórios</h3>
                        </header>
                        <p>A Taurus possui a lideran&ccedil;a absoluta em vendas de capacetes para motociclistas no Brasil.</p>
                    </a>
                </article>
            </div>

        </section>
     </div>
</div>