<div class="page-wrapper main-content">

<div class="wrapper pagecontact downloads-wrapper">
	<nav class="breadcrumb">
		<a href="<?php echo site_url(); ?>">
			Home
		</a>
		<a href="<?php el_url('downloads'); ?>">
			<?php echo lang('downloads.Downloads'); ?>
		</a>
	</nav>

	<h2 class="heading--alpha heading--inverse">
		<?php echo lang('downloads.Downloads'); ?>
	</h2>


	<form action="" class="download-form default-form">
		<fieldset class="download-form__fieldset">
			<label>
				<span class="visuallyhidden">
					<?php echo lang('downloads.category').' current category'; ?>
				</span>
				<div class="custom-select">
					<select name="empresa" class=" js-custom-select js-validate-required custom-select--real-select downloads--filter">
						<option value="">
							<?php echo lang('downloads.options-select'); ?>
						</option>
						<option class="filter" value="Posters" data-filter=".posters">
							<?php echo lang('downloads.options-posters'); ?>
						</option>
						<option class="filter" value="Catalog" data-filter=".catalog">
							<?php echo lang('downloads.options-catalog'); ?>
						</option>
						<option class="filter" value="News" data-filter=".news">
							<?php echo lang('downloads.options-news'); ?>
						</option>
					</select>
				</div>
			</label>
		</fieldset>
	</form>

	<section>
		<header><h3 class="heading--section"><strong><?php echo lang('downloads.category'); ?></strong> <span class="heading--downloads">Current Category</span></h3></header>
		<div class="list-showcase download-page">
			<?php foreach ($downloads as $download) : ?>
				<div class="mix list-box download-box <?php echo $download->type; ?>">
					<header class="download-box__header">
						 <figure class="download-box__figure">
							 <img src="<?php if ($download->image){ echo base_url().'assets/img/content/downloads/'.$download->image; } else { echo base_url().'assets/img/layout/placeholder-img.jpg'; } ?>" alt="Download">
						 </figure>
						 <time class="download-box__time"></time>
						 <h3 class="download-box__title">
						 	<?php if (isset($download->name)) echo $download->name; ?>
						 </h3>
					</header>
					<p class="download-box__category">
					<?php switch ($download->type) {
						case 'catalog':
							if ($this->lang->lang() == 'es'){
								$tipo = 'catálogo';
							} else {
								$tipo = 'catalog';
							}
							break;
						case 'news':
							if ($this->lang->lang() == 'es'){
								$tipo = 'notícias';
							} else {
								$tipo = 'news';
							}
							break;
						case 'posters':
							if ($this->lang->lang() == 'es'){
								$tipo = 'posters';
							} else {
								$tipo = 'posters';
							}
							break;
						}
					?>

						<?php echo lang('downloads.category').' '.ucfirst($tipo); ?>
					</p>
					<a href="<?php echo $download->file != '' && $download->file != null ? base_url().'assets/files/content/downloads/'.$download->file : ''; ?>" class="bt bt--secondary" download>
						<?php echo lang('downloads.Download'); ?>
					</a>
				</div>
			<?php endforeach; ?>

		</div>

	</section>
</div>