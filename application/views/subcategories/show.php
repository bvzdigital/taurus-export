<div class="page-wrapper main-content">

	<div class="wrapper">
		<nav class="breadcrumb">
			<a href="<?php echo site_url(); ?>">
				Home
			</a>
			<a href="#">
				<?php echo lang('subcategories.Products'); ?>
			</a>
			<a href="<?php echo el_url('products', $category->slug); ?>">
				<?php echo $category->title; ?>
			</a>
			<a href="<?php el_url('products', [$category->slug, $subcategory->slug]) ?>">
				<?php echo $subcategory->title; ?>
			</a>
		</nav>

		<h2 class="heading--alpha heading--inverse">
			<span><?php echo $category->title; ?> -</span> <?php echo $subcategory->title; ?>
		</h2>
		<section>
			<div class="list-showcase subcategory-page">
				<?php foreach ($products as $product): ?>
					<article class="list-box subcategory-box">
						<header class="product-box__header">
							<a href="<?php el_url('products', [$category->slug, $subcategory->slug, $product->slug]) ?>">
								<figure class="product-box__figure_sub">
									<img src="<?php echo $product->gallery != null ? thumbnail('assets/img/content/products/'.$product->gallery[0]->url, 300, 200) : base_url().'assets/img/layout/placeholder-img.jpg'; ?>"
									alt="<?php echo $product->name; ?>">
								</figure>

								<h3 class="product-box__title">
									<?php echo $product->name; ?>
								</h3>
							</a>
						</header>
						<p>
							<?php echo limit_text($product->description, 15); ?> <!-- <a href="<?php el_url('products', [$category->slug, $subcategory->slug, $product->slug]) ?>"><?php echo lang('subcategories.more'); ?></a> -->
						</p>
					</article>
				<?php endforeach; ?>
			</div>
		</section>
	</div>