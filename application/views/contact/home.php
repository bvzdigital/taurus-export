<div class="page-wrapper main-content">

<div class="wrapper pagecontact">
    <nav class="breadcrumb">
        <a href="<?php el_url('home'); ?>">Home</a>
        <a href="<?php el_url('contact'); ?>"><?php echo lang('contact.contact'); ?></a>
    </nav>

    <h2 class="heading--alpha heading--inverse"><?php echo lang('contact.contact'); ?></h2>

    <form data-url-action="<?php el_url('contact'); ?>/create" class="contact-form default-form js-form">
            <fieldset class="contact-form__fieldset">

                <label>
                    <span class="visuallyhidden"><?php echo lang('contact.name'); ?></span>
                    <input type="text" name="name" placeholder="<?php echo lang('contact.name'); ?>" class="js-validate-required">
                </label>
                <label>
                    <span class="visuallyhidden"><?php echo lang('contact.email'); ?></span>
                    <input type="email" name="email" placeholder="<?php echo lang('contact.email'); ?>" class="js-validate-required">
                </label>
                <label>
                    <span class="visuallyhidden"><?php echo lang('contact.phone'); ?></span>
                    <input type="text" name="phone" placeholder="<?php echo lang('contact.phone'); ?>" class="js-validate-required">
                </label>
                <label>
                    <span class="visuallyhidden"><?php echo lang('contact.region'); ?></span>
                    <div class="custom-select">
                        <select name="region" class="js-custom-select js-validate-required custom-select--real-select">
                            <option value=""><?php echo lang('contact.region'); ?></option>
                            <option value="africa">Africa</option>
                            <option value="asia">Asian</option>
                            <option value="europa">Europe</option>
                            <option value="america-latina">Latin America</option>
                            <option value="oriente-medio">Middle East</option>
                            <option value="oceania">Oceania</option>
                        </select>
                    </div>
                </label>
                <label>
                    <span class="visuallyhidden">Country</span>
                        <div class="custom-select">
                            <select name="country" class="js-custom-select js-validate-required custom-select--real-select">
                                <option value="">Country</option>
                                <?php foreach($countries as $listaPaises) { ?>
                                    <option value="<?= $listaPaises->name; ?>"><?= $listaPaises->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                </label>
            </fieldset>
            <fieldset class="contact-form__fieldset contact-form__fieldset--full">
                <label>
                    <span class="visuallyhidden"><?php echo lang('contact.message'); ?></span>
                    <textarea name="message" rows="6" placeholder="<?php echo lang('contact.message'); ?>" class="js-validate-required"></textarea>
                </label>
            </fieldset>
            <fieldset class="contact-form__fieldset contact-form__submit-area">
                <button type="submit" class="bt bt--primary"><?php echo lang('contact.send'); ?></button>
            </fieldset>
        <div class="default-form__message"><div class="table"><div class="table__cell table__cell--v-middle"></div></div></div>
    </form>
</div>