<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo isset($title) && !empty($title) ? $title : 'Taurus Export'; ?></title>
    <meta name="description" content="Empresas Taurus: Taurus Armas, Taurus Forjados, Taurus Blindagens, Taurus Plast, Famastil Taurus Ferramentas, Taurus Capacetes e Taurus Wotan. A Taurus exporta para diversos países,sempre preocupada com responsabilidade social e compromisso com investidores. Taurus - Av. do Forte, 511. Porto Alegre/RS. SAC:0800 600 66 00 - sac@taurus.com.br" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='icon' href='<?php echo base_url(); ?>favicon.png' type='image/x-icon'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url('assets/js/fancybox/jquery.fancybox.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/main.css'); ?>?v=<?php echo date('YmdHis'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/cssmap-continents.css'); ?>">
</head>

<body>
    <div class="minresolution" style="display: none;">Resolução mínima: 335px</div>

    <header class="main-header-full">
        <!-- <div class="top-bar">
                <div class="wrapper">
                    <div class="top-bar-item top-bar-item--a">
                        <a href="http://www.taurus.com.br">
                            <?php echo lang('header.mother_menu-institutional'); ?>
                        </a>
                    </div>
                    <div class="top-bar-item top-bar-item--b"><span class="item-label">
                        <?php echo lang('header.mother_menu-guns_and_acessories'); ?>:</span><a href="#">Brazil</a><a href="#">USA</a><a href="#" class="item-active">Export</a></div>
                    <div class="top-bar-item top-bar-item--c">
                        <a href="#">
                            <?php echo lang('header.mother_menu-bulletproof_vests'); ?>
                        </a>
                    </div>
                    <div class="top-bar-item top-bar-item--d">
                        <a href="#">
                        <?php echo lang('header.mother_menu-helmets_and_acessories'); ?>
                        </a>
                    </div>
                    <div class="top-bar-item top-bar-item--e">
                        <a href="#">
                            <?php echo lang('header.mother_menu-plastic_containers'); ?>
                        </a>
                    </div>
                    <div class="top-bar-item top-bar-item--f">
                        <a href="#">
                            <?php echo lang('header.mother_menu-mim'); ?>
                        </a>
                    </div>
                </div>
            </div> -->

        <div class="wrapper">
            <div class="main-header__top-area table table--full">
                <div class="table__cell table__cell--v-middle">
                    <a href="<?php echo site_url(); ?>">
                        <h1 class="js-taurus-logo taurus-logo main-header__logo taurus-logo--pt">
                            <?php echo lang('header.logo'); ?>
                        </h1>
                    </a>
                </div>
                <nav class="language-nav">
                    <div class="select-language">
                        <a class="language-nav__item" href="">
                            <i class="<?php echo (current_language() == 'en') ? 'flag-en' : 'flag-es'; ?>"></i>
                            <span class="lang"><?php echo (current_language() == 'en') ? "EN" : "ES"; ?></span>
                            <span class="fa-angle-down"></span>
                        </a>
                        <a class="language-nav__item" href="<?php echo $uri_set[(current_language() == 'en') ? 'es' : 'en']; ?>">
                            <i class="<?php echo (current_language() == 'en') ? 'flag-es' : 'flag-en'; ?>"></i>
                            <span class="lang"><?php echo (current_language() == 'en') ? "ES" : "EN"; ?></span>
                        </a>
                    </div>
                </nav>

                <div class="menutop">
                    <div class="contact-nav__area social">
                        <!--begin social!-->
                        <a href="https://www.facebook.com/taurusarmasofficial" target="_blank">
                            <span class="icon social icon-rounded-facebook"></span>
                        </a>
                        <a href="https://www.linkedin.com/company/forjas-taurus-s-a" target="_blank">
                            <span class="icon social icon-rounded-linkedin"></span>
                        </a>
                        <a href="https://www.youtube.com/channel/UC5Xlrrp7me99Dgjj7xOTSfw" target="_blank">
                            <span class="icon social icon-rounded-youtube"></span>
                        </a>
                    </div>
                    <!--end social!-->
                    <div class="table__cell table__cell--v-middle table__cell--r-align msearch">
                        <!--begin search!-->
                        <div class="main-header__nav-area">
                            <form class="search-form" action="<?php echo site_url('search'); ?>" method="POST">
                                <input class="search-form__input" type="search" placeholder="<?php echo lang('header.Search'); ?>" name="search" value="<?php echo @$termo; ?>">
                                <button class="search-form__submit" type="submit">
                                    <?php echo lang('header.Search'); ?>
                                </button>
                            </form>
                            <nav class="js-main-nav main-nav">
                                <span class="js-main-nav__trigger main-nav__trigger">
                                    MENU
                                    <svg class="main-nav__trigger__icon" viewbox="0 0 15 13" width="15px" height="13px">
                                        <rect width="15" height="3" fill="#bf0411"></rect>
                                        <rect width="15" height="3" fill="#bf0411" y="5"></rect>
                                        <rect width="15" height="3" fill="#bf0411" y="10"></rect>
                                    </svg>
                                    <span class="main-nav__trigger__text" data-toggled-text=""></span>
                                </span>

                                <div class="js-main-nav__nav main-nav__nav-wrapper">
                                    <div class="main-nav__nav-wrapper__title"></div>
                                    <a class="main-nav__item main-nav__item__title" href="<?php el_url('about-the-company'); ?>">
                                        <?php echo lang('header.menu-company'); ?>
                                    </a>
                                    <div class="main-nav__item">
                                        <span class="main-nav__item__title">
                                            <?php echo lang('header.menu-products'); ?>
                                        </span>
                                        <nav class="main-nav__item__subnav">
                                            <div class="main-nav__item__title main-nav__item__title--active main-nav__item__subtitle">
                                                <img src="<?php echo base_url() . 'assets/img/layout/taurus-logo-menu.png'; ?>">
                                                <a>Taurus</a>
                                            </div>
                                            <?php foreach ($categories_taurus as $category) : ?>
                                                <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?php el_url('products', $category->slug); ?>">
                                                    <?php echo $category->title; ?>
                                                </a>
                                            <?php endforeach; ?>
                                        </nav>
                                    </div>
                                    <div class="main-nav__item">
                                        <span class="main-nav__item__title">
                                            Dealers
                                        </span>
                                        <nav class="main-nav__item__subnav">
                                            <a class="main-nav__item__subnav__item" href="<?php el_url('distributors-taurus'); ?>">
                                                Taurus
                                            </a>
                                        </nav>
                                    </div>
                                    <a class="main-nav__item main-nav__item__title" href="<?php el_url('downloads'); ?>">
                                        <?php echo lang('header.menu-downloads'); ?>
                                    </a>
                                    <a class="main-nav__item main-nav__item__title" href="<?= base_url() ?>en/news">
                                        News & Updates
                                    </a>
                                    <div class="main-nav__item">
                                        <span class="main-nav__item__title">
                                            Safety
                                        </span>
                                        <nav class="main-nav__item__subnav">
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/safety/home-gun-safety">
                                                Home Gun Safety
                                            </a>
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/safety/10-rules-of-firearms-safety">
                                                10 Rules of Firearms Safety
                                            </a>
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/safety/range-safety">
                                                Range Safety
                                            </a>
                                        </nav>
                                    </div>
                                    <div class="main-nav__item">
                                        <span class="main-nav__item__title">
                                            Support
                                        </span>
                                        <nav class="main-nav__item__subnav">
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/support/spare-parts">
                                                Spare Parts
                                            </a>
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/support/media">
                                                Media
                                            </a>
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/support/trademark">
                                                Trademark
                                            </a>
                                            <a class="main-nav__item__subnav__item main-nav__item__subnav__item__indent" href="<?= base_url() ?>en/support/faq">
                                                FAQ's
                                            </a>
                                        </nav>
                                    </div>
                                    <a class="main-nav__item main-nav__item__title" href="<?php el_url('contact'); ?>">
                                        <?php echo lang('header.menu-contact'); ?>
                                    </a>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <!--end search!-->
                </div>
            </div>
        </div>
    </header>