<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>
	<p>A seguinte mensagem foi enviada através do formulário de contato (<?php echo $pagina; ?>):</p>
	<p>
		Name: <strong><?php echo $name; ?></strong><br>
		Email: <strong><?php echo $email; ?></strong><br>
		Phone: <strong><?php echo $phone; ?></strong><br>
		Country: <strong><?php echo $country; ?></strong>
	</p>
	<p><?php echo $message; ?></p>
	<br>
	<p><small><i>Esta mensagem também foi registrada no painel administrativo do site.</i></small></p>
	<a href="<?php echo $admin; ?>"><?php echo $admin; ?></a>
</body>
</html>