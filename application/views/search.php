<div class="page-wrapper main-content">
    <div class="wrapper">
        <nav class="breadcrumb">
            <a href="<?php echo site_url(); ?>">Home</a>
            <a href="#"><?php echo lang('search.search'); ?></a>
        </nav>
        <h2 class="heading--alpha"><?php echo lang('search.search_results'); ?></h2>
        <div class="search-page__paragraph">
            <?php if(empty($resultados)) : ?>
                <p><?php echo lang('search.no_results', array('<span class="search-page__paragraph__highlight">', $termo, '</span>')) ?></p>
            <?php else : ?>
                <p><?php echo lang('search.success', array('<span class="search-page__paragraph__highlight">', $resultados, '</span>', '<span class="search-page__paragraph__highlight">', $termo, '</span>')); ?></p>
            <?php endif; ?>
        </div>
    </div>

    <div class="wrapper">
        <?php $len = sizeof($products); ?>
        <?php if($len > 0) : ?>
            <h3 class="heading--epsilon default-content__heading-extra-bold"><?php echo lang('search.products'); ?></h3>
            <div class="media-box-list">
                <?php for($i = 0; $i<$len; $i++) : ?>
                    <article class="list-box subcategory-box">
                        <header class="product-box__header">
                            <a href="<?php el_url('products', [$products[$i]->category->slug, $products[$i]->subcategory->slug, $products[$i]->slug]) ?>">
                                <figure class="product-box__figure_sub">
                                    <img src="<?php echo $products[$i]->gallery != null ? thumbnail('assets/img/content/products/'.$products[$i]->gallery[0]->url, 300, 200) : base_url().'assets/img/layout/placeholder-img.jpg'; ?>">
                                </figure>

                                <h3 class="product-box__title">
                                    <?php echo $products[$i]->name; ?>
                                </h3>
                            </a>
                        </header>
                        <p>
                            <?php echo limit_text($products[$i]->description, 15); ?> <a href="<?php el_url('products', [$products[$i]->category->slug, $products[$i]->subcategory->slug, $products[$i]->slug]) ?>"><?php echo lang('subcategories.more'); ?></a>
                        </p>
                    </article>
                <?php endfor; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="wrapper">
        <?php $len = sizeof($subcategories); ?>
        <?php if($len > 0) : ?>
            <h3 class="heading--epsilon default-content__heading-extra-bold"><?php echo lang('search.subcategories'); ?></h3>
            <div class="media-box-list">
                <?php for($i = 0; $i<$len; $i++) : ?>
                    <article class="list-box subcategory-box">
                        <header class="product-box__header">
                            <a href="<?php el_url('products', [$subcategories[$i]->category->slug, $subcategories[$i]->slug]) ?>">
                                <figure class="product-box__figure">
                                    <img src="<?php echo thumbnail('assets/img/content/subcategories/'.$subcategories[$i]->image, 300, 140); ?>">
                                </figure>

                                <h3 class="product-box__title">
                                    <?php echo $subcategories[$i]->title; ?>
                                </h3>
                            </a>
                        </header>
                    </article>
                <?php endfor; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="wrapper">
        <?php $len = sizeof($categories); ?>
        <?php if($len > 0) : ?>
            <h3 class="heading--epsilon default-content__heading-extra-bold"><?php echo lang('search.categories'); ?></h3>
            <div class="media-box-list">
                <?php for($i = 0; $i<$len; $i++) : ?>
                    <article class="list-box subcategory-box">
                        <header class="product-box__header">
                            <a href="<?php el_url('products', $categories[$i]->slug) ?>">
                                <figure class="product-box__figure">
                                    <img src="<?php echo thumbnail('assets/img/content/categories/'.$categories[$i]->image, 300, 140); ?>">
                                </figure>

                                <h3 class="product-box__title">
                                    <?php echo $categories[$i]->title; ?>
                                </h3>
                            </a>
                        </header>
                    </article>
                <?php endfor; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="wrapper">
        <?php $len_rossi = sizeof($distributors_rossi); ?>
        <?php if($len_rossi > 0) : ?>
            <h3 class="heading--epsilon default-content__heading-extra-bold"><?php echo lang('search.distributors').' - Rossi'; ?></h3>
            <?php for($i = 0; $i<$len_rossi; $i++) : ?>
                <article class="media-box media-box--half-width">
                    <a rel="external" href="<?php el_url('distributors-'.strtolower($distributors_rossi[$i]->brand)); ?>"><span class="media-box__heading heading--epsilon heading--thin"><?php echo $distributors_rossi[$i]->name; ?></span></a>
                    <div class="paragraph media-box__padded-text">
                        <p>
                            <?php echo isset($distributors_rossi[$i]->region_id) ? get_region($distributors_rossi[$i]->region_id) : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_rossi[$i]->address) ? $distributors_rossi[$i]->address : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_rossi[$i]->address) ? $distributors_rossi[$i]->emails : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_rossi[$i]->address) ? $distributors_rossi[$i]->phones : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_rossi[$i]->address) ? $distributors_rossi[$i]->site : ''; ?>
                        </p>
                    </div>
                </article>
            <?php endfor; ?>
        <?php endif; ?>
    </div>

    <div class="wrapper">
        <?php $len_taurus = sizeof($distributors_taurus); ?>
        <?php if($len_taurus > 0) : ?>
            <h3 class="heading--epsilon default-content__heading-extra-bold"><?php echo lang('search.distributors').' - Taurus'; ?></h3>
            <?php for($i = 0; $i<$len_taurus; $i++) : ?>
                <article class="media-box media-box--half-width">
                    <a rel="external" href="<?php el_url('distributors-'.strtolower($distributors_taurus[$i]->brand)); ?>"><span class="media-box__heading heading--epsilon heading--thin"><?php echo $distributors_taurus[$i]->name; ?></span></a>
                    <div class="paragraph media-box__padded-text">
                        <p>
                            <?php echo isset($distributors_taurus[$i]->region_id) ? get_region($distributors_taurus[$i]->region_id) : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_taurus[$i]->address) ? $distributors_taurus[$i]->address : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_taurus[$i]->address) ? $distributors_taurus[$i]->emails : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_taurus[$i]->address) ? $distributors_taurus[$i]->phones : ''; ?>
                        </p>
                        <p>
                            <?php echo isset($distributors_taurus[$i]->address) ? $distributors_taurus[$i]->site : ''; ?>
                        </p>
                    </div>
                </article>
            <?php endfor; ?>
        <?php endif; ?>
    </div>

    <div class="wrapper">
        <?php $len = sizeof($downloads); ?>
        <?php if($len > 0) : ?>
            <a href="<?php echo site_url('downloads'); ?>">
                <h3 class="heading--epsilon default-content__heading-extra-bold">
                    <?php echo lang('search.downloads'); ?>
                </h3>
            </a>
            <?php for($i = 0; $i<$len; $i++) : ?>
                <article class="list-box download-box">
                    <header class="download-box__header">
                         <figure class="download-box__figure">
                             <img src="<?php if ($downloads[$i]->image){ echo base_url().'assets/img/content/downloads/'.$downloads[$i]->image; } else { echo base_url().'assets/img/layout/placeholder-img.jpg'; } ?>" alt="Download">
                         </figure>
                         <time class="download-box__time"></time>
                         <h3 class="download-box__title">
                            <?php if (isset($downloads[$i]->name)) echo $downloads[$i]->name; ?>
                         </h3>
                    </header>
                    <p class="download-box__category">
                        <?php echo lang('search.category').' '.ucfirst($downloads[$i]->type); ?>
                    </p>
                    <a href="<?php echo base_url().'assets/files/content/downloads/'.$downloads[$i]->file; ?>" class="bt bt--secondary" download>
                        <?php echo lang('search.download'); ?>
                    </a>
                </article>
            <?php endfor; ?>
        <?php endif; ?>
    </div>
