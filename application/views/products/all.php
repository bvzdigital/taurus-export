<div class="page-wrapper main-content">

<div class="wrapper">
    <nav class="breadcrumb">
        <a href="<?php echo site_url(); ?>">
            Home
        </a>
        <a href="#" id="breadcrumb-categories">
            Products
        </a>
    </nav>

    <h2 class="heading--alpha heading--inverse">
        PRODUCTS
    </h2>
    
    <?php if(isset($category->description)): ?>
    <!-- <article class="category-description">
        <?php echo $category->description;?>
    </article> -->
    <?php endif; ?>
    <section>
        <div class="list-showcase subcategory-page">
        <?php 
        ?>
            <?php foreach ($category as $product): ?>
                <article class="list-box subcategory-box">
                    <header class="product-box__header">
                        <a href="<?php el_url('products', $product->slug); ?>">
                            <figure class="product-box__figure_sub">
                                <img src="<?php echo @$product->image != null ? thumbnail('assets/img/content/categories/'.$product->image, 270, 140) : base_url().'assets/img/layout/placeholder-img.jpg'; ?>"
                                    alt="<?php echo @$product->image ?>">
                            </figure>

                            <h3 class="product-box__title" style="text-transform: uppercase;"><?php echo $product->title; ?></h3>
                        </a>
                    </header>
                    <p>
                        <?php echo limit_text($product->abstract, 15); ?> <a href="<?php el_url('products', $product->slug); ?>"></a>
                    </p>
                </article>
            <?php endforeach; ?>
        </div>
    </section>
</div>