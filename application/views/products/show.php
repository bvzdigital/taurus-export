<div class="page-wrapper main-content">

    <div class="wrapper pagecontact2">
        <div class="a-b-columns a-b-columns--full product-content">
            <div class="a-b-columns__column product-box-text">
                <nav class="breadcrumb">
                    <a href="<?php echo site_url(); ?>">
                        Home
                    </a>
                    <a href="<?php echo el_url('products', $category->slug); ?>">
                        <?php echo lang('products.Products'); ?>
                    </a>
                    <a href="<?php echo el_url('products', $category->slug); ?>">
                        <?php echo $category->title; ?>
                    </a>
                    <a href="<?php el_url('products', [$category->slug, $subcategory->slug]) ?>">
                        <?php echo $subcategory->title; ?>
                    </a>
                    <a href="<?php el_url('products', [$category->slug, $subcategory->slug, $product->slug]) ?>">
                        <?php echo $product->name; ?>
                    </a>
                </nav>
                <h2 class="heading--alpha heading--extra">
                    <?php echo $product->name; ?>
                </h2>
                <p>
                    <?php echo $product->description; ?>
                </p>
            </div>

            <div class="a-b-columns__column product-box-gallery js-content-banner-gallery" data-rotate="true" >
                <?php foreach ($product->gallery as $image): ?>
                    <div data-pagination-width="470" data-width="690" class="picture-slider js-picture-slider" style="width: 690px;">
                        <div>
                            <figure>
                             <a href="<?php echo imgs_url('products/'.$image->url); ?>" rel="gallery" class="zoom fancybox">
                                <img src="<?php echo thumbnail('assets/img/content/products/'.$image->url, 690, 460); ?>" />
                            </a>							
                        </figure>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="wrapper pagecontact2">
    <div class="a-b-columns a-b-columns--full product-content">
        <?php
        if (isset($product->video) && ! empty($product->video)) {
            $this->load->view('products/product-detail-video', ['product' => $product]);
        } else {
            $this->load->view('products/product-detail-novideo', ['product' => $product]);
        }
        ?>
    </div>
</div>

<?php if (count($awards) > 0){ ?>
<div class="dotted-bg">
    <div class="wrapper pagecontact2">
        <section>
            <header class="award-header">
                <h3 class="heading--beta heading--no-margin">
                    <?php echo lang('products.Awards'); ?>
                </h3>
                <p><?php echo $product->name; ?></p>
            </header>
            <div class="award-showcase">
                <?php foreach ($awards as $award): ?>
                    <article class="award-box">
                        <figure class="product-box__figure">
                            <img src="<?php echo imgs_url('awards/'.$award->image); ?>" alt="<?php echo $award->title; ?>">
                        </figure>
                        <p><?php echo $award->title; ?></p>
                    </article>
                <?php endforeach; ?>
            </div>
        </section>
    </div>
</div>
<?php } ?>