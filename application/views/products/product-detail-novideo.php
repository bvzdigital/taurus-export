<div class="a-b-columns__column product-box-detail">
    <h3 class="heading--delta">
        <?php echo lang('products.technical_specifications'); ?>
    </h3>
    <div class="product-spec">
        <?php echo $product->technical_specifications; ?>
    </div>
</div>

<div class="a-b-columns__column product-box-detail">
    <div class="download-nav">
        <div class="download-nav__area">
            <span class="icon icon-camera"> </span>
            <a href="<?php echo current_url();?>/download">
                <?php echo lang('products.download_images'); ?>
            </a>
        </div>
        <?php if ($product->promotional_pack){ ?>
        <div class="download-nav__area">
            <span class="icon icon-book"></span>
            <a href="<?php echo base_url().'assets/files/content/products/'.$product->promotional_pack; ?>" rel="external">
                Download user manual
            </a>
        </div>
        <?php } ?>
        <?php if ($product->specs_document){ ?>
        <div class="download-nav__area">
            <span class="icon icon-book"></span>
            <a href="<?php echo base_url().'assets/files/content/products/'.$product->specs_document; ?>" rel="external">
                <?php echo lang('products.download_tech_specs'); ?>
            </a>
        </div>
        <?php } ?>
    </div>
</div>