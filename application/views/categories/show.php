<div class="page-wrapper main-content">

	<div class="wrapper pagecontact categories-page">
		<nav class="breadcrumb">
			<a href="<?php echo site_url(); ?>">
				Home
			</a>
			<a href="#">
				<?php echo lang('categories.Products'); ?>
			</a>
			<a href="<?php echo el_url('products', $category->slug); ?>">
				<?php echo $category->title; ?>
			</a>
		</nav>

		<h2 class="heading--alpha heading--inverse">
			<?php echo $category->title; ?>
		</h2>
		<?php
		if (!is_array($subcategories)) {
			$subcategory = $subcategories;
			$subcategories = array();
			$subcategories[] = $subcategory;
		}
		
		foreach ($subcategories as $subcategory) : ?>
				<?php if(empty($subcategory->products)) { ?>
					<?php continue; ?>
				<?php } ?>
				<div class="subcategory-wrapper">
					<h2 class="featured-title subcategory-title"><?= $subcategory->title ?></h2>
					<?php foreach ($subcategory->products as $product) : ?>
						<article class="list-box subcategory-box">
							<header class="product-box__header">
								<a href="<?php echo el_url('products', [$category->slug, $subcategory->slug, $product->slug]) ?>">
									<figure class="product-box__figure_sub">
										<img src="<?php echo $product->gallery != null ? thumbnail('assets/img/content/products/' . $product->gallery[0]->url, 300, 200) : base_url() . 'assets/img/layout/placeholder-img.jpg'; ?>" alt="<?php echo $product->name; ?>">
									</figure>
	
									<h3 class="product-box__title">
										<?php echo $product->name; ?>
									</h3>
								</a>
							</header>
						</article>
					<?php endforeach; ?>
				</div>
		<?php endforeach; ?>
	</div>