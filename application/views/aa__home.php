<?php if($modal && count($modal) > 0):
    if($modal->title != ''): ?>
        <div class="modal-pl" style="display: none;">
            <div class="modal-inner">
                <span class="close">FECHAR</span>
            <?php if($modal->link != ''): ?>
                <a href="<?php echo $modal->link; ?>" <?php echo ($modal->target != '')? 'target="'.$modal->target.'"' : ''?>>
            <?php endif; ?>
                <article class="taurus-novo-local">
                    <h1><?php echo $modal->title; ?></h1>
                    <?php echo $modal->description; ?>
                </article>
            <?php if($modal->link != ''): ?>
                </a>
            <?php endif; ?>
            </div>
        </div>
    <?php else: ?>
        <div class="modal-pl" style="display: none;">
            <div class="modal-pl--inner">
                <?php if($modal->link != ''): ?>
                    <a href="<?php echo $modal->link; ?>" <?php echo ($modal->target != '')? 'target="'.$modal->target.'"' : ''?>>
                <?php endif; ?>
               <!-- <img src="<?php //echo base_url().'assets/img/content/modals/'.$modal->image; ?>">!-->
                <?php if($modal->link != ''): ?>
                    </a>
                <?php endif; ?>
                <span class="close"></span>
            </div>
        </div>
    <?php endif;
endif; ?>

<div class="page-wrapper">
    <div class="banner-full">
        <?php $i = 1; foreach ($banners as $listaBanner) { ?>
            <a href="<?php el_url('products', $listaBanner->slug); ?>">
                <div class="banner-item banner-nav-item-<?php echo $i; ?> <?php if ($i == 1){ echo 'banner-item-active'; }?>" style="background-image: url('<?php echo base_url(); ?>assets/img/content/categories/<?php echo $listaBanner->image; ?>');background-repeat:no-repeat;background-size: cover;width: 100%;">
                    <div class="banner-inner">
                        <div class="wrapper content-banner-box">
                            <article>
                                <h2 class="content-banner__title heading--beta heading--thin banner-caption"><?php echo $listaBanner->title; ?></h2>
                                <p class="banner-caption--abstract">
                                    <?php echo $listaBanner->abstract; ?>
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </a>
        <?php $i++; } ?>
        <div class="banner-nav">
            <div class="banner-wrapper">
            <ul>
                <?php $i = 1; foreach ($banners as $listaBanner) { ?>
                <li><a href="<?php el_url('products', $listaBanner->slug); ?>" class="banner-nav-item <?php if ($i == 1){ echo 'active'; }?>" data-href=".banner-nav-item-<?php echo $i; ?>"><span><?php echo $listaBanner->title; ?></span></a></li>
                <?php $i++; } ?>
            </ul>
            </div>
        </div>
    </div>

    <div class="alert-full" style="display: none;">
        <div class="alert-wrapper">
            <a href="http://www.qualidadetaurus.com.br" target="_blank">
                <img src="<?php echo base_url().'assets/img/layout/text-qualidade-taurus-site.png'; ?>">
            </a>
        </div>
    </div>
<div class="wrapper wrapper-brand">
    <div class="wrapper-inner">
        <section class="brand-section">
            <header class="brand-section__header">
                <h2 class="heading--beta heading--no-margin">
                    <?php echo $text_line; ?>
                </h2>
                <p class="brand-section__text">
                    <?php echo limit_text($text, 72); ?> <!--<a href="<?php el_url('institucional'); ?>"><?php echo lang('home.more'); ?></a>-->
                </p>
            </header>
        </section>

        <img class="timeline-brand-image" src="<?php echo base_url().'assets/img/layout/timeline-evolucao-marca-transparencia_1.png'?>" alt=""/>
    </div>
</div>
<div class="repaging dotted-bg">
    <div class="wrapper">
        <section>
            <div class="product-showcase">
                <h2 class="featured-title"><?php echo lang('home.featured-title'); ?></h2>
                <?php foreach ($featured_contents as $featured_content): ?>
                    <article class="product-box">
                            <header class="product-box__header">
                               <a href="<?php echo $featured_content->link; ?>" target="<?php echo $featured_content->target; ?>">
                                   <figure class="product-box__figure">
                                       <img src="<?php echo base_url().'assets/img/content/featured_contents/'.$featured_content->image; ?>" alt="<?php echo $featured_content->title; ?>">
                                   </figure>
                               </a>
                               <h3 class="product-box__title">
                                   <?php echo $featured_content->title; ?>
                               </h3>
                            </header>
                            <p>
                                <?php echo limit_text($featured_content->text, 30); ?> <a href="<?php echo $featured_content->link; ?>" target="<?php echo $featured_content->target; ?>"><?php echo lang('home.more'); ?></a>
                            </p>
                        </a>
                    </article>
                <?php endforeach; ?>
            </div>
        </section>
    </div>
</div>