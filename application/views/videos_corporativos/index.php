<div class="page-wrapper main-content">

    <div class="wrapper">
        <nav class="breadcrumb" style="background-color: #fff;">
            <a href="<?php echo base_url(); ?>">
                Home
            </a>
            <a href="<?php echo base_url(); ?>en/corporate-videos">
                Corporate Videos
            </a>
        </nav>

        <h2 class="heading--gamma heading--inverse">
            Corporate Videos
        </h2>
        <ul class="faq-list videos">
            <?php foreach ($videosCorporativos as $video) : ?>
                <li>
                    <div class="item-question videos-corporativos">
                        <span><?= $video['title'] ?></span>
                        <span class="icon down"></span>
                    </div>
                    <div class="item-answer videos-to-show">
                        <video controls muted paused src="<?= $video['url'] ?>"></video>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    
</div>