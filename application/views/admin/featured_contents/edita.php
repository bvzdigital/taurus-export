<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">
			<li class="half first">
				<label for="status">Status</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?> <?php echo ($disabled) ? 'disabled' : ''; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<li>
				<label for="title">Título</label>
				<input type="text" class="required" name="title" value="<?php echo isset($dado->title) ? $dado->title : ''; ?>">

			</li>

			<li>
				<label for="language_iso">Idioma</label>
				<select name="language_iso" id="language_iso">
					<option value="en" <?php if (isset($dado) && $dado->language_iso == 'en') echo 'selected'; ?>>
						Inglês
					</option>
					<option value="es" <?php if (isset($dado) && $dado->language_iso == 'es') echo 'selected'; ?>>
						Espanhol
					</option>
				</select>
			</li>

			<li class="first half">
				<label for="target">Destino do link</label>
				<select id="target" name="target" <?php echo (@$dado->target == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?> <?php echo ($disabled) ? 'disabled' : ''; ?>>
					<option value="_blank" <?php if(@$dado->target == '_blank') echo 'selected="selected"'; ?> >Nova Janela</option>
					<option value="_self" <?php if(@$dado->target == '_self') echo 'selected="selected"'; ?> >Mesma Janela</option>
				</select>
			</li>
			<li>
				<label for="link">Link</label>
				<input type="text" name="link" value="<?php echo isset($dado->link) ? $dado->link : ''; ?>">
			</li>
			<li>
				<label for="order">Ordem</label>
				<input type="text" name="order" value="<?php echo isset($dado->order) ? $dado->order : ''; ?>">
				<span class="footnote">* Ex: 1. Ordem de exibição dos destaques no site</span>
			</li>

			<li class="first">
				<label for="text">Texto</label>
				<textarea cols="80" rows="7" name="text" class="tinyEditor"><?php echo isset($dado->text) ? $dado->text : ''; ?></textarea>
			</li>

		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">02. Imagem</h3>
		<ul class="fields-list">
			<?php if(@$dado->image && file_exists('assets/img/content/' . @$uri_super . '/' . @$dado->image)) : ?>
				<li class="first image">
					<img src="<?php echo 'assets/img/content/'.@$uri_super.'/'.@$dado->image; ?>" />
					<input type="hidden" name="current_image" value="<?php echo @$dado->image; ?>">
				</li>
				<?php if(file_exists('assets/img/content/' . @$uri_super . '/tn_' . @$dado->image)) : ?>
					<li class="image thumb">
						<img src="<?php echo 'assets/img/content/'.@$uri_super.'/tn_'.@$dado->image; ?>" />
					</li>
				<?php endif ?>
				<li class="first">
					<a href="<?php echo base_url() . 'admin/modulo/crop/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Crop Imagem</strong></a>
					<a href="<?php echo base_url() . 'admin/modulo/remove_img/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Excluir</strong></a>
				</li>
			<?php else : ?>
				<li class="file">
					<label for="file">Imagem</label>
					<input type="file" id="file" name="image"></input>
					<span class="footnote">* formatos permitidos: gif|jpg|png</span>
				</li>
			<?php endif ?>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>