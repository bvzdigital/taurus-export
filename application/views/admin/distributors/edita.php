<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">

			<!-- Status -->
			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<!-- Idioma -->
			<li class="half first">
				<label for="language">Idioma*</label>
				<select name="language">
					<option value="en"
						<?php if (isset($dado) && $dado->language_iso == 'en') echo $dado->language_iso; ?>>
						Inglês
					</option>
					<option value="es"
						<?php if (isset($dado) && $dado->language_iso == 'es') echo $dado->language_iso; ?>>
						Espanhol
					</option>
				</select>
			</li>

			<!-- Nome -->
			<li class="first">
			  <label for="name">Nome*</label>
			  <input class="required" name="name" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>

			<!-- Telefones -->
			<li class="first">
			  <label for="phones">Telefones</label>
			  <input name="phones" type="text" value="<?php echo isset($dado->phones) ? $dado->phones : ''; ?>"/>
			</li>

			<!-- Emails -->
			<li class="first">
			  <label for="emails">Emails</label>
			  <input name="emails" type="text" value="<?php echo isset($dado->emails) ? $dado->emails : ''; ?>"/>
			</li>

			<!-- Região -->
			<li class="first">
			  <label for="region">Região*</label>
			  <select name="region">
			  	<option value="">Selecione...</option>
			  	<?php foreach (@$regions as $region): ?>
					<option value="<?php echo $region->id; ?>"
						<?php if (@$dado->region_id == $region->id) echo 'selected'; ?>>
						<?php echo $region->name; ?>
					</option>
			  	<?php endforeach; ?>
			  </select>
			</li>

			<!-- País -->
			<li class="first">
			  <label for="country">País</label>
			  <input name="country" type="text" value="<?php echo isset($dado->country) ? $dado->country : ''; ?>"/>
			</li>

			<!-- Endereço -->
			<li class="first">
			  <label for="address">Endereço</label>
			  <input name="address" type="text" value="<?php echo isset($dado->address) ? $dado->address : ''; ?>"/>
			</li>

			<!-- Site -->
			<li class="first">
			  <label for="site">Site</label>
			  	<textarea cols="80" rows="5" name="site" class="tinyEditor">
					<?php echo isset($dado->site) ? $dado->site : ''; ?>
				</textarea>
			</li>

			<!-- Marca -->
			<li class="first">
			  <label for="brand">Marca*</label>
			  <input class="required" name="brand" type="text" value="<?php echo isset($dado->brand) ? $dado->brand : ''; ?>"/>
			</li>

			<!-- Latitude -->
			<li class="first">
			  <label for="latitude">Latitude</label>
			  <input name="latitude" type="text" value="<?php echo isset($dado->latitude) ? $dado->latitude : ''; ?>"/>
			</li>

			<!-- Longitude -->
			<li class="first">
			  <label for="longitude">Longitude</label>
			  <input name="longitude" type="text" value="<?php echo isset($dado->longitude) ? $dado->longitude : ''; ?>"/>
			</li>

		</ul>
	</fieldset>

	<?php if(isset($content->id) && ($content->id != '')) :?>
		<div class="upload-wrapper form-elements">
			<fieldset class="gray">
				<h3 class="form-title">02. Galeria de Fotos</h3>
				<form action="<?php echo base_url() . 'admin/upload/' . $uri_super; ?>" method="post" enctype="multipart/form-data">
					<input type="file" name="filename" class="fileUploadGallery" multiple>
					<input type="hidden" name="content_id" value="<?php echo @$content->id;?>" />
				</form>
				<div id="thumbs">
					<ul id="thumb-list" class="gallery" data-module="<?php echo $uri_super; ?>">
						<?php if(count(@$arrayGallery)) : ?>
							<?php foreach($arrayGallery as $image): ?>
								<li class="thumb" rel="<?php echo $image->id ?>">
									<span rel="event" id="<?php echo $image->id ?>" class="removeItemGallery" data-module="<?php echo $uri_super; ?>" title="Excluir imagem">X</span>
									<!-- <span rel="event" id="<?php echo $image->id ?>" class="editItemGallery" data-module="<?php echo $uri_super; ?>" title="Editar Legenda">E</span> -->
									<img src="<?php echo 'assets/img/content/' . $uri_super . '/' . $image->image; ?>" alt="Foto"/>
									<span class="caption"><?php echo $image->image ?></span>
									<form class="editItemGalleryForm" action="<?php echo 'admin/modulo/editar_legenda/' . @$uri_super; echo '/' . $image->id; ?>" method="post">
										<label>Legenda PT:</label>
										<input type="text" name="legenda_pt" value="<?php echo $image->legenda_pt; ?>" class="legendaImg"></input>
										<label>Legenda EN:</label>
										<input type="text" name="legenda_en" value="<?php echo $image->legenda_en; ?>" class="legendaImg"></input>
										<label>Legenda ES:</label>
										<input type="text" name="legenda_es" value="<?php echo $image->legenda_es; ?>" class="legendaImg"></input>
										<label class="retornoAtualiza"></label>
									</form>
								</li>
							<?php endforeach; ?>
						<?php endif; ?>
					</ul>
				</div>
			</fieldset>
		</div>
	<?php endif; ?>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>