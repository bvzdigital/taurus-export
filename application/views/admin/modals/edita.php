<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. Modal do tipo Texto</h3>
		<ul class="fields-list">
			<li class="first">
			  <label for="title">Título</label>
			  <input name="title" type="text" value="<?php echo isset($dado->title) ? $dado->title : ''; ?>"/>
			</li>
			<li class="first">
				<label for="description">Descrição*</label>
				<textarea cols="80" rows="40" name="description" class="tinyEditor">
					<?php echo isset($dado->description) ? $dado->description : ''; ?>
				</textarea>
			</li>

		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">02. Modal do tipo Imagem</h3>
		<ul class="fields-list">
			<?php if(@$dado->image && file_exists('assets/img/content/' . @$uri_super . '/' . @$dado->image)) : ?>
				<li class="first image">
					<img src="<?php echo 'assets/img/content/'.@$uri_super.'/'.@$dado->image; ?>" />
					<input type="hidden" name="current_image" value="<?php echo @$dado->image; ?>">
				</li>
				<?php if(file_exists('assets/img/content/' . @$uri_super . '/tn_' . @$dado->image)) : ?>
					<li class="image thumb">
						<img src="<?php echo 'assets/img/content/'.@$uri_super.'/tn_'.@$dado->image; ?>" />
					</li>
				<?php endif ?>
				<li class="first">
					<a href="<?php echo base_url() . 'admin/modulo/crop/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Crop Imagem</strong></a>
					<a href="<?php echo base_url() . 'admin/modulo/remove_img/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Excluir</strong></a>
				</li>
			<?php else : ?>
				<li class="file">
					<label for="file">Imagem</label>
					<input type="file" id="file" name="image"></input>
					<span class="footnote">* formatos permitidos: gif|jpg|png</span>
				</li>
			<?php endif ?>
		</ul>
	</fieldset>

	<fieldset class="gray">
		<ul class="fields-list">
			<!-- Idioma -->
			<li class="half first">
				<label for="language">Idioma*</label>
				<select name="language">
					<option value="en"
						<?php if (isset($dado) && $dado->language_iso == 'en') echo 'selected="selected"'; ?>>
						Inglês
					</option>
					<option value="es"
						<?php if (isset($dado) && $dado->language_iso == 'es') echo 'selected="selected"'; ?>>
						Espanhol
					</option>
				</select>
			</li>
			
			<li class="first">
			  <label for="link">Link</label>
			  <input name="link" type="text" value="<?php echo isset($dado->link) ? $dado->link : ''; ?>"/>
			</li>

			<li class="half first">
				<label for="target">Target</label>
				<select id="target" name="target">
					<option value="" <?php if(@$dado->target == '') echo 'selected="selected"'; ?> >Mesma página</option>
					<option value="_blank" <?php if(@$dado->target == '_blank') echo 'selected="selected"'; ?> >Nova aba</option>
				</select>
			</li>

			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>	
			<li class="first half">
				<label for="start">Inicio: </label>
				<input class="txtData" name="start" type="text" value="<?php echo isset($dado->start) ? dateToPt($dado->start) : ''; ?>"/>
			</li>
			<li class="half">
				<label for="end">Fim: </label>
				<input class="txtData" name="end" type="text" value="<?php echo isset($dado->end) ? dateToPt($dado->end) : ''; ?>"/>
			</li>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>