<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">
			<!-- Status -->
			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<!-- Idioma -->
			<li class="half first">
				<label for="idioma">Idioma*</label>
				<select name="idioma">
					<option value="en"
						<?php if (isset($dado) && $dado->language_iso == 'en') echo $dado->language_iso; ?>>
						Inglês
					</option>
					<option value="es"
						<?php if (isset($dado) && $dado->language_iso == 'es') echo $dado->language_iso; ?>>
						Espanhol
					</option>
				</select>
			</li>

			<li class="first">
			  <label for="faq_category">Categoria da pergunta* (ex: general, firearms, repairs, etc...)</label>
			  <input class="required" name="faq_category" type="text" value="<?php echo isset($dado->faq_category) ? $dado->faq_category : ''; ?>"/>
			</li>

			<li class="first">
			  <label for="question">Pergunta*</label>
			  <textarea cols="80" rows="40" name="question" class="tinyEditor">
			  	<?php echo isset($dado->question) ? $dado->question : ''; ?>
			  </textarea>
			</li>
			<li class="first">
			  <label for="answer">Resposta*</label>
			  <textarea cols="80" rows="40" name="answer" class="tinyEditor">
			  	<?php echo isset($dado->answer) ? $dado->answer : ''; ?>
			  </textarea>
			</li>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>