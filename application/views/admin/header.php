<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
<head>
<base href="<?php echo base_url(); ?>"/>
<title>Administra&ccedil;&atilde;o :: <?php echo @$title; echo (!empty($sub_title)) ? ' :: ' . $sub_title : ''; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta HTTP-EQUIV="Pragma" content="no-cache">
<meta HTTP-EQUIV="Expires" content="-1">
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<meta name="author" content="dEx Digital - www.dexdigital.com.br"/>

<link type="text/css" rel="stylesheet" href="assets/admin/css/normalize.css"/>
<link type="text/css" rel="stylesheet" href="assets/admin/css/main.css"/>
<link rel="stylesheet" href="assets/admin/css/form-elements.css" />

<link type="text/css" rel="stylesheet" href="assets/admin/plugins/jquery-ui/css/dex/estilos.css"/>
<link type="text/css" rel="stylesheet" href="assets/admin/plugins/simple-modal/css/simple-modal.css"/>
<link type="text/css" rel="stylesheet" href="assets/admin/plugins/fancybox/jquery.fancybox-1.3.4.css"/>

<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>-->
<script type="text/javascript" src="assets/admin/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="assets/admin/plugins/jquery-ui/js/jquery-ui-1.8.8.min.js"></script>

<script type="text/javascript" src="assets/admin/js/jquery.metadata.js"></script>
<script type="text/javascript" src="assets/admin/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="assets/admin/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="assets/admin/js/jquery.maskMoney.js"></script>  

<script type="text/javascript" src="assets/admin/plugins/data_table/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8" src="assets/admin/plugins/data_table/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="assets/admin/plugins/data_table/media/js/TableTools.js"></script>

<script type="text/javascript" src="assets/admin/plugins/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="assets/admin/plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<!-- <script type="text/javascript" src="assets/admin/plugins/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript" src="assets/admin/plugins/tiny_mce/tiny_mce.js"></script> -->

<script type="text/javascript" src="assets/admin/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="assets/admin/plugins/simple-modal/js/simple-modal.js"></script>
<script type="text/javascript" src="assets/admin/js/main.js"></script>

<link href="assets/admin/uploader/css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
<link href="assets/admin/uploader/css/fileUploader.css" rel="stylesheet" type="text/css" />
<script src="assets/admin/uploader/js/jquery.fileUploader.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/jquery_form.js"></script>    -->

<script type="text/javascript">
    tinymce.init({
        selector: 'textarea.tinyEditor',
        height: 250,
        width: 750,
        toolbar: 'styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor | code',
        plugins: 'advlist list table paste image code textcolor colorpicker link',
        language: 'pt_BR',
        relative_urls: false,
        convert_urls: false,
        remove_script_host : false,
        menubar: false
    });
</script>

<!-- <script type="text/javascript">
    tinyMCE.init({
        theme: "advanced",
        language: "pt",
        plugins : "table",
        plugins : "paste",
        theme_advanced_buttons3_add : "pastetext, pasteword, selectall",
        paste_auto_cleanup_on_paste : true,
        paste_preprocess : function(pl, o) {
            // Content string containing the HTML from the clipboard
            alert(o.content);
            o.content = "-: CLEANED :-\n" + o.content;
        },
        paste_postprocess : function(pl, o) {
            // Content DOM node containing the DOM structure of the clipboard
            alert(o.node.innerHTML);
            o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
        }, 
        paste_text_sticky : true,
        setup : function(ed) {
            ed.onInit.add(function(ed) {
              ed.pasteAsPlainText = true;
            });
        }, 
        tools: "inserttable",   
        theme_advanced_buttons1: "bold, italic, underline, strikethrough, separator, justifyleft, justifycenter, justifyright, justifyfull, separator, link, unlink, separator, table, bullist, undo, redo, cleanup,code",
        theme_advanced_buttons2: "",
        theme_advanced_buttons3: "",
        theme_advanced_buttons4: "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        mode : "textareas"
    }); 
</script> -->
</head>
<body 
    <?php echo !empty($uri_super) ? 'data-uri-super="' . $uri_super . '"' : ''; ?>
    <?php echo !empty($pag_type) ? 'data-pag-type="' . $pag_type . '"' : ''; ?> 
    <?php echo !empty($action_content) ? 'data-action-content="'.$action_content.'"' : ''; ?>
>
<div id="<?php if (!$this->session->userdata('logged')){ echo 'pageLogin'; } else { echo 'page'; } ?>">
	<?php if($this->session->userdata('logged')){ ?>
    <div id="header">
        <div class="lateral-direita"></div>        
        <div id="topo">
            <div id="identifica">
                <div class="logo">
                    <a href="admin"><img src="assets/admin/images/logo_pb.png" alt="dEx CMS" /></a>
                </div> 
                <h2>Ol&aacute;&nbsp;<strong><?php echo $this->session->userdata('NAME'); ?></strong><br /><span>Seja bem vindo a sua área administrativa</span></h2>
            </div>
        
            <div id="navigation">
                <ul class="sf-menu">
                    <?php 
                        $menu_total = count(@$menu);
                        foreach (@$menu as $key => $menuList) : 
                    ?>
						<li>							
							<a href="<?php echo !empty($menuList->menu_super) ? 'admin/modulo/'.$menuList->menu_super : '#'; ?>" title="<?php echo $menuList->menu; ?>">
                                <?php echo $menuList->menu; ?>
                            </a><?php echo (($key + 1) < $menu_total) ? '&nbsp;|&nbsp' : ''; ?>
							<?php								
                                $submenu = $this->admin_modules_model->get_menu(null, $menuList->id);
								if (count(@$submenu) > 0) :
							?>
									<ul class="sf-submenu">
										<?php foreach ($submenu as $submenuList) : ?>
											<li>
                                                <a href="<?php 
                                                            echo !empty($submenuList->menu_super) ? 'admin/modulo/'.$submenuList->menu_super : '#'; 
                                                            echo !empty($submenuList->menu_sub) ? '/sub/'.$submenuList->menu_sub : '';
                                                        ?>">
                                                    <?php echo $submenuList->menu; ?>
                                                </a>
                                            </li>
										<?php endforeach; ?>										
									</ul>
							<?php endif; ?>
						</li>						
					<?php endforeach; ?>
                </ul>
                <div class="ponta-azul"><img src="assets/admin/images/ponta-verde.png" alt="" width="15" height="35" /></div>
            </div>
        
            <div id="indica">
                <p>VOCÊ ESTÁ EM:</p>
                <div class="ponta-azul"><img src="assets/admin/images/ponta-cinza.png" alt="" width="15" height="35" /></div>
            </div>
            
            <ul id="bread" class="sf-menu">               
                <li><a href="<?php echo (@$uri_super != 'conteudo_padrao') ? 'admin/modulo/' . @$uri_super : 'admin/modulo/' . @$uri_super . '/sub/' . @$uri_sub; ?>" title="<?php echo @$title; ?>"><?php echo @$title; ?></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</li>                
                <?php if(isset($sub_title)){ ?>
                    <li><a href="<?php echo 'admin/modulo/' . @$uri_super . '/sub/' . @$uri_sub; ?>" title="<?php echo @$sub_title; ?>"><?php echo @$sub_title; ?></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</li>
                <?php } ?>                
                <li><?php echo @$section; ?></li>
            </ul>
            
            <ul id="login">
                <li>
                	<span class="l-um"><img src="assets/admin/images/l1-bt.png" alt="" width="5" height="25" /></span>
                    <a class="bt-user" href="admin/modulo/editar/admin_users/<?php echo $this->session->userdata('ID');?>"><span class="icon-user"></span>Perfil</a>
                    <span class="l-dois"><img src="assets/admin/images/l2-bt.png" alt="" width="5" height="25" /></span>
                </li>
                
                <li>
                	<span class="l-um"><img src="assets/admin/images/l1-bt.png" alt="" width="5" height="25" /></span>
                    <a class="bt-user" href="<?php echo base_url(); ?>" target="_blank"><span class="icon-site"></span>Ir para o site </a>
                    <span class="l-dois"><img src="assets/admin/images/l2-bt.png" alt="" width="5" height="25" /></span>
                </li>
                
                <li>
                	<span class="l-um"><img src="assets/admin/images/l1-bt.png" alt="" width="5" height="25" /></span>
                    <a class="bt-user" href="admin/home/logout"><span class="icon-login"></span>logout</a>
                    <span class="l-dois"><img src="assets/admin/images/l2-bt.png" alt="" width="5" height="25" /></span>
                </li>
            </ul>            
        </div>        
    </div>
<?php } ?>
<div id="content">