<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">			
	<h3 class="form-title">01. <?php echo@$title; ?></h3>
		<ul class="fields-list">			
			<li class="first">
			  <label for="nome">Nome*</label>
			  <input class="required" type="text" id="nome" name="nome" value="<?php echo isset($dado->name) ? $dado->name : ''; ?> "></input>
			</li>	
		</ul>
	</fieldset>	

	<fieldset class="gray">
		<h3 class="form-title">02. Permissões de acesso</h3>
		<ul class="fields-list">		
			<?php 
				if (@$dado->id == 1) : 
					echo '<li>*Grupo com permissões Gerais.</li>'; 
					echo '<input type="hidden" name="estado" value="BR" />';
				 else : 									
					if (count(@$menu) > 0 && ($this->session->userdata('GRUPO') != @$dado->id)) : 
			?>
					<li class="single-column"> 
						<ul class="checkboxes">
							<li><input type="checkbox" class="check-all" name="checkAll" value="0" /> <label for="checkAll">Selecionar Todos </label></li>
							<?php foreach(@$menu as $key => $menuList) : ?>
								<li>
									<input id="check-<?php echo ($key + 1); ?>" class="groupChecks parentCheckBox" type="checkbox" name="menus[]" value="<?php echo $menuList->id;?>" <?php echo in_array($menuList->id, $arrIdsMenusSel) ? 'checked="checked"': ''; ?> />
									<label for="check-<?php echo ($key + 1); ?>"><?php echo $menuList->menu; ?></label>							
									<?php 
										$submenu = $this->admin_modules_model->get_menu($this->session->userdata('GROUP'), $menuList->id);											
										if(count($submenu) > 0) :													
									?>
										<ul class="listaMenu">
											<?php foreach($submenu as $key2 => $submenuList) : ?>
												<li>
													<input id="check2-<?php echo ($key2 + 1); ?>" class="groupChecks childCheckBox" type="checkbox" name="menus[]" value="<?php echo $submenuList->id;?>" <?php echo in_array($submenuList->id, $arrIdsMenusSel) ? 'checked="checked"': ''; ?> />
													<label for="check2-<?php echo ($key2 + 1); ?>"><?php echo $submenuList->menu; ?></label>
												</li>												
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</li>						
							<?php endforeach;	?>
						</ul>
					</li>						
			<?php 
					endif; 
				endif; 
			?>	
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>			  		
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>			
</form>
<?php $this->load->view('admin/footer');?>