<link rel="stylesheet" href="assets/admin/css/form-elements.css" />
<script src="assets/admin/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>
<script src="assets/admin/js/saija.custom-tables.js" type="text/javascript"></script>
<link rel="stylesheet" href="assets/admin/css/saija.custom-tables.css" />
<link rel="stylesheet" href="assets/admin/plugins/fullcalendar/fullcalendar.css" />


<h2 class="section-title">Busca <span>Something relevant</span></h2>
<ul class="actions top">
	<li>
		<a href="admin/modal" class="gray-button large add-button open-modal">Cadastrar nova <strong>Instituição de Ensino</strong></a>
	</li>
</ul>
<form class="form-elements">
	<fieldset class="gray">
		<ul class="fields-list">
			<li class="first">
				<label for="cnpj">CNPJ</label>
				<input type="text" id="cnpj" name="cnpj"></input>
			</li>
			<li>
				<label for="nome">Nome</label>
				<input type="text" id="nome" name="nome"></input>
			</li>
			<li>
				<label for="programa">Programa</label>
				<select id="programa" name="programa">
					<option value=""></option>
					<option value="so-long-program">Programa com nome muito grande que acho que não cabe inteiro aqui :)</option>
					<option value="ppt">Powerpoint</option>
					<option value="doc">Word</option>
				</select>
			</li>
			<li class="uf first">
				<label for="estado">Estado</label>
				<select id="estado" name="estado">
					<option value=""></option>
					<option value="rs">RS</option>
				</select>
			</li>
			<li class="city">
				<label for="cidade">Cidade</label>
				<select id="cidade" name="cidade">
					<option value=""></option>
					<option value="viamao">Viamão</option>
				</select>
			</li>
			<li>
				<label>Status</label>
				<ul class="radios">
					<li><input type="radio" id="status-ativo" name="status" value="ativo" /><label for="status-ativo">Ativo</label></li>
					<li><input type="radio" id="status-inativo" name="status" value="inativo" /><label for="status-inativo">Inativo</label></li>
				</ul>
				<ul class="checkboxes">
					<li><input type="checkbox" id="status-bacon" name="bacon" /><label for="status-bacon">Somente com bacon</label></li>
				</ul>
			</li>
		</ul>
	</fieldset>
	<fieldset class="submit-button">
		<input type="submit" class="green-button search" value="Buscar" />
	</fieldset>
</form>
<h2 class="section-title"><span>Resultado</span> da busca</h2>
<div class="search-controls">
	<span class="search-results">Mostrando de <strong>01</strong> até <strong>05</strong> de <strong>100</strong> registros</span>
	<ul class="actions">
		<li>
			<label for="results-limit">Exibir</label>
			<select id="results-limit">
				<option value="5">5</option>
			</select>
		</li>
		<li>
			<a href="#something" class="gray-button"><strong>Copiar</strong> planilha</a>
		</li>
		<li>
			<a href="#something" class="gray-button"><strong>Exportar Planilha</strong> em excel</a>
		</li>
	</ul>
</div>
<table class="fancy-table">
	<thead>
		<tr>
			<th class="check"><input type="checkbox" id="check-all-1" class="tip" title="Selecionar Todos" /><label for="check-all-1">Selecionar Todos</label></th>
			<th>Nome</th>
			<th>Cidade/UF</th>			
			<th>Status</th>
			<th class="table-actions">Ações</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0;$i<5;$i++) { ?>
		<tr<?php echo ($i%2 ? ' class="odd"':"");?>>
			<td class="check"><input type="checkbox" id="reg-<?php echo $i; ?>" /><label for="reg-<?php echo $i; ?>">Selecionar</label></td>
			<td>Suspendisse Faucibus</td>
			<td>Porto Alegre/RS</td>			
			<td>
				<?php if($i % 3 === 0) $status = "success"; else if($i % 3 === 1) $status = "warning"; else $status = "error"; ?>
				<i class="status-element status-<?php echo $status ?>"></i>
			</td>
			<td class="actions">
				<a href="#" class="action tip password-generator" title="Gerar Senha">Gerar Senha</a>	
				<a href="#" class="action tip send-mail" title="Enviar Email">Enviar Email</a>					
				<a href="#" class="action tip add-people" title="Add Pessoas">Add Pessoas</a>	
				<a href="#" class="action tip calendar" title="Agendar">Agendar</a>
				<a href="#" class="action tip edit" title="Editar">Editar</a>
				<a href="#" class="action tip delete" title="Excluir">Excluir</a>
				<a href="#" class="action tip view" title="Visualizar">Visualizar</a>
				<a href="#" class="action tip tick" title="Aceitar">Aceitar</a>
				<a href="#" class="action tip inactive" title="Inativo">Inativo</a>
				<a href="#" class="action tip schedule" title="Agendar">Agendar</a>
			</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<table class="fancy-table">
	<thead>
		<tr>
			<th class="check"><input type="checkbox" id="check-all-1" class="tip" title="Selecionar Todos" /><label for="check-all-1">Selecionar Todos</label></th>
			<th>Nome</th>
			<th>Cidade/UF</th>
			<th>Tipo</th>
			<th>Grau de Ensino</th>
			<th>Status</th>
			<th class="table-actions">Ações</th>
		</tr>
	</thead>
	<tbody>
<?php for($i=0;$i<5;$i++) { ?>
		<tr<?php echo ($i%2 ? ' class="odd"':"");?>>
			<td class="check"><input type="checkbox" id="reg-<?php echo $i; ?>" /><label for="reg2-<?php echo $i; ?>">Selecionar</label></td>
			<td><input type="text" /></td>
			<td><input type="text" /></td>
			<td><select><option>lorem</option></select></td>
			<td><input type="radio" name="lorem-1"><label>lorem</label><input type="radio" name="lorem-1"><label>ipsum</label></td>
			<td><select></select></td>
			<td class="actions">				
				<a href="#" class="action tip edit" title="Editar">Editar</a>
				<a href="#" class="action tip delete" title="Excluir">Excluir</a>				
			</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<div class="search-controls bottom">
	<span class="search-results">Mostrando de <strong>01</strong> até <strong>05</strong> de <strong>100</strong> registros</span>
	<ul class="actions">
		<li><span class="back disabled">Anterior</span></li>
		<li class="numbers"><span>1</span></li>
		<li class="numbers"><a href="#something">2</a></li>
		<li class="numbers"><a href="#something">3</a></li>
		<li class="next"><a href="#next">Próxima</a></li>
	</ul>
</div>
<form class="form-elements">
	<fieldset class="gray">
		<h3 class="form-title">01. Dados pessoais</h3>
		<ul class="fields-list">
			<li class="first">
				<label for="cnpj">CNPJ</label>
				<input type="text" id="cnpj" name="cnpj"></input>
			</li>
			<li>
				<label for="nome">Nome</label>
				<input type="text" id="nome" name="nome"></input>
			</li>
			<li class="uf">
				<label for="estado">Estado</label>
				<select id="estado" name="estado">
					<option value=""></option>
					<option value="rs">RS</option>
				</select>
			</li>
			<li class="city">
				<label for="cidade">Cidade</label>
				<select id="cidade" name="cidade">
					<option value=""></option>
					<option value="viamao">Viamão</option>
				</select>
			</li>
			<li class="textarea">
				<label for="observacoes">Observações</label>
				<textarea id="observacoes"></textarea>
			</li>
			<li>
				<label>Radios e checkboxes, larguras iguais</label>
				<ul class="radios iso">
					<li><input type="radio" id="status-ativo2" name="status" value="ativo" /><label for="status-ativo2">Ativo</label></li>
					<li><input type="radio" id="status-inativo2" name="status" value="inativo" /><label for="status-inativo2">Inativo</label></li>
				</ul>
				<ul class="checkboxes iso">
					<li><input type="checkbox" id="status-bacon2" name="bacon" /><label for="status-bacon2">Bacon</label></li>
					<li><input type="checkbox" id="status-bacon3" name="bacon" /><label for="status-bacon3">Coffee</label></li>
					<li><input type="checkbox" id="status-bacon4" name="bacon" /><label for="status-bacon4">Peanut Butter</label></li>
					<li><input type="checkbox" id="status-bacon5" name="bacon" /><label for="status-bacon5">Jelly</label></li>
				</ul>
			</li>
			<li class="half">
				<label for="half-select">Half com select</label>
				<select id="half-select" name="half-select">
					<option value=""></option>
					<option value="rs">RS</option>
				</select>
			</li>
			<li class="half">
				<label for="displaced">Half text</label>
				<input type="text" id="displaced" name="displaced"></input>
			</li>
			<li class="cep">
				<label for="cep">CEP</label>
				<input type="text" id="cep" name="cep"></input>
				<a class="footnote" href="http://www.youtube.com/watch?v=07So_lJQyqw" target="_blank">Não sabe o seu CEP?</a>
			</li>
			<li class="dates">
				<label>Período</label>
				<label for="de">de</label>
				<input type="text" id="de" name="de"></input>
				<label for="ate">até</label>
				<input type="text" id="ate" name="ate"></input>
			</li>
			<li class="first">
				<label for="s-s">Class first para forçar quebra de linha</label>
				<input type="text" id="s-s" name="s-s"></input>
			</li>
			<li class="file">
				<label for="file">Selecione um arquivo</label>
				<input type="file" id="file" name="file"></input>
				<span class="footnote">* formatos permitidos: .doc .txt</span>
			</li>
			<li class="new-register">
				<a href="#somewhere" class="gray-button large add-button open-modal">Cadastrar nova <strong>Turma</strong></a>
			</li>
	</fieldset>
	<fieldset class="gray">
		<h3 class="form-title">02. Imagens</h3>
		<ul class="fields-list">
			<li class="first image">
				<img src="http://www.placecage.com/c/540/560" />
			</li>
			<li class="image thumb">
				<img src="http://www.placecage.com/c/540/560" />
			</li>
			<li class="first">
				<a href="#something" class="gray-button"><strong>Excluir</strong></a>
			</li>
		</ul>
	</fieldset>
	<fieldset class="gray">
		<h3 class="form-title">03. Listas</h3>
		<ul class="fields-list">
			<li class="single-column">
				<label>Lista single-column</label>
				<ul class="checkboxes">
					<li><input type="checkbox" id="check-1"><label for="check-1">Mazarópi</label></li>
					<li><input type="checkbox" id="check-2"><label for="check-2">Paulo Roberto</label></li>
					<li><input type="checkbox" id="check-3"><label for="check-3">Baidek</label></li>
					<li><input type="checkbox" id="check-4"><label for="check-4">De León</label></li>
					<li><input type="checkbox" id="check-5"><label for="check-5">P.C Magalhães</label></li>
					<li><input type="checkbox" id="check-6"><label for="check-6">China</label></li>
					<li><input type="checkbox" id="check-7"><label for="check-7">Osvaldo	</label></li>
					<li><input type="checkbox" id="check-8"><label for="check-8">Mário Sérgio</label></li>
					<li><input type="checkbox" id="check-9"><label for="check-9">Renato Gaúcho</label></li>
					<li><input type="checkbox" id="check-10"><label for="check-10">Tarciso</label></li>
					<li><input type="checkbox" id="check-11"><label for="check-11">Paulo César Lima	</label></li>
					<li><input type="checkbox" id="check-12"><label for="check-12">Valdir Espinosa</label></li>
				</ul>
			</li>
			<li class="single-column">
				<label>Lista single-column com scroll</label>
				<div class="scroll">
					<ul class="radios">
						<li><input type="radio" name="coffee" id="check-s-1"><label for="check-s-1">Mazarópi</label></li>
						<li><input type="radio" name="coffee" id="check-s-2"><label for="check-s-2">Paulo Roberto</label></li>
						<li><input type="radio" name="coffee" id="check-s-3"><label for="check-s-3">Baidek</label></li>
						<li><input type="radio" name="coffee" id="check-s-4"><label for="check-s-4">De León</label></li>
						<li><input type="radio" name="coffee" id="check-s-5"><label for="check-s-5">P.C Magalhães</label></li>
						<li><input type="radio" name="coffee" id="check-s-6"><label for="check-s-6">China</label></li>
						<li><input type="radio" name="coffee" id="check-s-7"><label for="check-s-7">Osvaldo	</label></li>
						<li><input type="radio" name="coffee" id="check-s-8"><label for="check-s-8">Mário Sérgio</label></li>
						<li><input type="radio" name="coffee" id="check-s-9"><label for="check-s-9">Renato Gaúcho</label></li>
						<li><input type="radio" name="coffee" id="check-s-10"><label for="check-s-10">Tarciso</label></li>
						<li><input type="radio" name="coffee" id="check-s-11"><label for="check-s-11">Paulo César Lima	</label></li>
						<li><input type="radio" name="coffee" id="check-s-12"><label for="check-s-12">Valdir Espinosa</label></li>
					</ul>
				</div>
			</li>
			<li class="two-columns">
				<label>Lista two-columns com scroll</label>
				<ul class="radios">
					<li><input type="radio" name="bacon" id="check-2-1"><label for="check-2-1">Mazarópi</label></li>
					<li><input type="radio" name="bacon" id="check-2-2"><label for="check-2-2">Paulo Roberto</label></li>
					<li><input type="radio" name="bacon" id="check-2-3"><label for="check-2-3">Baidek</label></li>
					<li><input type="radio" name="bacon" id="check-2-4"><label for="check-2-4">De León</label></li>
					<li><input type="radio" name="bacon" id="check-2-5"><label for="check-2-5">P.C Magalhães</label></li>
					<li><input type="radio" name="bacon" id="check-2-6"><label for="check-2-6">China</label></li>
					<li><input type="radio" name="bacon" id="check-2-7"><label for="check-2-7">Osvaldo	</label></li>
					<li><input type="radio" name="bacon" id="check-2-8"><label for="check-2-8">Mário Sérgio</label></li>
					<li><input type="radio" name="bacon" id="check-2-9"><label for="check-2-9">Renato Gaúcho</label></li>
					<li><input type="radio" name="bacon" id="check-2-10"><label for="check-2-10">Tarciso</label></li>
					<li><input type="radio" name="bacon" id="check-2-11"><label for="check-2-11">Paulo César Lima	</label></li>
					<li><input type="radio" name="bacon" id="check-2-12"><label for="check-12">Valdir Espinosa</label></li>
				</ul>
			</li>
			<li class="three-columns">
				<label>Lista three-column com scroll</label>
				<ul class="checkboxes">
					<li><input type="checkbox" id="check-3-1"><label for="check-3-1">Mazarópi</label></li>
					<li><input type="checkbox" id="check-3-2"><label for="check-3-2">Paulo Roberto</label></li>
					<li><input type="checkbox" id="check-3-3"><label for="check-3-3">Baidek</label></li>
					<li><input type="checkbox" id="check-3-4"><label for="check-3-4">De León</label></li>
					<li><input type="checkbox" id="check-3-5"><label for="check-3-5">P.C Magalhães</label></li>
					<li><input type="checkbox" id="check-3-6"><label for="check-3-6">China</label></li>
					<li><input type="checkbox" id="check-3-7"><label for="check-3-7">Osvaldo	</label></li>
					<li><input type="checkbox" id="check-3-8"><label for="check-3-8">Mário Sérgio</label></li>
					<li><input type="checkbox" id="check-3-9"><label for="check-3-9">Renato Gaúcho</label></li>
					<li><input type="checkbox" id="check-3-10"><label for="check-3-10">Tarciso</label></li>
					<li><input type="checkbox" id="check-3-11"><label for="check-3-11">Paulo César Lima	</label></li>
					<li><input type="checkbox" id="check-3-12"><label for="check-3-12">Valdir Espinosa</label></li>
				</ul>
			</li>
		</ul>
	</fieldset>
	<fieldset class="gray">
		<h3 class="form-title">04. Status</h3>
		<ul class="fields-list">
			<li><div><i class="status-element status-success"></i> - Sucesso</div></li>
			<li><div><i class="status-element status-warning"></i> - Alerta</div></li>
			<li><div><i class="status-element status-error"></i> - Erro</div></li>
			<li>
				<div>
					<i class="status-element status-success"></i><i class="status-element status-warning"></i><i class="status-element status-error"></i>
				</div>
			</li>
		</ul>
	</fieldset>
	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" disabled />
	</fieldset>
</form>
<div class="full-calendar-container">
	<h2 class="section-title search-controls">
		Agenda
		<div class="date-select-container">
			<label for="select-month-full-calendar">Mês / Ano: </label>
			<select id="select-month-full-calendar">
				<option value="mah-oe">Selecione</option>
			</select>
		</div>
	</h2>
	<div class="full-calendar"></div>
</div>
<form>
	<fieldset>
		<h2 class="section-title">Tabelas customizadas - relatório físico:</h2>
		<table class="fancy-table relatorio-fisico">
			<thead>
				<tr>
					<th>Período</th>
					<th>Estado&nbsp;</th>
					<th class="medium hidden city">Cidade</th>
					<th class="medium">Projeto</th>
					<th class="medium">Empresa</th>
					<th>Programa</th>
					<th class="can-delete">Alunos</th>
					<th class="can-delete">Turmas</th>
					<th class="can-delete">Volunt.</th>
					<th class="can-delete">Escolas</th>
					<th class="can-delete">Part.</th>
					<th class="can-delete">Coluna</th>
					<th class="can-delete">Coluna</th>
					<th class="can-delete">Coluna</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>00/00/00 00/00/00</td>
					<td><a href="#" data-has-childs=".childset-1" data-show-column=".city">UF <button class="action plus tip" title="Mais" data-title-toggle="Menos"></button></a></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr class="child childset-1">
					<td class="blank"></td>
					<td class="blank"></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr class="child childset-1">
					<td class="blank"></td>
					<td class="blank"></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr class="child childset-1">
					<td class="blank"></td>
					<td class="blank"></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr>
					<td>00/00/00 00/00/00</td>
					<td><a href="#" data-has-childs=".childset-2" data-show-column=".city">UF <button class="action plus tip" title="Mais" data-title-toggle="Menos"></button></a></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr class="child childset-2">
					<td class="blank"></td>
					<td class="blank"></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
				<tr class="child childset-2">
					<td class="blank"></td>
					<td class="blank"></td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>AEPRFS</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td>TOTAIS:</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
			</tfoot>
		</table>
		<script type="text/javascript">
			$('.relatorio-fisico').relatorioFisico()
		</script>
		<br style="clear: both; display: block;" />
		<h2 class="section-title" style="clear: both; margin-top: 10px;">Tabelas customizadas - relatório de gestão:</h2>
		<table class="fancy-table relatorio-gestao">
			<thead>
				<tr>
					<th class="medium">Status</th>
					<th class="medium">Projeto</th>
					<th>Estado</th>
					<th>Periodo</th>
					<th class="large">Meta</th>
					<th class="large">Realizadas</th>
					<th class="large">Agendadas</th>
					<th class="medium">Pendente</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>UF</td>
					<td>00/00/00 00/00/00</td>
					<td>
						<ul>
							<li>Aluno <mark>000</mark></li>
							<li>Turmas <mark>000</mark></li>
							<li>Voluntários <mark>000</mark></li>
							<li>Escola <mark>000</mark></li>
							<li>Professor <mark>000</mark></li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>Lorem Ipsum Dolor Site Amet</td>
					<td>UF</td>
					<td>00/00/00 00/00/00</td>
					<td>
						<ul class="counter">
							<li>Aluno <mark>000</mark></li>
							<li>Turmas <mark>000</mark></li>
							<li>Voluntários <mark>000</mark></li>
							<li>Escola <mark>000</mark></li>
							<li>Professor <mark>000</mark></li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
					<td>
						<ul class="counter">
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
							<li>000</li>
						</ul>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td>TOTAIS:</td>
					<td></td>
					<td></td>
					<td></td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
					<td>000</td>
				</tr>
			</tfoot>
		</table>
		<script type="text/javascript">
			$('.relatorio-gestao').relatorioGestao()
		</script>
	</fieldset>
</form>
<form class="form-elements">
	<h2 class="section-title">Deixe seu recado</h2>
	<fieldset>
		<div class="textarea-highlight">
			<textarea placeholder="Escreva sua mensagem"></textarea>
		</div>
	</fieldset>
	<fieldset class="submit-button">
		<input class="green-button" value="Publicar Recado" type="submit">
	</fieldset>
</form>
<h2 class="section-title">Recados publicados</h2>
<div class="message-box">
	<div class="message-author">
		Carlos da Silva
	</div>
	<time class="message-datetime" datetime="2014-01-20 08:00:00">20/01/2014 - 08:00</time>
	<div class="message-content">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, quod, illum, reiciendis, tempore qui quidem quae inventore recusandae et error temporibus excepturi eligendi vel labore ut magnam ea accusantium repellat. ipsum dolor sit amet, consectetur adipisicing elit. Nobis, dolorem harum nostrum soluta eaque possimus pariatur necessitatibus cum consectetur repudiandae quia ut rerum ex magni vero aperiam minima laudantium magnam. ipsum dolor sit amet, consectetur adipisicing elit. Nobis, reprehenderit natus ipsum sint numquam veniam ratione voluptatum praesentium adipisci amet nostrum commodi repudiandae architecto quibusdam ipsa voluptatem iste. Adipisci, eveniet?
	</div>
	<a href="#" class="message-delete">Excluir</a>
</div>
<div class="message-box">
	<div class="message-author">
		Suzana Martins
	</div>
	<time class="message-datetime" datetime="2014-01-20 08:00:00">20/01/2014 - 08:00</time>
	<div class="message-content">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium dolorum officiis reprehenderit quisquam itaque harum assumenda illum a consequuntur maiores! Molestiae consectetur eveniet accusantium porro ullam iusto cum minima libero. ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, quam at beatae perspiciatis facere cumque sunt. Ipsum, culpa, harum, autem molestiae at consequuntur id minus non rerum possimus tempora eum.
	</div>
	<a href="#" class="message-delete">Excluir</a>
</div>
<div class="message-box">
	<div class="message-author">
		Carlos da Silva
	</div>
	<time class="message-datetime" datetime="2014-01-20 08:00:00">20/01/2014 - 08:00</time>
	<div class="message-content">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, cum, repellat at facilis laboriosam rerum incidunt. Beatae, magnam, ipsam nihil ipsa consequatur repellendus veniam labore doloribus necessitatibus minus molestiae sunt.
	</div>
	<a href="#" class="message-delete">Excluir</a>
</div>
<div class="search-controls bottom">
	<span class="search-results">Mostrando de <strong>01</strong> até <strong>05</strong> de <strong>100</strong> registros</span>
	<ul class="actions">
		<li><span class="back disabled">Anterior</span></li>
		<li class="numbers"><span>1</span></li>
		<li class="numbers"><a href="#something">2</a></li>
		<li class="numbers"><a href="#something">3</a></li>
		<li class="next"><a href="#next">Próxima</a></li>
	</ul>
</div>
<?php $this->load->view('admin/footer'); ?>	