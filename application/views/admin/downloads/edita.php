<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">

			<!-- Status -->
			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<!-- Idioma -->
			<li class="half first">
				<label for="language">Idioma*</label>
				<select name="language">
					<option value="en"
						<?php if (isset($dado) && $dado->language_iso == 'en') echo $dado->language_iso; ?>>
						Inglês
					</option>
					<option value="es"
						<?php if (isset($dado) && $dado->language_iso == 'es') echo $dado->language_iso; ?>>
						Espanhol
					</option>
				</select>
			</li>

			<li class="first">
				<label for="type">Categoria</label>
				<select name="type" class="required">
					<option value="">Selecione...</option>
					<option value="posters" <?php if (isset($dado) && $dado->type == 'posters') echo 'selected'; ?>>
						Posters
					</option>
					<option value="catalog" <?php if (isset($dado) && $dado->type == 'catalog') echo 'selected'; ?>>
						Catalog
					</option>
					<option value="news" <?php if (isset($dado) && $dado->type == 'news') echo 'selected'; ?>>
						News
					</option>
					<option value="trade-mark" <?php if (isset($dado) && $dado->type == 'trade-mark') echo 'selected'; ?>>
						Trademark
					</option>
					<option value="media" <?php if (isset($dado) && $dado->type == 'media') echo 'selected'; ?>>
						Media
					</option>
				</select>
			</li>

			<!-- Nome -->
			<li class="first">
			  <label for="name">Nome</label>
			  <input name="name" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>

		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">02. Imagem</h3>
		<ul class="fields-list">
			<?php if(@$dado->image && file_exists('assets/img/content/' . @$uri_super . '/' . @$dado->image)) : ?>
				<li class="first image">
					<img src="<?php echo 'assets/img/content/'.@$uri_super.'/'.@$dado->image; ?>" />
					<input type="hidden" name="current_image" value="<?php echo @$dado->image; ?>">
				</li>
				<?php if(file_exists('assets/img/content/' . @$uri_super . '/tn_' . @$dado->image)) : ?>
					<li class="image thumb">
						<img src="<?php echo 'assets/img/content/'.@$uri_super.'/tn_'.@$dado->image; ?>" />
					</li>
				<?php endif ?>
				<li class="first">
					<a href="<?php echo base_url() . 'admin/modulo/crop/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Crop Imagem</strong></a>
					<a href="<?php echo base_url() . 'admin/modulo/remove_img/' . @$uri_super . '/' . @$dado->id; ?>" class="gray-button"><strong>Excluir</strong></a>
				</li>
			<?php else : ?>
				<li class="file">
					<label for="file">Imagem</label>
					<input type="file" id="file" name="image"></input>
					<span class="footnote">* formatos permitidos: gif|jpg|png</span>
				</li>
			<?php endif ?>
		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">03. Arquivo</h3>
		<ul class="fields-list">
			<?php if(@$dado->file) : ?>
				<li class="first">
					<input type="hidden" name="curent_download_file" value="<?php echo @$dado->file; ?>">
					<a href="<?php echo base_url().'assets/files/content/'.@$uri_super.'/'.@$dado->file; ?>" download><?php echo @$dado->file; ?></a>
				</li>
			<?php endif ?>
			<li class="first file">
				<label for="file2">Selecione um arquivo</label>
				<input type="file" id="file2" name="file2"></input>
				<span class="footnote">* formatos permitidos: pdf txt doc docx gif png jpg</span>
			</li>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>