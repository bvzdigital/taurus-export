<?php $this->load->view('admin/header');?>
<?php
	$img_inicial = $dado->image;
	$src = 'assets/img/content/' . $uri_super . '/' . $img_inicial;

	list($orig_width, $orig_height, $type, $attr) = getimagesize( $src );
	$targ_w = 218;
	$targ_h = 138;
	$proporcao = $orig_width / $targ_w;
	$proporcao = 1;

	if ($_SERVER['REQUEST_METHOD'] == 'POST'){
		$id = $_POST['id'];
		$src = $_POST['src'];
		$src_final = 'assets/img/content/' . $uri_super . '/tn_' . $_POST['src_final'];
		$targ_w = $_POST['targ_w'];
		$targ_h = $_POST['targ_h'];
		$jpeg_quality = 100;
		$ext = substr(strtolower($_POST['src_final']), -3);

		if ($ext == 'jpg' || $ext == 'jpeg'){
            $img_r = imagecreatefromjpeg($src);
        } else if ($ext == 'png'){
            $img_r = imagecreatefrompng($src);
        }
		$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x']*$_POST['proporcao'],$_POST['y']*$_POST['proporcao'],
		$targ_w,$targ_h,$_POST['w']*$_POST['proporcao'],$_POST['h']*$_POST['proporcao']);
		imagejpeg($dst_r,$src_final,$jpeg_quality);
		echo '<script>document.location="admin/modulo/editar/' . $uri_super . '/' . $id . '"</script>';
		exit;
	}
?>
<link rel="stylesheet" href="assets/admin/css/jquery.Jcrop.css" type="text/css" />
<script type="text/javascript" src="assets/admin/js/jquery.Jcrop.js"></script>
<script type="text/javascript">
	// Remember to invoke within jQuery(window).load(...)
	// If you don't, Jcrop may not initialize properly
	jQuery(window).load(function(){
		jQuery('#cropbox').Jcrop({
			onChange: showPreview,
			onSelect: showPreview,
			//minSize: [ <?php echo $targ_w/$proporcao; ?>, <?php echo $targ_h/$proporcao; ?> ],
			aspectRatio: <?php echo $targ_w; ?> / <?php echo $targ_h; ?>,
			setSelect: [ 0, 0, <?php echo $targ_w; ?>, <?php echo $targ_h; ?> ],
			onSelect: updateCoords
		});

	});

	function updateCoords(c)
	{
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	};

	// Our simple event handler, called from onChange and onSelect
	// event handlers, as per the Jcrop invocation above
	function showPreview(coords)
	{
		var rx = <?php echo round($targ_w/$proporcao); ?> / coords.w;
		var ry = <?php echo round($targ_h/$proporcao); ?> / coords.h;

		jQuery('#preview').css({
			width: Math.round(rx * <?php echo $orig_width; ?>) + 'px',
			height: Math.round(ry * <?php echo $orig_height; ?>) + 'px',
			marginLeft: '-' + Math.round(rx * coords.x * <?php echo $proporcao; ?>) + 'px',
			marginTop: '-' + Math.round(ry * coords.y * <?php echo $proporcao; ?>) + 'px'
		});
	}

	function verifica_corte(){
		if(document.crop.w.value >= <?php echo round($targ_w/$proporcao); ?> || document.crop.h.value >= <?php echo round($targ_h/$proporcao); ?>){
			document.crop.submit();
		} else {
			alert('Por favor, selecione uma área maior.');
		}
	}
</script>
		<div align="center">
			<img src="<?php echo 'assets/img/content/' . $uri_super . '/' . $dado->image; ?>" style="margin-right: 10px;" id="cropbox" alt="Create Thumbnail" name="cropbox" />
			<div style="border: solid 1px;width:<?php echo $targ_w; ?>px;height:<?php echo $targ_h; ?>px;overflow:hidden; margin-top:20px;">
				<img src="<?php echo $src; ?>" id="preview" />
		  	</div>
			<br style="clear:both;"/>
			<form name="thumbnail" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                <fieldset style="border:none;">
                	<input name="src_final" type="hidden" id="src_final" value="<?php echo $img_inicial; ?>" />
                    <input name="id" type="hidden" id="id" value="<?php echo $dado->id; ?>" />
                    <input name="src" type="hidden" id="src" value="<?php echo $src; ?>" />
                    <input type="hidden" id="proporcao" name="proporcao" value="<?php echo $proporcao; ?>" />
                    <input type="hidden" id="targ_w" name="targ_w" value="<?php echo $targ_w; ?>" />
			  		<input type="hidden" id="targ_h" name="targ_h" value="<?php echo $targ_h; ?>" />
                    <input type="hidden" name="x" value="" id="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <button type="submit" name="upload_thumbnail" value="Salvar" id="save_thumb" onclick="verifica_corte()" />Salvar Imagem</button>
                </fieldset>
			</form>
		</div>
<?php $this->load->view('admin/footer');?>