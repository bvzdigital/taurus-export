<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
		<h3 class="form-title">01. <?php echo @$title; ?></h3>		
		<ul class="fields-list">	
			<li class="half first">
				<label for="status">Status</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?> <?php echo ($disabled) ? 'disabled' : ''; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>				
			</li>
				
			<li class="first">
				<label for="user">Usuário*</label>
				<input class="required" <?php echo isset($dado->user) ? 'disabled' : ''; ?> type="text" id="user" name="user" value="<?php if (isset($dado->user)) { echo $dado->user; } else if ($this->session->userdata('new_email')) { echo $this->session->userdata('new_email'); } ?>"></input>
				<span class="footnote">* minúsculo e sem acentos. <br />* ex: nomeusuario ou nome@dominio.com.br </span>				
			</li>		

			<li>
			  <label for="nome">Nome*</label>
			  <input class="required" name="nome" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>	

			<li>
				<label for="email">Email*</label>
				<input class="email" type="text" id="email" name="email" value="<?php if (isset($dado->email)) { echo $dado->email; } else if ($this->session->userdata('new_email')) { echo $this->session->userdata('new_email'); } ?>"></input>
			</li>		
						
			<li class="first">
				<label for="senha">Senha*</label>
				<input <?php echo ($this->uri->segment(3) != 'editar') ? 'class="required"' : ''; ?> type="password" id="senha" name="senha"></input>
				<?php if ($this->uri->segment(3) == 'editar'){ echo '<span class="footnote">* para não alterar senha, deixe esse campo em branco.</span>'; } ?>
			</li>

			<li>
				<label for="senha2">Redigite sua senha*</label>
				<input type="password" id="senha2" name="senha2"></input>				
			</li>			
		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">02. Permissões de Acesso</h3>
		<ul class="fields-list">
			<?php 
				if (@$dado->id == 1) : $disabled = false;
					echo '<li>*Usuário com permissões Gerais.</li>'; 
					echo '<input type="hidden" name="estado" value="BR" />';
				 else : 	
				 	if (count(@$groups) > 0) : $disabled = false;				 	
			?>					
						<li class="first">
							<label for="grupo">Grupo*</label>
							<select class="required" id="selGroup" name="grupo">
								<?php foreach ($groups as $key => $group) : ?>
									<option <?php echo (@$dado->admin_group_id == @	$group->id) ? 'selected="selected"' : ''; ?> value="<?php echo @$group->id; ?>"><?php echo @$group->name; ?></option>
								<?php endforeach; ?>
							</select>						
						</li>	

						<li class="first"><label for="menus">Permissões para inserir, editar e deletar</label></li>
						<li class="single-column"> 	
							<ul class="checkboxes">
								<li><input type="checkbox" class="check-all" name="checkAll" value="0"/> <label for="checkAll">Selecionar Todos </label></li>
								<li id="liMenus">
									<?php if ((count(@$menuUser) > 0) && ($this->session->userdata('ID') != @$dado->id)) : ?>
										<ul>
											<?php foreach(@$menuUser as $key => $menuList) : ?>
												<li>
													<input id="check-<?php echo ($key + 1); ?>" class="groupChecks parentCheckBox" type="checkbox" name="menus[]" value="<?php echo $menuList->id; ?>" <?php echo in_array($menuList->id, @$arrIdsMenusSel) ? 'checked="checked"': ''; ?> />
													<label for="check-<?php echo ($key + 1); ?>"><?php echo $menuList->menu; ?></label>
													<?php 
														$submenus = $this->admin_modules_model->get_menu(@$dado->admin_group_id, $menuList->id);
														if (count($submenus) > 0) :													
													?>
														<ul class="listaMenu">
															<?php foreach ($submenus as $key2 => $submenuList) : ?>
																<li>
																	<input id="check2-<?php echo ($key2 + 1); ?>" class="groupChecks childCheckBox" type="checkbox" name="menus[]" value="<?php echo $submenuList->id;?>" <?php echo in_array($submenuList->id, @$arrIdsMenusSel) ? 'checked="checked"': ''; ?> />
																	<label for="check2-<?php echo ($key2 + 1); ?>"><?php echo $submenuList->menu; ?></label>
																</li>
															<?php endforeach; ?>
														</ul>
													<?php endif; ?>
												</li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>	
								</li>
							</ul>						
						</li>
					<?php else : $disabled = true;  ?>
						<li>
							<label>&nbsp;</label>	
							<span style="color:#ff0000">Nenhum grupo cadastrado.</span>
						</li>
					<?php endif; ?>
			<?php endif; ?>	
		</ul>
	</fieldset>
	
	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>			  
		<input type="hidden" name="url_ajax_modules" id="url_ajax_modules" value="<?php echo base_url().'admin/modulo/get_modules_groups/'; ?>" />			  	
		<input type="submit" class="green-button tick" <?php echo ($disabled) ? 'disabled="disabled"' : ''; ?> value="Finalizar" />
	</fieldset>					
</form>
<?php $this->load->view('admin/footer');?>