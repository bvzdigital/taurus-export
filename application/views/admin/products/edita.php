<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">

			<!-- Status -->
			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<!-- Idioma -->
			<li class="half first">
				<label for="language">Idioma*</label>
				<select name="language">
					<option value="en"
						<?php if (isset($dado) && $dado->language_iso == 'en') echo 'selected="selected"'; ?>>
						Inglês
					</option>
					<option value="es"
						<?php if (isset($dado) && $dado->language_iso == 'es') echo 'selected="selected"'; ?>>
						Espanhol
					</option>
				</select>
			</li>

			<!-- Nome -->
			<li class="first">
			  <label for="name">Nome*</label>
			  <input class="required" name="name" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>

			<!-- Subcategoria -->
			<li class="first">
				<label for="subcategory">Subcategoria*</label>
				<select name="subcategory">
					<option value="">Selecione...</option>
					<?php foreach($subcategories as $subcategory) : ?>
						<option value="<?php echo $subcategory->id; ?>" <?php if (isset($dado) && $dado->subcategory_id == $subcategory->id) echo 'selected'; ?>>
							<?php echo $subcategory->title; ?>
						</option>
					<?php endforeach; ?>
				</select>
			</li>

			<!-- Description -->
			<li class="first">
				<label for="description">Descrição*</label>
				<textarea cols="80" rows="20" name="description" class="tinyEditor">
					<?php echo isset($dado->description) ? $dado->description : ''; ?>
				</textarea>
			</li>

			<!-- Technical Information -->
			<li class="first">
				<label for="technical_specifications">Especificações Técnicas</label>
				<textarea cols="80" rows="20" name="technical_specifications" class="tinyEditor">
					<?php echo isset($dado->technical_specifications) ? $dado->technical_specifications : ''; ?>
				</textarea>
			</li>

			<!-- Video -->
			<li class="first">
				<label for="video">Vídeo (embed)</label>
				<input name="video" type="text" value="<?php echo isset($dado->video) ? $dado->video : ''; ?>"/>
			</li>

		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">02. Imagens do Produto (p/ Download)</h3>
		<ul class="fields-list">
			<li class="first">
				<input type="hidden" name="current_image_pack" value="<?php echo @$dado->image_pack; ?>">
				<a href="<?php echo base_url().'assets/files/content/'.@$uri_super.'/'.@$dado->image_pack; ?>" download><?php echo @$dado->image_pack; ?></a>
			</li>
			<li class="first">
				<label for="image_pack">Selecione um arquivo</label>
				<input type="file" name="image_pack"></input>
				<span class="footnote">* formatos permitidos: zip rar</span>
			</li>
		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">03. Manual do usuário</h3>
		<ul class="fields-list">
			<li class="first">
				<input type="hidden" name="current_promotional_pack" value="<?php echo @$dado->promotional_pack; ?>">
				<a href="<?php echo base_url().'assets/files/content/'.@$uri_super.'/'.@$dado->promotional_pack; ?>" download><?php echo @$dado->promotional_pack; ?></a>
			</li>
			<li class="first">
				<label for="promotional_pack">Selecione um arquivo</label>
				<input type="file" name="promotional_pack"></input>
				<span class="footnote">* formatos permitidos: zip rar</span>
			</li>
		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">04. Especificações do Produto (p/ Download)</h3>
		<ul class="fields-list">
			<li class="first">
				<input type="hidden" name="current_specs_document" value="<?php echo @$dado->specs_document; ?>">
				<a href="<?php echo base_url().'assets/files/content/'.@$uri_super.'/'.@$dado->specs_document; ?>" download><?php echo @$dado->specs_document; ?></a>
			</li>
			<li class="first">
				<label for="specs_document">Selecione um arquivo</label>
				<input type="file" name="specs_document"></input>
				<span class="footnote">* formatos permitidos: pdf</span>
			</li>
		</ul>
	</fieldset>

	<fieldset class="gray">
		<h3 class="form-title">05. Prêmios</h3>
		<ul class="fields-list">
			<?php foreach ($awards as $award): ?>
				<li class="first">
					<input type="checkbox" name="awards[]"
						value="<?php echo $award->id; ?>"
						<?php if (isset($product_awards) && in_array($award->id, $product_awards)) echo 'checked'; ?>>
						<?php echo $award->title; ?>
					</input>
				</li>
			<?php endforeach; ?>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>

	<?php if(isset($dado->id) && ($dado->id != '')) :?>
	<div class="upload-wrapper form-elements">
		<fieldset class="gray">
			<h3 class="form-title">06. Galeria de Imagens</h3>
			<form action="<?php echo base_url(); ?>admin/upload_products" method="post" enctype="multipart/form-data">
				<input type="file" name="filename" class="fileUploadProducts" multiple>
				<input type="hidden" value="<?php echo @$dado->id;?>" name="id"/>
			</form>
			<div id="thumbs">
				<ul id="thumb-list" class="products">
					<?php if(count($images)) : ?>
						<?php foreach($images as $image): ?>
							<li class="thumb" rel="<?php echo $image->id ?>">
								<span rel="event" id="<?php echo $image->id ?>" class="removeItemProducts" data-module="products">X</span>
								<img src="assets/img/content/products/tn_<?php echo  $image->url ?>" alt="Foto"/>
								<span class="caption"><?php echo $image->url ?></span>
							</li>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
			<p>* Arraste as imagens acima para ordená-las.</p>
		</fieldset>
	</div>
	<?php endif; ?>

<?php $this->load->view('admin/footer');?>