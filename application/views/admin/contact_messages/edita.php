<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">
			<li>
			  <label for="name">Nome*</label>
			  <input class="required" name="name" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>
			<li>
			  <label for="email">Email*</label>
			  <input class="required" name="email" type="text" value="<?php echo isset($dado->email) ? $dado->email : ''; ?>"/>
			</li>
			<li>
			  <label for="phone">Telefone*</label>
			  <input class="required" name="phone" type="text" value="<?php echo isset($dado->phone) ? $dado->phone : ''; ?>"/>
			</li>
			<li>
			  <label for="country">País*</label>
				<select id="country" name="country">
					<?php foreach(@$countries as $listaPaises){ ?>
					<option value="<?php echo $listaPaises->name; ?>" <?php if(@$dado->country == $listaPaises->name) echo 'selected="selected"'; ?> ><?php echo $listaPaises->name; ?></option>
					<?php } ?>
				</select>
			</li>
			<li class="first">
				<label for="message">Mensagem</label>
				<textarea cols="80" rows="7" name="message" class="tinyEditor"><?php echo isset($dado->message) ? $dado->message : ''; ?></textarea>
			</li>

		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>