<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">

			<!-- Status -->
			<li class="half first">
				<label for="status">Status*</label>
				<select id="status" name="status" <?php echo (@$dado->status == 0) ? 'style="background-color: #FFA500"' : 'style="background-color: #7FFF00"'; ?>>
					<option value="0" <?php if(@$dado->status == 0) echo 'selected="selected"'; ?> >Inativo</option>
					<option value="1" <?php if(@$dado->status == 1) echo 'selected="selected"'; ?> >Ativo</option>
				</select>
			</li>

			<!-- Título -->
			<li class="first">
			  <label for="title">Título</label>
			  <input name="title" type="text" value="<?php echo isset($dado->title) ? $dado->title : ''; ?>"/>
			</li>

			<!-- Description -->
			<li class="first">
				<label for="description">Descrição*</label>
				<textarea cols="80" rows="40" name="description" class="tinyEditor">
					<?php echo isset($dado->description) ? $dado->description : ''; ?>
				</textarea>
			</li>

		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>