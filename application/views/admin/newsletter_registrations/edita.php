<?php $this->load->view('admin/header'); ?>
<form class="form-elements" action="<?php echo 'admin/modulo/' . @$action_content . '/' . @$uri_super; echo (isset($dado->id)) ? '/' . $dado->id : ''; ?>" method="post" enctype="multipart/form-data" >
	<fieldset class="gray">
	<h3 class="form-title">01. <?php echo @$title; ?></h3>
		<ul class="fields-list">
			<li>
			  <label for="name">Nome*</label>
			  <input class="required" name="name" type="text" value="<?php echo isset($dado->name) ? $dado->name : ''; ?>"/>
			</li>
			<li>
			  <label for="email">Email*</label>
			  <input class="required" name="email" type="text" value="<?php echo isset($dado->email) ? $dado->email : ''; ?>"/>
			</li>
		</ul>
	</fieldset>

	<fieldset class="submit-button">
		<span class="footnote">* Preenchimento obrigatório</span>
		<input type="submit" class="green-button tick" value="Finalizar" />
	</fieldset>
</form>
<?php $this->load->view('admin/footer');?>