	<script type="text/javascript">
		$(document).ready(function(){
			var oTable = $("#table_id").dataTable({
				"sDom": 'T<"clear">lfrtip',
				"iDisplayLength": 25,
				"aLengthMenu": [[25, 50, 100, 200, -1], [25, 50, 100, 200, "Todos"]],
				"oTableTools": {
					"sSwfPath": "assets/admin/plugins/data_table/media/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
							"copy",
							{
								"sExtends": "xls",
								"sTitle": "Cadastros",
								"sFileName": "cadastros_<?php echo date("d_m_Y"); ?>.csv",
								"mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
							}
						]
				},
				"bAutoWidth": false,
				"sPaginationType": "full_numbers",
				"oLanguage": {
					"sProcessing":   "Processando...",
					"sLengthMenu":   "Mostrar _MENU_ registros",
					"sZeroRecords":  "Não foram encontrados resultados",
					"sInfo":         "Mostrando de <strong>_START_</strong> até <strong>_END_</strong> de <strong>_TOTAL_</strong> registros",
					"sInfoEmpty":    "Mostrando de <strong>0</strong> até <strong>0</strong> de <strong>0</strong> registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros no total)",
					"sInfoPostFix":  "",
					"sSearch":       "Buscar:",
					"sUrl":          "",
					"oPaginate": {
						"sFirst":    "",
						"sPrevious": "Anterior",
						"sNext":     "Seguinte",
						"sLast":     ""
					}
				}
			});
		//oTable.fnSetColumnVis( 1, false );
		});
	</script>

	<h2 class="section-title">Lista <span><?php echo @$title; ?></span></h2>

	<span class="msg-delete error">O registro possui vínculo com <span id="erroContent"></span> e não pode ser removido.</span>
	<table class="fancy-table" id="table_id">
		<thead>
			<tr>
				<!--<th class="table-actions">Selecionar</th>-->
				<?php foreach ($fields as $key => $val) : ?>
					<th><?php echo $key; ?></th>
				<?php endforeach ?>
				<?php if ($edit_module || $this->session->userdata('ID') == 1) : ?>
					<th class="table-actions">Ações</th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($list_content as $content) : ?>
				<tr>
					<?php foreach($fields as $key => $val) : ?>
						<?php if ($val == 'status') { ?>
							<td align="center"><?php echo showStatus($content->$val); ?></td>
						<?php } else if ($val == 'crop'){ ?>
								<td align="center">
									<?php if ($content->imagem) : ?>
										<a href="admin/modulo/crop/<?php echo $uri_super; ?>/<?php echo $content->id ?>"><img src="assets/admin/images/lin_agt_wrench.png" alt="Crop Imagem" title="Crop Imagem" /></a>
									<?php endif; ?>
								</td>
						<?php } else if ($val == 'categoria_id') { ?>
							<td><?php $categoria = getContent('exemplos_categorias', $content->$val); echo $categoria->nome; ?></td>
						<?php } else { ?>
							<td><?php echo $content->$val; ?></td>
						<?php } ?>
					<?php endforeach ?>
					<?php if ($edit_module || $this->session->userdata('ID') == 1) : ?>
						<td class="actions">
							<a href="<?php echo 'admin/modulo/editar/' . $uri_super . '/' . $content->id; ?>" class="action tip edit" title="Editar">Editar</a>
							<a href="<?php echo 'admin/modulo/deletar/' . $uri_super . '/' . $content->id ?>" class="action tip delete" title="Excluir">Excluir</a>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
