<div class="noticias--content">
	<div class="wrapper">
		<nav class="breadcrumb">
	        <a href="<?php echo site_url(); ?>">
	            Home
	        </a>
	        <a href="#" id="breadcrumb-categories">
	            News & Updates
	        </a>
	    </nav>
		<div class="main-content news-page">
			<h2 class="heading--alpha heading--inverse">
				News & Updates
			</h2>
		    <section class="news-list">
		    <?php
		    	foreach ($all_news as $news) : ?>
		    		<article class="noticias-box">
		    			<figure class="product-box__figure">
		    				<a href="<?php echo base_url() . $this->lang->lang() . '/' . 'news' . '/' . $news->slug; ?>">
		    					<img src="<?php echo base_url() . 'assets/img/content/news/tn_' . $news->image; ?>" alt="<?php echo $news->title ;?>" />
		    				</a>
		    			</figure>
		    			<a href="<?php echo base_url() . $this->lang->lang() . '/' . 'news' . '/' . $news->slug; ?>">
		    				<h3 class="noticias-box__title">
			    			  <!-- <span class="noticias-date"><?php echo date("d/m/y", strtotime($news->date));?></span><br> -->
		                      <?php echo $news->title; ?>
	                    	</h3>
	                    </a>
		    		</article>
		    	<?php endforeach ?>
		    </section>
		    <?php if(count($all_news) === 6) { ?>
		    	<a data-history="1" class="load-more-news" href="<?php echo base_url() . $this->lang->lang() ?>/news/page/">CARREGAR MAIS</a>
		    <?php } ?>
		</div>
	</div>
</div>