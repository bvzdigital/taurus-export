<div class="page-wrapper main-content">
<div class="wrapper">
    <nav class="breadcrumb">
        <a href="<?php echo site_url() ;?>">Home</a>
        <a href="<?php echo site_url() . '/news' ?>">Notícias</a>
    </nav>
    <section class="news-main">
	    <h2 class="heading--alpha heading--inverse">News</h2>
        <div class="social-media--wrapper">
            <div class="social-media-share">
                <div class="social-media-text">
                    <span class="share-text">
                        compartilhe
                    </span>
                </div>
                <a class="share-news-button facebook" target="_blank" rel="noopener" aria-label="Share on Facebook"  href="<?= $share_urls['facebook'] ?>"></a>
                <a class="share-news-button whatsapp" target="_blank" rel="noopener" aria-label="Share on Whatsapp"  href="<?= $share_urls['wpp'] ?>"></a>
                <a class="share-news-button twitter"  target="_blank" rel="noopener" aria-label="Share on Twitter" href="<?= $share_urls['twitter'] ?>"></a>
                <a class="share-news-button linkedin" target="_blank" rel="noopener" aria-label="Share on LinkedIN"  href="<?= $share_urls['linkedin'] ?>"></a>
            </div>
        </div>
	    <article>
	    	<h2>
	    		<?php echo $news->title; ?><br>
	    		<!-- <span class="news-date"><?php echo date("d/m/y", strtotime($news->date));?></span> -->
			</h2>
	    	<p><?php echo $news->description; ?></p>
	    </article>
    </section>
    <?php if(count($related_guns) > 0) { ?>
    <section class="related-products">
    	<h2>Produtos Relacionados</h2>
        <div>
    	<?php foreach ($related_guns as $gun) { ?>
               	<article class="list-box subcategory-box">
                    <header class="product-box__header">
                        <a href="<?php echo base_url() . 'en/products/' . $gun->cat_slug . '/' . $gun->subcategory_slug . '/' . $gun->slug;?>">
                            <figure class="product-box__figure_sub">
                                <img src="<?php echo base_url();?>assets/img/content/products/<?php echo $gun->image_url ?>" alt="<?php echo $gun->name; ?>">
                            </figure>

                            <h3 class="product-box__title">
                                 <?php echo $gun->name; ?>
                            </h3>
                        </a>
                    </header>
                    <p>
                        <?php echo limit_text($gun->description, 15); ?> 
                        <a href="<?php el_url('products', [$gun->cat_slug, $gun->slug]) ?>"><?php echo lang('categories.more'); ?></a>
                    </p>
                </article>
    	<?php } ?>
        </div>
    </section>
    <?php } ?>
</div>