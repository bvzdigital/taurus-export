<?php if($modal && count($modal) > 0):
    if($modal->title != ''): ?>
        <div class="modal-pl" style="display: none;">
            <div class="modal-inner">
                <span class="close">FECHAR</span>
            <?php if($modal->link != ''): ?>
                <a href="<?php echo $modal->link; ?>" <?php echo ($modal->target != '')? 'target="'.$modal->target.'"' : ''?>>
            <?php endif; ?>
                <article class="taurus-novo-local">
                    <h1><?php echo $modal->title; ?></h1>
                    <?php echo $modal->description; ?>
                </article>
            <?php if($modal->link != ''): ?>
                </a>
            <?php endif; ?>
            </div>
        </div>
    <?php else: ?>
        <div class="modal-pl" style="display: none;">
            <div class="modal-pl--inner">
                <?php if($modal->link != ''): ?>
                    <a href="<?php echo $modal->link; ?>" <?php echo ($modal->target != '')? 'target="'.$modal->target.'"' : ''?>>
                <?php endif; ?>
               <!-- <img src="<?php //echo base_url().'assets/img/content/modals/'.$modal->image; ?>">!-->
                <?php if($modal->link != ''): ?>
                    </a>
                <?php endif; ?>
                <span class="close"></span>
            </div>
        </div>
    <?php endif;
endif; ?>

<div class="page-wrapper">
    <div class="banner-full">
        <?php $i = 1; foreach ($banners as $listaBanner) { ?>
            <a href="<?= base_url() . $listaBanner->link ?>">
                <div class="banner-item banner-nav-item-<?php echo $i; ?> <?php if ($i == 1){ echo 'banner-item-active'; }?>" style="background-image: url('<?php echo base_url(); ?>assets/img/content/featured_contents/<?php echo $listaBanner->image; ?>');background-repeat:no-repeat;background-size: cover;width: 100%;">
                    <div class="banner-inner">
                        <div class="wrapper content-banner-box">
                            <article>
                                <h2 class="content-banner__title heading--beta heading--thin banner-caption"><?php echo $listaBanner->title; ?></h2>
                                <p class="banner-caption--abstract">
                                    <?= strip_tags($listaBanner->text) ?>
                                </p>
                            </article>
                        </div>
                    </div>
                </div>
            </a>
        <?php $i++; } ?>
        <div class="banner-nav">
            <div class="banner-wrapper">
            <ul>
                <?php $i = 1; foreach ($banners as $listaBanner) { ?>
                <li><a href="<?= base_url() . $listaBanner->link ?>" class="banner-nav-item <?php if ($i == 1){ echo 'active'; }?>" data-href=".banner-nav-item-<?php echo $i; ?>"><span><?php echo $listaBanner->title; ?></span></a></li>
                <?php $i++; } ?>
            </ul>
            </div>
        </div>
    </div>

    <div class="alert-full" style="display: none;">
        <div class="alert-wrapper">
            <a href="http://www.qualidadetaurus.com.br" target="_blank">
                <img src="<?php echo base_url().'assets/img/layout/text-qualidade-taurus-site.png'; ?>">
            </a>
        </div>
    </div>
<div class="wrapper wrapper-brand">
    <div class="wrapper-inner">
        <section class="brand-section">
            <header class="brand-section__header">
                <h2 class="heading--beta heading--no-margin">
                    <?php echo $text_line; ?>
                </h2>
                <p class="brand-section__text">
                    <?php echo $text; ?>
                </p>
            </header>
            <a class="bt bt--primary" href="<?php el_url('about-the-company') ?>">Read More</a>
        </section>
    </div>
</div>

<?php if (count(@$all_news) > 0) { ?>
    <div class="news-and-updates-wrapper">
        <div class="news-showcase">
            <a href="<?php echo base_url()?>en/news/">
                <h2 class="featured-title">News & Updates</h2>
            </a>
            <?php foreach ($all_news as $news): ?>
                <article class="news-box">
                    <header class="news-box__header">
                        <a href="<?php echo base_url() .  'en/news/' . $news->slug; ?>">
                            <figure class="news-box__figure">
                                <img src="<?php echo base_url() . 'assets/img/content/news/tn_' . $news->image; ?>"
                                        alt="<?php echo $news->title; ?>"/>
                            </figure>
                        </a>
                        <a href="<?php echo base_url() . 'en/news/' . $news->slug; ?>">
                            <h3 class="noticias-box__title">
                                <!-- <span class="noticias-date"><?php echo date("d/m/y", strtotime($news->date)); ?></span><br> -->
                                <?php echo $news->title; ?>
                            </h3>
                        </a>
                    </header>
                </article>
            <?php endforeach; ?>
        </div>
    </div>
<?php } ?>

<div id="video-institucional">
    <div class="video-institucional-inner">
        <video controls loop paused>
            <source src="<?= base_url() ?>assets/img/content/institutional_contents/TAURUS_80_ANOS_ENG.mp4" type="video/mp4">
            Your browser does not support this video.
        </video>
    </div>
</div>