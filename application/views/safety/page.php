<div class="safety-page-content">
    <div class="wrapper">
        <nav class="breadcrumb">
	        <a href="<?php echo site_url(); ?>">
	            Home
	        </a>
	        <a href="<?php echo site_url() ?>/safety" id="breadcrumb-categories">
	            Safety
            </a>
            <a href="#" id="breadcrumb-categories">
	            <?= $content->title ?>
	        </a>
        </nav>

        <div class="page-content-wrapper">
            <div class="title">
                <h2 class="heading--beta"><?= $content->title ?></h2>
            </div>
            <div class="safety-page-content-inner">
                <?= $content->description ?>
            </div>
        </div>
    </div>
</div>