<div class="safety--content">
	<div class="wrapper">
		<nav class="breadcrumb">
	        <a href="<?php echo site_url(); ?>">
	            Home
	        </a>
	        <a href="#" id="breadcrumb-categories">
	            Safety
	        </a>
        </nav>
        
        <div id="safety-wrapper">
            <div class="title">
                <h2 class="heading--beta">Safety</h2>
            </div>
            <div class="subtitle">
                <h3 class="featured-title">Basic keys to safe handling of firearms:</h3>
            </div>
            <div class="list-content">
                <div class="list-content-inner">
                    <div class="content-block">
                        <span>1. KEEP YOUR FINGER OFF THE TRIGGER UNTIL YOU ARE READY TO FIRE.</span>
                        <p>If you maintain good trigger discipline, it is extremely unlikely the firearm will fire.</p>
                    </div>

                    <div class="content-block">
                        <span>2. KEEP THE MUZZLE POINTED IN A SAFE DIRECTION.</span>
                        <p>Even if the firearm discharges, if the muzzle is pointed in a safe direction, no one will be injured.</p>
                    </div>

                    <div class="content-block">
                        <span>3. NEVER TRUST ANY SAFETY MECHANISM.</span>
                        <p>If you adopt the attitude that you will not trust any safety mechanism, you will be vigilant about keeping your finger off the trigger and keeping the muzzle pointed in a safe direction.</p>
                    </div>

                    <div class="content-block">
                        <span>4. KNOW WHAT IS BEHIND YOUR TARGET.</span>
                        <p>Unless you are shooting into a bullet trap designed to capture the type of ammunition you are firing, projectiles may pass through the target and strike anyone or anything beyond. Bullets can travel miles.</p>
                    </div>

                    <div class="content-block">
                        <span>5. ALWAYS WEAR HEARING PROTECTION.</span>
                        <p>Firearms are loud. The sound they generate is greater indoors. Repeated exposure to loud noises can cause deafness. Hearing protection is rated by decibel reduction. Look for “dB” reduction of 30 or more when purchasing ear protection. Modern ear protection can allow normal hearing until the sound from the discharge reaches the ear. Layering hearing protection, for example, wearing 30db rated foam plugs with 20db rated ear muffs, significant protection can be achieved.</p>
                    </div>

                    <div class="content-block">
                        <span>6. ALWAYS WEAR EYE PROTECTION.</span>
                        <p>In normal operation,firearms emit hot high-velocity gases, particles and metal. These materials flying into your eye can cause blindness. Protect your vision with proper eye protection. Any eye protection may be better than none, but look for ANSI Z87.1-2003 certified eyewear for the best protection.</p>
                    </div>

                    <div class="content-block">
                        <span>7. NEVER HANDLE FIREARMS WHILE UNDER THE INFLUENCE OF DRUGS OR ALCOHOL.</span>
                        <p>Firearms are dangerous. Handling firearms while impaired by drugs or alcohol is foolhardy and may result in serious injury or death. This warning includes prescription drugs that contain a warning about using machinery or driving while taking the medication.</p>
                    </div>

                    <div class="content-block">
                        <span>8. MODIFIED FIREARMS ARE DANGEROUS, AND USE CAN RESULT IN SERIOUS INJURY OR DEATH.</span>
                        <p>“Trigger jobs,” polishing original components or installing aftermarket parts can cause a firearm to function in ways different than intended. Some such work may also be illegal.</p>
                    </div>

                    <div class="content-block">
                        <span>9. DAMAGED FIREARMS ARE DANGEROUS, AND USE CAN RESULT IN SERIOUS INJURY OR DEATH.</span>
                        <p>Repairs should always be made by a qualified gunsmith or by a factory-authorized repair site. Work done by those unfamiliar with the design can cause a firearm to function in ways different than intended.</p>
                    </div>

                    <div class="content-block">
                        <span>10. NEVER ATTEMPT TO DISASSEMBLE OR CLEAN A LOADED FIREARM. DOING SO CAN RESULT IN SERIOUS BODILY INJURY OR DEATH.</span>
                        <p>How often have we heard “But it went off while I was cleaning it!” or, “I did not know it was loaded!” or, “It was jammed, and I tried to take it apart!”?</p>
                    </div>

                    <div class="content-block">
                        <span>11. IF A FIREARM GOES OFF, THERE WAS A ROUND OF AMMUNITION IN THE CHAMBER. PERIOD.</span>
                        <p>“Clear” (or empty) the firearm of all ammunition before disassembling or cleaning.</p>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>