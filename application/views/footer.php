
        <footer class="main-footer">
            <div class="wrapper main-footer__wrapper">
                <div class="a-b-columns">
                    <div class="c-column c-column--half">
                        <h2 class="featured-title no-margin--bottom">CONTACTS</h2>
                        <div class="d-column d-column-full">
                            <span class="tel-contact"><h4>Phone:</h4>+55 (51) 3021.3123 | +55 (51) 3021.3198</span>
                            <span class="tel-contact"><h4>Customer Service:</h4>export@taurus.com.br</span>
                        </div>
                    </div>
                    <div class="b-colum a-column--half">
                        <div class="d-column d-column-full">
                        <h2 class="featured-title no-margin--bottom">NEWSLETTER</h2>
                        <form id="newsletter-formulary" class="js-form newsletter-form default-form" data-url-action="<?php el_url('contact'); ?>/create-news">
                            <fieldset>
                                <label class="newsletter-form__label">
                                    <span class="visuallyhidden">
                                        <?php echo lang('footer.name_mandatory'); ?>
                                    </span>
                                    <input
                                        class="js-validate-required"
                                        type="text"
                                        placeholder="<?php echo lang('footer.name_mandatory'); ?>"
                                        name="name">
                                </label>
                                <label class="newsletter-form__label">
                                    <span class="visuallyhidden">
                                        <?php echo lang('footer.email_mandatory'); ?>
                                    </span>
                                    <input
                                        class="js-validate-required js-validate-email"
                                        type="email"
                                        placeholder="<?php echo lang('footer.email_mandatory'); ?>"
                                        name="email">
                                </label>
                                <div class="default-form__submit-area">
                                    <button class="bt bt--secondary footer-submit" type="submit">
                                        <?php echo lang('footer.Submit'); ?>
                                    </button>
                                </div>
                            </fieldset>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <section class="address">
            <div class="inner-address">
                <p class="contact-nav">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    Avenida São Borja, n° 2181 - Prédio A - Fazenda São Borja - CEP: 93035-411<br>
                    São Leopoldo - RS - Brasil<br>
                    <a class="see-maps" rel="external" href="https://www.google.com.br/maps/place/Av.+S%C3%A3o+Borja,+2181+-+Rio+Branco,+S%C3%A3o+Leopoldo+-+RS/@-29.7867307,-51.1192387,16z/data=!4m2!3m1!1s0x951969153707b197:0x1dad954d2acfcc28">
                        See on Map
                    </a>
                </p>
            </div>
        </section>
        <section class="copyright wrapper">
            <div class="wrapper table table--full">
                <div class="table__cell">
                    All Rights Reserved - Taurus Companies.
                </div>
                <div class="table__cell table__cell--r-align">
                    <a class="copyright__dex" href="http://dexdigital.com.br" rel="external">dEx - Especialista em Presença Digital</a>
                </div>
            </div>
        </section>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/fancybox/jquery.fancybox.pack.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.mixitup.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.cssmap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js?v=2"></script>
    </div> <!-- /page-wrapper -->

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-62384509-1');ga('send','pageview');
        </script>
    </body>
</html>