<div class="page-wrapper main-content">

<div class="wrapper downloads">
	<nav class="breadcrumb">
		<a href="<?php echo site_url(); ?>">
			Home 
		</a>
		<a href="<?php echo site_url(); ?>/pt/catalogo-produto">
            Product Catalog
		</a>
	</nav>

	<h2 class="heading--gamma heading--inverse">
		Product Catalog
    </h2>
    
	<section>
		<div class="list-showcase download-page">
			<?php foreach ($catalogos as $catalogo) : ?>
				<div class="mix list-box download-box">
					<header class="download-box__header">
						 <figure class="download-box__figure">
						 	<img src="<?= $catalogo['thumb'] ?>" alt="<?= $catalogo['title'] ?>" style="width:100%;">
						 </figure>
						 <time class="download-box__time"></time>
						 <h3 class="download-box__title">
                            <?= $catalogo['title'] ?>
						 </h3>
					</header>
					<a href="<?= $catalogo['url'] ?>" class="bt bt--secondary" target="_blank">See more</a>
				</div>
			<?php endforeach; ?>

		</div>

	</section>
</div>