<div class="page-wrapper main-content distrib">

<div class="wrapper pagecontact">
    <nav class="breadcrumb">
        <a href="<?php echo base_url(); ?>">
            Home
        </a>
        <a href="#">
            Dealers
        </a>
    </nav>

    <h2 class="heading--alpha heading--inverse">
        Dealers
    </h2>
    <nav class="distributors-nav" data-url="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2); ?>" data-active="<?php echo @$actualRegion; ?>">
        <?php foreach ($regions as $regioes) { ?>
        <a href="#" class="filter <?php echo $regioes->slug; ?> <?php if (@$actualRegion == $regioes->slug){ echo 'item-active'; } ?>" data-filter=".<?php echo $regioes->slug; ?>">
            <?php echo $regioes->name; ?>
        </a>
        <?php } ?>
    </nav>
    <div id="country-selector-wrapper">
        <div class="country-selector-inner">
            <select name="Country Selector" id="country-selector">
                <option data-region="0" value="0">Select a country</option>
                <?php foreach ($distributors as $key => $value) { ?>
                    <option data-region="<?= $value[0]->slug; ?>" value="<?= $value[0]->country_sigla; ?>"><?= $key ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    
    <div id="map-continents">
        <ul class="continents">
            <li data-filter=".africa" class="c1 filter"><a href="#africa" data-filter=".africa">Africa</a></li>
            <li data-filter=".asia" class="c2 filter"><a href="#asia" data-filter=".asia">Asia</a></li>
            <li data-filter=".australia" class="c3 filter"><a href="#australia" data-filter=".australia">Australia</a></li>
            <li data-filter=".europe" class="c4 filter"><a href="#europe" data-filter=".europe">Europe</a></li>
            <li data-filter=".north-america" class="c5 filter"><a href="#north-america" data-filter=".north-america">North America</a></li>
            <li data-filter=".south-america" class="c6 filter"><a href="#south-america" data-filter=".south-america">South America</a></li>
        </ul>
    </div>

    <div class="a-b-columns distributors-page">
	
        <?php if (isset($distributors)): ?>
            <?php foreach ($distributors as $key => $value){ ?>
            <div class="mix box-distributors <?php echo $value[0]->slug; ?>" data-country="<?= $value[0]->country_sigla; ?>">
                <div class="distributors-inner">
                    <header>
                        <h3 class="heading--distributors">
                            <?php echo $key; ?>
                        </h3>
                    </header>
                    <?php foreach ($value as $pais){ ?>
                        <div class="distributors-area">
                            <?php if ($pais->name <> ''){ ?>
                            <p><?php echo $pais->name; ?></p>
                            <?php } ?>
                            <?php if ($pais->address <> ''){ ?>
                            <p><?php echo $pais->address; ?></p>
                            <?php } ?>
                            <?php if ($pais->phones <> ''){ ?>
                            <p><?php echo $pais->phones; ?></p>
                            <?php } ?>
                            <?php if ($pais->emails <> ''){ ?>
                            <p><?php echo $pais->emails; ?></p>
                            <?php } ?>
                            <?php if ($pais->site <> ''){ ?>
                            <?php echo $pais->site; ?>                            
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        <?php endif; ?>
    </div>
</div>