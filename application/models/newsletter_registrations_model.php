<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Newsletter_registrations_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');
		$this->load->helper('inflector');
	}

	public function fields_grid(){
		$listaMenu = array('ID' => 'id', 'Nome' => 'name', 'Email' => 'email');
		return $listaMenu;
	}

	public function get_list(){
		$this->db->select('*')->from('newsletter_registrations');
		$query = $this->db->get();
		return $query->result();
	}

	public function edit($id){
		$this->db->select('*');
		$query = $this->db->get_where('newsletter_registrations' , array('id' => $id));
		return $query->row();
	}

	public function insert(){
		$data = array(
			'name'  	=> $this->input->post('name'),
			'email'		=> $this->input->post('email')
		);

		return $this->db->insert('newsletter_registrations', $data);
	}

	public function update($id){
		$data = array(
			'name'  	=> $this->input->post('name'),
			'email'		=> $this->input->post('email')
		);

		$this->db->where('id', $id);
		return $this->db->update('newsletter_registrations', $data);
	}

	public function delete($id){
		return $this->db->query("DELETE FROM newsletter_registrations WHERE id = ".$id);
	}
}