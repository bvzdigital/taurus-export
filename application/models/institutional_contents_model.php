<?php

class Institutional_contents_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'institutional_contents';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Página' => 'page',
			'Idioma' => 'language_iso',
			'Status' => 'status',
			'Imagem' => 'image'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);

		return $this->db->get()->result();
	}

	public function find_by_page($page)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->where(['page' => $page]);
		$this->db->limit(1);

		return $this->db->get()->row();
	}

	public function insert()
	{
		$img = $this->fetchImage();

		$data = [
			'text_line'			=> $this->input->post('text_line'),
			'text' 				=> $this->input->post('text'),
			'image' 			=> $img,
			'page' 				=> $this->input->post('page'),
			'language_iso' 		=> $this->input->post('language'),
			'status' 			=> $this->input->post('status')
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$img = $this->fetchImage();

		$data = [
			'text_line'			=> $this->input->post('text_line'),
			'text' 				=> $this->input->post('text'),
			'image' 			=> $img,
			'page' 				=> $this->input->post('page'),
			'language_iso' 		=> $this->input->post('language'),
			'status' 			=> $this->input->post('status')
		];

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		$this->deleteImage($id);

		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function crop($id)
	{
		return $this->edit($id);
	}

	public function uploadImg($file)
	{
		$config['upload_path']   	= 'assets/img/content/institutional_contents/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($file)) {
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/institutional_contents/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if ($this->image_lib->resize()) {
				return $data['file_name'];
			} else {
				echo $this->image_lib->display_errors();

				return "";
			}
		}

		return false;
	}

	public function search($terms){
		$parts = (array) preg_split('/\s+/', $terms);
		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND language_iso = '".$this->lang->lang()."' AND (page = 'about' AND UPPER(text) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(text) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";
		return $this->db->query($sql)->result();
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/institutional_contents/'.$img;
		$thumb_path = 'assets/img/content/institutional_contents/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

}