<?php

 class Featured_contents_model extends CI_Model {

 	private $table;

 	public function __construct() {
 		parent::__construct();

 		$this->table = 'featured_contents';
 	}

 	public function get_list() {
 		$this->db->select();
 		$this->db->from($this->table);

 		return $this->db->get()->result();
 	}

 	public function get_active() {
 		$this->db->select();
 		$this->db->from($this->table);
 		$this->db->where(['status' => 1]);
 		$this->db->where(['language_iso' => $this->lang->lang()]);
 		$this->db->order_by('order', 'ASC');

 		return $this->db->get()->result();
 	}

 	public function fields_grid() {
 		$lista_menu = array(
 			'Titulo' => 'title',
 			'Texto' => 'text',
 			'Link' => 'link',
 			'Ordem' => 'order',
 			'Idioma' => 'language_iso',
 			'Status' => 'status',
 			'Imagem' => 'image'
 		);

 		return $lista_menu;
 	}

 	public function insert() {
 		$img = $this->fetchImage();

		$dataInsert = array(
			'title'	 		=> $this->input->post('title'),
			'text'	 		=> $this->input->post('text'),
			'link'			=> $this->input->post('link'),
			'target'		=> $this->input->post('target'),
			'order'			=> $this->input->post('order'),
			'image'			=> $img,
			'language_iso'	=> $this->input->post('language_iso'),
			'status'		=> $this->input->post('status')
		);

		return $this->db->insert($this->table, $dataInsert);
 	}

 	public function update($id) {
 		$img = $this->fetchImage();

 		$data = array(
 			'title'	 		=> $this->input->post('title'),
			'text'	 		=> $this->input->post('text'),
			'link'			=> $this->input->post('link'),
			'target'		=> $this->input->post('target'),
			'order'			=> $this->input->post('order'),
			'image'			=> $img,
			'language_iso'	=> $this->input->post('language_iso'),
			'status'		=> $this->input->post('status')
 		);

 		$this->db->where('id', $id);

		return $this->db->update($this->table, $data);
 	}

 	public function delete($id) {
 		return $this->db->delete($this->table, array('id' => $id));
 	}

 	public function edit($id) {
 		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
 	}

 	public function crop($id)
	{
		return $this->edit($id);
	}

	public function uploadImg($file)
	{
		$config['upload_path']   	= 'assets/img/content/featured_contents/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($file)) {
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/featured_contents/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if ($this->image_lib->resize()) {
				return $data['file_name'];
			} else {
				echo $this->image_lib->display_errors();

				return "";
			}
		}

		return false;
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/featured_contents/'.$img;
		$thumb_path = 'assets/img/content/featured_contents/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

 }

 ?>