<?php

class Safety_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'safety';

		$this->load->helper('functions');
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Página' => 'title',
			'Identificador da Página' => 'slug',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);

		return $this->db->get()->result();
	}

	public function find_by_slug($slug)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['slug' => $slug]);
		$this->db->limit(1);

		return $this->db->get()->row();
	}

	public function insert()
	{
		$data = [
			'description'		=> $this->input->post('description'),
			'title' 			=> $this->input->post('title'),
			'status' 			=> $this->input->post('status')
		];

		$data['slug'] = normalizar($data['title']);

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$data = [
			'description'		=> $this->input->post('description'),
			'title' 			=> $this->input->post('title'),
			'status' 			=> $this->input->post('status')
		];

		$data['slug'] = normalizar($data['title']);

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function crop($id)
	{
		return $this->edit($id);
	}

	public function search($terms){
		$parts = (array) preg_split('/\s+/', $terms);
		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND (page = 'about' AND UPPER(text) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(text) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";
		return $this->db->query($sql)->result();
	}
}