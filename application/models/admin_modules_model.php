<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Modules_model extends CI_Model{

	public function __construct(){
        parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');
    }

    public function get_module_by_id($id){ 
		$this->db->select('*');
		$query = $this->db->get_where('admin_modules' , array('id' => $id));		
		return $query->row();		
	}	
	
	public function get_module_by_super($super){ 
		$this->db->select('*');
		$query = $this->db->get_where('admin_modules' , array('menu_super' => $super, 'menu_sub' => NULL)); 
		return $query->row();
	}	

	public function get_module_by_sub($sub){ 
		$this->db->select('*');
		$query = $this->db->get_where('admin_modules' , array('menu_sub' => $sub)); 
		return $query->row();
	}	

	public function get_module_by_super_sub($super, $sub){ 
		$this->db->select('*');
		$query = $this->db->get_where('admin_modules' , array('menu_super' => $super, 'menu_sub' => $sub)); 
		return $query->row();
	}	

	public function get_ids_permissions_group($group_id){
		$table = 'admin_modules_has_groups';
		$this->db->select('module_id');
		$this->db->from($table);
		$this->db->where('group_id', $group_id);
		$query = $this->db->get();		
		$res = $query->result();			
		$arrReturn = array();
		foreach($res as $row){
			$arrReturn[] = $row->module_id;
		}
		return $arrReturn;
	}
	
	public function get_permissions_user($module_id){ 
		if($this->session->userdata('ID') != 1){
			$this->db->select('*');
			$this->db->from('admin_modules_has_users');
			$this->db->where('module_id', $module_id);
			$this->db->where('user_id', $this->session->userdata('ID'));
			$query = $this->db->count_all_results();		
			return $query;
		}else{
			return 1;
		}
	}	

	public function get_ids_permissions_user($user_id){
		$table = 'admin_modules_has_users';
		$this->db->select('module_id');
		$this->db->from($table);
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();		
		$res = $query->result();			
		$arrReturn = array();
		foreach($res as $row){
			$arrReturn[] = $row->module_id;
		}
		return $arrReturn;
	}
	
	public function get_menu($group_id = null, $module_super_id = null){	
		$arrIdsModulesUser = $this->get_ids_permissions_user($this->session->userdata('ID'));

		$table = 'admin_modules';
		$table2 = 'admin_modules_has_groups';
		$this->db->select('*');
		$this->db->from($table);
		$this->db->join($table2, $table . '.id = ' . $table2 . '.module_id');
		
		if (!empty($group_id)) { 
			$this->db->where($table2 . '.group_id', $group_id);
		} else {
			$this->db->where($table2 . '.group_id', $this->session->userdata('GRUPO'));
		}

		$this->db->where_in($table . '.id', $arrIdsModulesUser);			
		$this->db->where($table . '.module_super_id', $module_super_id);				

		$this->db->group_by($table . '.id');
		$this->db->order_by($table . '.sequence ASC');
		$query = $this->db->get();	
		return $query->result();
	}	
}
?>