<?php
class Admin_Groups_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');		
	}

	public function fields_grid(){
		$listaMenu = array('ID' => 'id', 'Nome' => 'name');
		return $listaMenu;
	}
	
	public function get_list(){
		$table_name = 'admin_groups';		
		$this->db->select('*');
		$this->db->from($table_name);
		$query = $this->db->get();		
		return $query->result();
	}	

	public function edit($id){
		$this->db->select('*');
		$query = $this->db->get_where('admin_groups' , array('id' => $id));
		return $query->row();
	}
	
	public function insert(){
		$data = array(			
			'name' => $this->input->post('nome')
		);

		$this->db->insert('admin_groups' , $data);						

		if($this->input->post('menus')){	
			$id = mysql_insert_id();		
			$this->insert_permissions_groups($id);
		}
	}
	
	public function insert_permissions_groups($group_id){
		$data =array();
		$count = count($this->input->post('menus'));
		for($i = 0; $i < $count; $i++) {
			$menus = $this->input->post('menus');
			$data[$i] = array(
				'group_id' =>  $group_id,
			    'module_id' => $menus[$i]				   
			);
		}
		
		$this->db->insert_batch('admin_modules_has_groups' , $data);	
	}
	
	public function update($id){
		$data = array(			
			'name' => $this->input->post('nome')
		);
		
		$this->db->where('id', $id);
		$this->db->update('admin_groups', $data);
		$this->delete_permissions_groups($id);
		if($this->input->post('menus')){			
			$this->insert_permissions_groups($id);
		}
	}
	
	public function delete($id) {
		/* VERIFICA SE EXISTE RELACIONAMENTO COM USUÁRIOS */	
		$this->db->select('admin_group_id');
		$this->db->from('admin_users');	
		$this->db->where('admin_group_id', $id);		
		$query = $this->db->get();		
		$admin_users = $query->result();

		if(count($admin_users) > 0){				
			return 'usuários';
		}else{	
			$this->delete_permissions_groups($id);
			$delete = $this->db->query('DELETE FROM admin_groups WHERE id = '.$id);
			return true;
		}
	}
	
	public function delete_permissions_groups($id){
		$this->db->query('DELETE FROM admin_modules_has_groups WHERE group_id = '.$id);		
		return true;
	}	
}
?>