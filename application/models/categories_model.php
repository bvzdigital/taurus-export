<?php

class Categories_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'categories';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Marca' => 'brand',
			'Título' => 'title',
			'Idioma' => 'language_iso',
			'Status' => 'status',
			'Imagem' => 'image'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select('brands.name AS brand, categories.*');
		$this->db->from($this->table);
		$this->db->join('brands', 'categories.brand_id = brands.id');

		return $this->db->get()->result();
	}

	public function get_site()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->order_by('order', 'ASC');

		return $this->db->get()->result();
	}

	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);

		return $this->db->get()->result();
	}

	public function find_by_slug($slug)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->where(['slug' => $slug]);

		return $this->db->get()->row();
	}

	public function find($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function find_by_brand($brand)
	{
		$this->db->select('categories.*');
		$this->db->from($this->table);
		$this->db->join('brands', 'categories.brand_id = brands.id');
		$this->db->where([
			'categories.status' => 1,
			'categories.language_iso' => $this->lang->lang()
		]);
		$this->db->like('brands.name', $brand, 'both');

		return $this->db->get()->result();
	}

	public function insert()
	{
		$img = $this->fetchImage();

		$data = [
			'title' 		=> $this->input->post('title'),
			'abstract' 		=> $this->input->post('abstract'),
			'slug' 			=> strtolower(url_title(convert_accented_characters(trim($this->input->post('title'))))),
			'image' 		=> $img,
			'brand_id' 		=> $this->input->post('brand'),
			'language_iso' 	=> $this->input->post('language'),
			'order' 		=> $this->input->post('order'),
			'status' 		=> $this->input->post('status')
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$img = $this->fetchImage();

		$data = [
			'title' 		=> $this->input->post('title'),
			'abstract' 		=> $this->input->post('abstract'),
			'slug' 			=> strtolower(url_title(convert_accented_characters(trim($this->input->post('title'))))),
			'image' 		=> $img,
			'brand_id' 		=> $this->input->post('brand'),
			'language_iso' 	=> $this->input->post('language'),
			'order' 		=> $this->input->post('order'),
			'status' 		=> $this->input->post('status')
		];

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		$this->deleteImage($id);

		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function crop($id)
	{
		return $this->edit($id);
	}

	public function uploadImg($file)
	{
		$config['upload_path']   	= 'assets/img/content/categories/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($file)) {
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/categories/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if ($this->image_lib->resize()) {
				return $data['file_name'];
			} else {
				echo $this->image_lib->display_errors();

				return "";
			}
		}

		return false;
	}

	public function search($terms){
		$parts = (array) preg_split('/\s+/', $terms);
		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND language_iso = '".$this->lang->lang()."' AND (UPPER(title) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(title) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";
		return $this->db->query($sql)->result();
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/categories/'.$img;
		$thumb_path = 'assets/img/content/categories/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

}