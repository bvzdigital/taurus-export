<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Frequently_asked_questions_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');
		$this->load->helper('inflector');
	}

	public function fields_grid(){
		$listaMenu = array('ID' => 'id', 'Pergunta' => 'question', 'Resposta' => 'answer', 'Idioma' => 'language_iso', 'Status' => 'status');
		return $listaMenu;
	}

	public function get_list(){
		$this->db->select('*')->from('frequently_asked_questions');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_active() {
 		$this->db->select();
 		$this->db->from('frequently_asked_questions');
 		$this->db->where(['status' => 1]);
 		$this->db->where(['language_iso' => $this->lang->lang()]);

 		return $this->db->get()->result();
 	}

	public function edit($id){
		$this->db->select('*');
		$query = $this->db->get_where('frequently_asked_questions' , array('id' => $id));
		return $query->row();
	}

	public function insert(){
		$data = array(
			'question'  	=> $this->input->post('question'),
			'answer'		=> $this->input->post('answer'),
			'status'		=> $this->input->post('status'),
			'language_iso' 	=> $this->input->post('idioma'),
			'faq_category'  => $this->input->post('faq_category')
		);

		return $this->db->insert('frequently_asked_questions', $data);
	}

	public function update($id){
		$data = array(
			'question'  	=> $this->input->post('question'),
			'answer'			=> $this->input->post('answer'),
			'status'		=> $this->input->post('status'),
			'language_iso' 	=> $this->input->post('idioma'),
			'faq_category'  => $this->input->post('faq_category')
		);

		$this->db->where('id', $id);
		return $this->db->update('frequently_asked_questions', $data);
	}

	public function delete($id){
		return $this->db->query("DELETE FROM frequently_asked_questions WHERE id = ".$id);
	}
}