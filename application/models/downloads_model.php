<?php

class Downloads_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'downloads';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Nome' => 'name',
			'Categoria' => 'type',
			'Arquivo' => 'file',
			'Imagem' => 'image',
			'Idioma' => 'language_iso',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->order_by('id', 'DESC');
		$this->db->where('type !=', 'media');
		$this->db->where('type !=', 'trade-mark');

		return $this->db->get()->result();
	}

	public function find_by_type($type)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->where(['type' => $type]);

		return $this->db->get()->result();
	}

	public function insert()
	{
		$img = $this->fetchImage();

		$arquivo = $this->uploadFile('file2');

		if (isset($arquivo->error_msg)) {
			return false;
		}

		$data = [
			'type' => $this->input->post('type'),
			'file' => $arquivo['file_name'],
			'image' => $img,
			'name' => $this->input->post('name'),
			'language_iso' => $this->input->post('language'),
			'status' => $this->input->post('status')
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{

		$img = $this->fetchImage();

		$arquivo = $this->uploadFile('file2');

		if($_FILES['file2']['error'] == 0){

			if (isset($arquivo->error_msg)) {
				return false;
			}

			$return = $this->edit($id);
			if ($return->file != null && file_exists('assets/files/content/downloads/'.$return->file)){
				unlink('assets/files/content/downloads/'.$return->file);
			}

			$data = [
				'type' => $this->input->post('type'),
				'file' => $arquivo['file_name'],
				'image' => $img,
				'name' => $this->input->post('name'),
				'language_iso' => $this->input->post('language'),
				'status' => $this->input->post('status')
			];
		} else {
			$data = [
				'type' => $this->input->post('type'),
				'image' => $img,
				'name' => $this->input->post('name'),
				'language_iso' => $this->input->post('language'),
				'status' => $this->input->post('status')
			];
		}

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function crop($id){
		return $this->edit($id);
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function search($terms){
		$parts = (array) preg_split('/\s+/', $terms);
		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND language_iso = '".$this->lang->lang()."' AND (UPPER(name) LIKE UPPER('%".$parts[0]."%') OR UPPER(file) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(name) LIKE UPPER('%".$parts[$i]."%') OR UPPER(file) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";
		return $this->db->query($sql)->result();
	}

	private function uploadImg($file){
		$config['upload_path']   	= 'assets/img/content/downloads/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if($this->upload->do_upload($file)){
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/downloads/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if($this->image_lib->resize())
				return $data['file_name'];
			else{
				echo $this->image_lib->display_errors();
				return "";
			}
		}else{
			return false;
		}
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/downloads/'.$img;
		$thumb_path = 'assets/img/content/downloads/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

	private function uploadFile($file){
        $config['upload_path']      = 'assets/files/content/downloads/';
        $config['allowed_types']	= 'pdf|doc|docx|txt|gif|png|jpg|xls|xlsx';
        $config['max_size']         = '20000';
        $config['file_name']        = underscore(removeAccents(@$_FILES[$file]['name']));

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($file)) {
            $data = $this->upload->data();
            return $data;
        } else {
            return $this->upload;
        }
    }

}
