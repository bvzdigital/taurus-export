<?php

class Modals_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'modals';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Título' => 'title',
			'Imagem' => 'image',
			'Link' => 'link',
			'Inicio' => 'start',
			'Fim' => 'end',
			'Idioma' => 'language_iso',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get_modal()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where('status', 1);
		$this->db->where('language_iso', $this->lang->lang());
		$this->db->where('(start <= "'.date('Y-m-d').'" OR start IS NULL)');
		$this->db->where('(end >= "'.date('Y-m-d').'" OR end IS NULL)');
		$this->db->order_by('id', 'DESC');

		return $this->db->get()->result();
	}

	public function insert()
	{
		$img = $this->fetchImage();
		$data = array(
			'title' => $this->input->post('title'),
			'description' => $this->input->post('description'),
			'image' => $img,
			'link' => $this->input->post('link'),
			'target' => $this->input->post('target'),
			'start' => $this->input->post('start') ? dateToMysql($this->input->post('start')) : NULL,
			'end' => $this->input->post('end') ? dateToMysql($this->input->post('end')) : NULL,
			'language_iso' 		=> $this->input->post('language'),
			'status' => $this->input->post('status')
		);

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$img = $this->fetchImage();
		$data = array(
			'title' => $this->input->post('title'),
			'description' => $this->input->post('description'),
			'image' => $img,
			'link' => $this->input->post('link'),
			'target' => $this->input->post('target'),
			'start' => $this->input->post('start') ? dateToMysql($this->input->post('start')) : NULL,
			'end' => $this->input->post('end') ? dateToMysql($this->input->post('end')) : NULL,
			'language_iso' 		=> $this->input->post('language'),
			'status' => $this->input->post('status')
		);

		$this->db->where('id', $id);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(array('id' => $id));

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		$this->deleteImage($id);
		return $this->db->delete($this->table, array('id' => $id));
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	public function uploadImg($file)
	{
		$config['upload_path']   	= 'assets/img/content/modals/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload($file)) {
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/modals/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if ($this->image_lib->resize()) {
				return $data['file_name'];
			} else {
				echo $this->image_lib->display_errors();

				return "";
			}
		}

		return false;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/modals/'.$img;
		$thumb_path = 'assets/img/content/modals/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

}