<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class products_gallery_model extends CI_Model{

	public function __construct(){
        parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');
    }

	public function get_galeria($id){
		$table_name = "products_images";
		$this->db->select('*')->from($table_name);
		$query = $this->db->where(array('product_id' => $id));
		$query = $this->db->order_by('order ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get(){
		$table_name = "products_images";
		$this->db->select('*')->from($table_name);
		$query = $this->db->order_by('order ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_line($image_id){
		$table_name = "products_images";
		$this->db->select('*')->from($table_name);
		$query = $this->db->where(array('id' => $image_id));
		$query = $this->db->get();
		$query = $query->result();
		return $query[0];
	}

	public function insert($id, $path){
		$data['product_id'] =  $id;
		$data['url'] =  $path;
		$this->db->insert('products_images' , $data);
	}

	public function remove_item($image_id){
		$img = $this->get_line($image_id);
		if ($img->url <> ''){
			unlink('assets/img/content/products/'.$img->url);
		}
		$delete = $this->db->query('DELETE FROM products_images WHERE id = '.$image_id);
		return TRUE;
	}

	public function count_this($id){
		$this->db->where('product_id', $id);
		$this->db->from('products_images');
		$query = $this->db->count_all_results();
		return $query;
	}

	public function update_order($object_id, $order_no){
		$this->db->where('id', $object_id);
		$data['order'] = $order_no;
		$this->db->update('products_images' , $data);
	}

}
?>