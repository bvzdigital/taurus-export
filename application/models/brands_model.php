<?php

class Brands_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'brands';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Nome' => 'name',
			'Logo' => 'logo'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);

		return $this->db->get()->result();
	}

	public function insert()
	{
		$img = $this->fetchImage();

		$data = [
			'name' => $this->input->post('name'),
			'logo' => $img
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$img = $this->fetchImage();

		$data = [
			'name' => $this->input->post('name'),
			'logo' => $img
		];

		$this->db->where('id', $id);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function crop($id){
		return $this->edit($id);
	}

	public function delete($id)
	{
		$this->deleteImage($id);

		return $this->db->delete($this->table, ['id' => $id]);
	}

	private function uploadImg($file){
		$config['upload_path']   	= 'assets/img/content/brands/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if($this->upload->do_upload($file)){
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/brands/tn_'.$data['file_name'];
			$config['maintain_ratio']   = TRUE;
			$config['width']            = 140;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if($this->image_lib->resize())
				return $data['file_name'];
			else{
				echo $this->image_lib->display_errors();
				return "";
			}
		}else{
			return false;
		}
	}

	private function fetchImage()
	{
		if (isset($_FILES['logo'])) {
			$img = $this->uploadImg('logo');
		} else if ($this->input->post('current_logo')) {
			$img = $this->input->post('current_logo');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('logo');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->logo;
		$path = 'assets/img/content/brands/'.$img;
		$thumb_path = 'assets/img/content/brands/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}



}