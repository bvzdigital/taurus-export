<?php

class Regions_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'regions';
	}

	public function get_site()
	{
		$this->db->select();
		$this->db->from($this->table);
		//$this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->order_by('sequence', 'ASC');

		return $this->db->get()->result();
	}

	public function get()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->order_by('sequence', 'ASC');

		return $this->db->get()->result();
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, ['id' => $id]);
	}

}