<?php

class Distributors_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'distributors';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Nome' => 'name',
			'País' => 'country',
			'Marca' => 'brand',
			'Idioma' => 'language_iso',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select();
		$this->db->from($this->table);

		return $this->db->get()->result();
	}

	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);

		return $this->db->get()->result();
	}

	public function get()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->where(['language_iso' => $this->lang->lang()]);

		return $this->db->get()->result();
	}

	public function get_site($brand = NULL)
	{
		$this->db->select('distributors.*, regions.name AS region, regions.slug AS slug');
		$this->db->from($this->table);
		$this->db->join('regions', 'distributors.region_id = regions.id');
		$this->db->where(['distributors.status' => 1]);
		$this->db->where(['distributors.language_iso' => $this->lang->lang()]);
		if ($brand){
			$this->db->where(['distributors.brand' => $brand]);
		}
		$this->db->order_by('distributors.country', 'ASC');
		return $this->db->get()->result();
	}

	public function find_by_region($region = NULL)
	{
		$this->db->select('distributors.*, regions.name AS region');
		$this->db->from($this->table);
		$this->db->join('regions', 'distributors.region_id = regions.id');
		$this->db->where(['regions.id' => $region]);
		$this->db->where(['distributors.status' => 1]);
		$this->db->where(['distributors.language_iso' => $this->lang->lang()]);
		$this->db->order_by('distributors.region_id', 'ASC');
		$this->db->order_by('distributors.country', 'ASC');

		return $this->db->get()->result();
	}

	public function insert()
	{
		$data = [
			'name' => $this->input->post('name'),
			'phones' => $this->input->post('phones'),
			'emails' => $this->input->post('emails'),
			'region_id' => $this->input->post('region'),
			'country' => $this->input->post('country'),
			'address' => $this->input->post('address'),
			'site' => $this->input->post('site'),
			'brand' => $this->input->post('brand'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'language_iso' => $this->input->post('language'),
			'status' => $this->input->post('status')
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$data = [
			'name' => $this->input->post('name'),
			'phones' => $this->input->post('phones'),
			'emails' => $this->input->post('emails'),
			'region_id' => $this->input->post('region'),
			'country' => $this->input->post('country'),
			'address' => $this->input->post('address'),
			'site' => $this->input->post('site'),
			'brand' => $this->input->post('brand'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'language_iso' => $this->input->post('language'),
			'status' => $this->input->post('status')
		];

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function search($terms){
		$parts = (array) preg_split('/\s+/', $terms);
		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND language_iso = '".$this->lang->lang()."' AND (UPPER(name) LIKE UPPER('%".$parts[0]."%') OR UPPER(country) LIKE UPPER('%".$parts[0]."%') OR UPPER(address) LIKE UPPER('%".$parts[0]."%') OR UPPER(site) LIKE UPPER('%".$parts[0]."%') OR UPPER(brand) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(name) LIKE UPPER('%".$parts[$i]."%') OR UPPER(country) LIKE UPPER('%".$parts[$i]."%') OR UPPER(address) LIKE UPPER('%".$parts[$i]."%') OR UPPER(site) LIKE UPPER('%".$parts[$i]."%') OR UPPER(brand) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";
		return $this->db->query($sql)->result();
	}

}