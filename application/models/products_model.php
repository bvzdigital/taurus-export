<?php

class Products_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'products';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Nome' => 'name',
			'Subcategoria' => 'subcategory',
			'Idioma' => 'language_iso',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select('products.*, subcategories.title AS subcategory');
		$this->db->from($this->table);
		$this->db->join('subcategories', 'products.subcategory_id = subcategories.id');

		return $this->db->get()->result();
	}

	public function find_by_category($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['products.subcategory_id' => $id, 'status' => 1]);
		return $this->db->get()->result();
	}  

	public function find_by_subcategory($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['subcategory_id' => $id, 'status' => 1]);

		return $this->db->get()->result();
	}

	public function find_by_slug($slug)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['slug' => $slug]);
		$this->db->where(['language_iso' => $this->lang->lang()]);

		return $this->db->get()->row();
	}

	public function find_by_news($id, $limit = null) {
		$this->db->select('products.*, products_images.url as image_url, categories.slug as cat_slug');
		$this->db->from($this->table);
		$this->db->join('news_products', 'news_products.id_produto = products.id');
		$this->db->where('news_products.id_noticia', $id);
		$this->db->join('products_images', 'products_images.product_id = news_products.id_produto');
		$this->db->join('categories', 'categories.id = products.subcategory_id');
		$this->db->group_by('products.id');

		if ($limit !== 'all') {
			$this->db->limit(3);
		}

		return $this->db->get()->result();
	}

	public function insert()
	{
		$files = $this->fetchFiles();

		$data = [
			'name' => $this->input->post('name'),
			'slug' => strtolower(url_title(convert_accented_characters(trim($this->input->post('name'))))),
			'subcategory_id' => $this->input->post('subcategory'),
			'description' => $this->input->post('description'),
			'technical_specifications' => $this->input->post('technical_specifications'),
			'video' => $this->input->post('video'),
			'language_iso' => $this->input->post('language'),
			'status' => $this->input->post('status'),
			'image_pack' => $files['image_pack'],
			'promotional_pack' => $files['promotional_pack'],
			'specs_document' => $files['specs_document']
		];

		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();

		$this->load->model('products_awards_model');

		$this->products_awards_model->delete($id);

		if ($this->input->post('awards')) {
			foreach ($this->input->post('awards') as $award) {
				$this->products_awards_model->create($id, $award);
			}
		}
	}

	public function update($id)
	{
		// no fetchFiles pega os novos que estão para serem cadastrados
		// $files = $this->fetchFiles($id);

		// no post pega os que já estão cadastrados.
		// $this->input->post()

		$data = [
			'name' => $this->input->post('name'),
			'slug' => strtolower(url_title(convert_accented_characters(trim($this->input->post('name'))))),
			'subcategory_id' => $this->input->post('subcategory'),
			'description' => $this->input->post('description'),
			'technical_specifications' => $this->input->post('technical_specifications'),
			'video' => $this->input->post('video'),
			'language_iso' => $this->input->post('language'),
			'status' => $this->input->post('status')
		];

		$files = $this->fetchFiles($id);

		if( !empty($this->input->post('current_image_pack')) && empty($files['image_pack']) ) {
			$data['image_pack'] = $this->input->post('current_image_pack');
		}
		if( empty($this->input->post('current_image_pack')) && !empty($files['image_pack']) ) {
			$data['image_pack'] = $files['image_pack'];
		}
		if( !empty($this->input->post('current_image_pack')) && !empty($files['image_pack']) ) {
			$data['image_pack'] = $files['image_pack'];
		}

		if( !empty($this->input->post('current_promotional_pack')) && empty($files['promotional_pack']) ) {
			$data['promotional_pack'] = $this->input->post('current_promotional_pack');
		}
		if( empty($this->input->post('current_promotional_pack')) && !empty($files['promotional_pack']) ) {
			$data['promotional_pack'] = $files['promotional_pack'];
		}
		if( !empty($this->input->post('current_promotional_pack')) && !empty($files['promotional_pack']) ) {
			$data['promotional_pack'] = $files['promotional_pack'];
		}

		if( !empty($this->input->post('current_specs_document')) && empty($files['specs_document']) ) {
			$data['specs_document'] = $this->input->post('current_specs_document');
		}
		if( empty($this->input->post('current_specs_document')) && !empty($files['specs_document']) ) {
			$data['specs_document'] = $files['specs_document'];
		}
		if( !empty($this->input->post('current_specs_document')) && !empty($files['specs_document']) ) {
			$data['specs_document'] = $files['specs_document'];
		}

		$this->db->where('id', $id);

		$this->db->update($this->table, $data);

		$this->load->model('products_awards_model');

		$this->products_awards_model->delete($id);

		if (@$this->input->post('awards')){
			foreach (@$this->input->post('awards') as $award) {
				$this->products_awards_model->create($id, $award);
			}
		}
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function delete($id)
	{
		$this->deleteFiles($id);

		return $this->db->delete($this->table, ['id' => $id]);
	}

	public function search($terms)
	{
		$parts = (array) preg_split('/\s+/', $terms);

		$sql = "SELECT * FROM ".$this->table." WHERE status = 1 AND language_iso = '".$this->lang->lang()."' AND (UPPER(name) LIKE UPPER('%".$parts[0]."%') OR UPPER(description) LIKE UPPER('%".$parts[0]."%') OR UPPER(technical_specifications) LIKE UPPER('%".$parts[0]."%')";
		$len = sizeof($parts);

		for($i = 1; $i<$len; $i++){
			$sql = $sql . " OR UPPER(name) LIKE UPPER('%".$parts[$i]."%') OR UPPER(description) LIKE UPPER('%".$parts[$i]."%') OR UPPER(technical_specifications) LIKE UPPER('%".$parts[$i]."%')";
		}

		$sql .= ")";

		return $this->db->query($sql)->result();
	}

	private function uploadFile($file)
	{
        $config['upload_path']      = 'assets/files/content/products/';
        $config['allowed_types']	= 'pdf|doc|docx|txt|gif|png|jpg|zip|rar|xls|xlsx';
        $config['max_size']         = '20000';
        $config['file_name']        = underscore(removeAccents(@$_FILES[$file]['name']));

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($this->upload->do_upload($file)){
            $data = $this->upload->data();
            return $data;
        }else{
            return $this->upload;
        }
    }

    private function fetchFiles($id = null)
    {
    	if ($id != null) {
    		$return = $this->edit($id);
    	}

    	$files = [];
		$image_pack = $this->uploadFile('image_pack');
		$promotional_pack = $this->uploadFile('promotional_pack');
		$specs_document = $this->uploadFile('specs_document');

		if(isset($image_pack->error_msg)){
			$files['image_pack'] = '';
		} else {
			$files['image_pack'] = $image_pack['file_name'];

			if ($id != null && ! empty($return->image_pack) && file_exists('assets/files/content/products/'.$return->image_pack)) {
	    		unlink('assets/files/content/products/'.$return->image_pack);
	    	}
		}

		if(isset($promotional_pack->error_msg)){
			$files['promotional_pack'] = '';
		} else {
			$files['promotional_pack'] = $promotional_pack['file_name'];

			if ($id != null && ! empty($return->promotional_pack) && file_exists('assets/files/content/products/'.$return->promotional_pack)) {
	    		unlink('assets/files/content/products/'.$return->promotional_pack);
	    	}
		}

		if(isset($specs_document->error_msg)){
			$files['specs_document'] = '';
		} else {
			$files['specs_document'] = $specs_document['file_name'];

			if ($id != null && ! empty($return->specs_document) && file_exists('assets/files/content/products/'.$return->specs_document)) {
	    		unlink('assets/files/content/products/'.$return->specs_document);
	    	}
		}

		return $files;
    }

    private function deleteFiles($id)
    {
    	$return = $this->edit($id);

    	if (! empty($return->image_pack) && file_exists('assets/files/content/products/'.$return->image_pack)) {
    		unlink('assets/files/content/products/'.$return->image_pack);
    	}

    	if (! empty($return->promotional_pack) && file_exists('assets/files/content/products/'.$return->promotional_pack)) {
    		unlink('assets/files/content/products/'.$return->promotional_pack);
    	}

    	if (! empty($return->specs_document) && file_exists('assets/files/content/products/'.$return->specs_document)) {
    		unlink('assets/files/content/products/'.$return->specs_document);
    	}
    }

}
