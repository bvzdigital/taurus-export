<?php
class Admin_Users_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('url');		
	}

	public function fields_grid(){
		$listaMenu = array('ID' => 'id', 'User' => 'user', 'Nome' => 'name', 'Email' => 'email', 'Status' => 'status', 'Grupo' => 'admin_group_id');
		return $listaMenu;
	}
	
	public function get_list(){
		$table = 'admin_users';
		$this->db->select('*');
		$this->db->from($table);				
		$query = $this->db->get();		
		return $query->result();
	}

	public function edit($id){
		$this->db->select('*');
		$query = $this->db->get_where('admin_users' , array('id' => $id));
		return $query->row();
	}
	
	public function insert() {			
		if (!$this->exist_content()) {
			$password = md5('123');
			if ($this->input->post('senha')) {
				$password = md5($this->input->post('senha'));
			}

			$data = array(
				'user' 				=> $this->input->post('user'),
				'name' 				=> $this->input->post('nome'),
				'password' 			=> $password,
				'email'   			=> $this->input->post('email'),			
				'created_on'		=> date('Y-m-d'),
				'status'  			=> $this->input->post('status'),
				'admin_group_id'	=> $this->input->post('grupo') 				
			);			
			$this->db->insert('admin_users' , $data);	
			if ($this->input->post('menus')) {
				$id = mysql_insert_id();
				$this->insert_permissions_users($id);
			}
		} else {
			return false;
		}
	}
	
	public function insert_permissions_users($user_id){
		$data =array();
		$count = count($this->input->post('menus'));
		for($i = 0; $i < $count; $i++) {
			$menu = $this->input->post('menus');
			$data[$i] = array(
				'user_id' =>  $user_id,
			    'module_id' => $menu[$i]				   
			);
		}
		
		$this->db->insert_batch('admin_modules_has_users' , $data);	
	}
	
	public function update($id){			
		if ($id == 1) {
			$status = 1;
			$admin_group_id = 1;
		} else {
			$status = $this->input->post('status');
			$admin_group_id = $this->input->post('grupo');
		}

		if ($this->input->post('senha') <> ''){
			$data = array(			
				'name' 				=> $this->input->post('nome'),					
				'password' 			=> md5($this->input->post('senha')),
				'email'   			=> $this->input->post('email'),				
				'status'  			=> $status,
				'admin_group_id'	=> $admin_group_id
			);
		} else {
			$data = array(			
				'name' 				=> $this->input->post('nome'),										
				'email'   			=> $this->input->post('email'),				
				'status'  			=> $status,
				'admin_group_id'	=> $admin_group_id
			);
		}
		
		$this->db->where('id', $id);
		$this->db->update('admin_users', $data);
		if ($id != 1 && $id != $this->session->userdata('ID')) { 
			$this->delete_permissions_users($id);
			if($this->input->post('menus')){			
				$this->insert_permissions_users($id);
			}	
		}
	}
	
	public function delete($id){
		$this->delete_permissions_users($id);
		$delete = $this->db->query('DELETE FROM admin_users WHERE id = '.$id);
		return true;
	}
	
	public function delete_permissions_users($id){
		$this->db->query('DELETE FROM admin_modules_has_users WHERE user_id = '.$id);		
		return true;
	}
	
	function validate(){
		$this->db->select('*');
		$this->db->from('admin_users');	 
		$this->db->where('user', $this->input->post('user'));
		$this->db->where('password', md5($this->input->post('senha')));
		$this->db->where('status', 1);
		$query = $this->db->get();
		
		if($query->num_rows == 1)
			return $query->row();
		
		return false;
	}
	
	function validate_edit($module_id) {
		$this->db->where('module_id', $module_id);
		$this->db->where('user_id', $this->session->userdata('ID'));
		$query = $this->db->get('admin_modules_has_users');	
		if($query->num_rows == 1)
			return $query->row();		
		return false;		
	}

	function exist_content() {
		$this->db->select('*');
		$this->db->from('admin_users');					
		$this->db->where('user', $this->input->post('user'));						
		$query = $this->db->get();			
		$usuario = $query->row();	
		if (count($usuario) <= 0) { 
			return false;
		} else {
			return true;
		}
	}

	function nova_senha(){
		$this->db->where('user', $this->input->post('login'));		
		$this->db->where('status', 1);
		$query = $this->db->get('admin_users');
		
		if ($query->num_rows == 1){
			$user = $query->row();

			$novaSenha = '';
			for ($i = 1; $i < 4; $i++) {
				$novaSenha .= chr(rand(65,91)).rand(1,9);
			}

			$data = array('password' => md5($novaSenha));

			$this->db->where('id', $user->id);
			$query = $this->db->update('admin_users', $data);
			if($query){
				$msg = 'Solicitação de nova senha - sistema administrativo Junior Achievement <br />' . 						
						'Nome: <strong>' . 	$user->nome . '</strong><br />' . 
						'Email: '. $user->email . '<br />' .
						'Nova senha: ' . $novaSenha . '<br />';	
				
				$this->load->library('email'); 
				$to[] = $user->email;				
				$this->email->to($to);
				//$this->email->from('contato@jabrasil.com.br', 'Junior Achievement');		
				$this->email->from('paula@dexdigital.com.br', 'Junior Achievement');		
				$this->email->subject('[Site - Admin] Nova senha');
				$this->email->message($msg);		
				return $this->email->send();	
			}
		}	
		return false;
	}
}
?>