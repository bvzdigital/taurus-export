<?php

class News_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'news';
	}

	public function fields_grid()
	{
		$listaMenu = array(
			'ID' => 'id',
			'Título' => 'title',
			'Data' => 'date',
			'Status' => 'status'
		);

		return $listaMenu;
	}

	public function get_list()
	{
		$this->db->select('news.*');
		$this->db->from($this->table);
		$this->db->order_by('news.id');

		return $this->db->get()->result();
	}

	/* /pt/ */
	public function get_active()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		$this->db->limit(3);
		$this->db->order_by('date', 'DESC');
		return $this->db->get()->result();
	}

	/**
	 * Pega os últimos 9 resultados da tabela de notícias
	 * @param offset de notícias para a sessão 'carregar mais'
	 * @return resultados da query
	 */
	public function get_site()
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		// $this->db->where(['language_iso' => $this->lang->lang()]);
		//$this->db->limit(6);
		$this->db->order_by('date', 'DESC');
		return $this->db->get()->result();
	}

	/**
	 * Pega um registro de notícia através da sua slug
	 * @param slug (link) da notícia
	 * @return resultado da query
	 */
	public function find_by_slug($slug)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['status' => 1]);
		// $this->db->where(['language_iso' => $this->lang->lang()]);
		$this->db->where(['slug' => $slug]);
		return $this->db->get()->row();
	}

	/* /pt/noticias/(:any) [Produtos Relacionados] */
	public function news_products($id = null) {
		$this->db->select('news_products.*, products.*');
		$this->db->from('products');
		if($id !== null) {
			$this->db->where('id_noticia', $id);
		}
		$this->db->join('news_products', 'news_products.id_produto = products.id');

		return $this->db->get()->results();
	}
	/* Notícias Relacionadas ao Um Produto Específico */
	public function products_news($id) {
		$this->db->select('news.*');
		$this->db->from($this->table);
		$this->db->join('news_products', 'news_products.id_noticia = news.id');
		$this->db->join('products', 'news_products.id_produto = products.id');
		$this->db->where('products.id', $id);
		return $this->db->get()->result();
	}

	public function get_news_guns() {
		$this->db->select('products.*');
		$this->db->from('products');
		$this->db->join('news_products', 'news_products.id_produto = products.id');
		$this->db->group_by('products.id');
		$this->db->order_by('products.name', 'ASC');
		return $this->db->get()->result();
	}

	public function find_by_gun($id) {
		$this->db->select('news.*, news.id as idNoticia');
		$this->db->from('news');
		$this->db->where_in('news_products.id_produto', $id);
		$this->db->join('news_products', 'news_products.id_noticia = news.id');
		$this->db->group_by('news.id');
		return $this->db->get()->result();
	}

	public function find_for_pagination($limit, $offset)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->order_by('date', 'DESC');
		$this->db->limit($limit, $offset);

		return $this->db->get()->result();
	}

	public function count()
	{
		return $this->db->count_all($this->table);
	}

	public function find($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function insert()
	{
 		$img = $this->fetchImage();

		if (isset($arquivo->error_msg)) {
			return false;
		}

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> strtolower(url_title(convert_accented_characters(trim($this->input->post('title'))))),
			'image'			=> $img,
			'status' 		=> $this->input->post('status'),
			'date' 			=> dateToMysql($this->input->post('date')),
			'language_iso'  => $this->lang->lang(),
			'description'	=> $this->input->post('description')
		];

		$this->db->insert($this->table, $data);
		$id = $this->db->insert_id();

		if ($this->input->post('gunsNews')) {
			foreach ($this->input->post('gunsNews') as $guns) {
				$this->news_model->create_relation($guns, $id);
			}
		}
	}

	public function update($id) {
		$img = $this->fetchImage();

		$data = [
			'title' 		=> $this->input->post('title'),
			'slug' 			=> strtolower(url_title(convert_accented_characters(trim($this->input->post('title'))))),
			'image'			=> $img,
			'status' 		=> $this->input->post('status'),
			'date' 			=> dateToMysql($this->input->post('date')),
			'language_iso'  => $this->lang->lang(),
			'description'	=> $this->input->post('description', false)
		];

		$this->news_model->delete_relation($id);

		if ($this->input->post('gunsNews')) {
			foreach ($this->input->post('gunsNews') as $guns) {
				$this->news_model->create_relation($guns, $id);
			}
		}

		$this->db->where(['id' => $id]);

		return $this->db->update($this->table, $data);
	}

	public function create_relation($product_id, $news_id)
	{

		$data = [
			'id_produto' => $product_id,
			'id_noticia' => $news_id
		];

		return $this->db->insert('news_products', $data);
	}

	public function delete_relation($news_id) {
		return $this->db->delete('news_products', array('id_noticia' => $news_id));
	}

	public function delete($news_id) {
		return $this->db->delete('news', array('id' => $news_id));
	}

	public function edit($id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['id' => $id]);

		return $this->db->get()->row();
	}

	public function crop($id)
	{
		return $this->edit($id);
	}

	private function uploadImg($file){
		$config['upload_path']   	= 'assets/img/content/news/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']         = '8000';
		$config['max_width']      	= '4000';
		$config['max_height']       = '4000';
		$file_name 					= underscore(removeAccents($_FILES[$file]['name']));
		$config['file_name'] 		= $file_name;

		$this->load->library('upload', $config);

		if($this->upload->do_upload($file)){
			$data = $this->upload->data();

			$config['image_library']    = 'GD2';
			$config['source_image']     = $data['full_path'];
			$config['new_image'] 		= 'assets/img/content/news/tn_'.$data['file_name'];
			$config['maintain_ratio']   = FALSE;
			$config['width']            = 270;
			$config['height']           = 140;

			$this->load->library('image_lib', $config);

			if($this->image_lib->resize())
				return $data['file_name'];
			else{
				echo $this->image_lib->display_errors();
				return "";
			}
		}else{
			return false;
		}
	}

	private function fetchImage()
	{
		if (isset($_FILES['image'])) {
			$img = $this->uploadImg('image');
		} else if ($this->input->post('current_image')) {
			$img = $this->input->post('current_image');
		}

		return $img;
	}

	private function deleteImage($id)
	{
		$this->db->select('image');
		$this->db->from($this->table);
		$this->db->where('id', $id);

		$img = $this->db->get()->row()->image;
		$path = 'assets/img/content/news/'.$img;
		$thumb_path = 'assets/img/content/news/tn_'.$img;

		if (file_exists($path) && file_exists($thumb_path)) {
			unlink($path);
			unlink($thumb_path);
		}
	}

	private function uploadFile($file){
        $config['upload_path']      = 'assets/files/content/news/';
        $config['allowed_types']	= 'pdf|doc|docx|txt|gif|png|jpg|xls|xlsx';
        $config['max_size']         = '20000';
        $config['file_name']        = underscore(removeAccents(@$_FILES[$file]['name']));

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($file)) {
            $data = $this->upload->data();
            return $data;
        } else {
            return $this->upload;
        }
    }
}
