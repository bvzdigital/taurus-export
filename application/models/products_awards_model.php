<?php

class Products_awards_model extends CI_Model {

	private $table;

	public function __construct()
	{
		parent::__construct();

		$this->table = 'products_awards';
	}

	public function find_by_product($product_id)
	{
		$this->db->select();
		$this->db->from($this->table);
		$this->db->where(['product_id' => $product_id]);

		return $this->db->get()->result();
	}

	public function create($product_id, $award_id)
	{
		$data = [
			'product_id' => $product_id,
			'award_id' => $award_id
		];

		return $this->db->insert($this->table, $data);
	}

	public function delete($product_id)
	{
		return $this->db->delete($this->table, ['product_id' => $product_id]);
	}

}