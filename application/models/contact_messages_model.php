<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Contact_messages_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function fields_grid(){
		$listaMenu = array('ID' => 'id', 'Nome' => 'name', 'Email' => 'email', 'Telefone' => 'phone', 'País' => 'country');
		return $listaMenu;
	}

	public function get_list(){
		$this->db->select('*')->from('contact_messages');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_list_countries(){
		$this->db->select('*')->from('countries')->order_by("name", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function edit($id){
		$this->db->select('*');
		$query = $this->db->get_where('contact_messages' , array('id' => $id));
		return $query->row();
	}

	public function insert(){
		$data = array(
			'name'  	=> $this->input->post('name'),
			'email'		=> $this->input->post('email'),
			'phone'		=> $this->input->post('phone'),
			'country'	=> $this->input->post('country'),
			'message'	=> $this->input->post('message')
		);

		return $this->db->insert('contact_messages', $data);
	}

	public function update($id){
		$data = array(
			'name'  	=> $this->input->post('name'),
			'email'		=> $this->input->post('email'),
			'phone'		=> $this->input->post('phone'),
			'country'	=> $this->input->post('country'),
			'message'	=> $this->input->post('message')
		);

		$this->db->where('id', $id);
		return $this->db->update('contact_messages', $data);
	}

	public function delete($id){
		return $this->db->query("DELETE FROM contact_messages WHERE id = ".$id);
	}
}