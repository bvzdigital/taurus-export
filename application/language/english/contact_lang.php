<?php

	$lang['contact.contact'] = 'Contact';
	$lang['contact.name'] = 'Name (mandatory)';
	$lang['contact.email'] = 'Email (mandatory)';
	$lang['contact.phone'] = 'Phone (mandatory)';
	$lang['contact.region'] = 'Region';
	$lang['contact.message'] = 'Message (mandatory)';
	$lang['contact.send'] = 'Send';
	$lang['contact.text'] = 'Feel free to use this space to contact us.';
	$lang['contact.ver_maps'] = 'See on Google Maps';
	$lang['contact.enviado_com_sucesso'] = 'Contact form successfuly sent!';
	$lang['contact.enviar_outra'] = 'I want to send another message.';
	$lang['contact.falha_no_envio'] = 'Failed to send contact form!';

	$lang['contact_news.enviar_outra'] = 'I want to add another email.';
	$lang['contact_news.enviado_com_sucesso'] = 'News form successfuly sent!';
	$lang['contact_news.falha_no_envio'] = 'Failed to send news form!';
?>
