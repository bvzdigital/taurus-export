<?php

$lang['footer.Submit'] = 'Send';
$lang['footer.name_mandatory'] = 'Name (mandatory)';
$lang['footer.email_mandatory'] = 'E-mail (mandatory)';
$lang['footer.see_on_maps'] = 'See on Google Maps';
$lang['footer.phone'] = 'Phone';
$lang['footer.copyright'] = 'All Rights Reserved - Empresas Taurus.';
$lang['footer.horario_atendimento'] = 'Office hours: Monday - Friday 9am - 12pm, 1pm - 5pm.';