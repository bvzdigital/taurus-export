<?php

$lang['downloads.category'] = 'Category:';
$lang['downloads.Downloads'] = 'Downloads';
$lang['downloads.Download'] = 'Download';
$lang['downloads.options-posters'] = 'Posters';
$lang['downloads.options-catalog'] = 'Catalog';
$lang['downloads.options-news'] = 'News';
$lang['downloads.options-select'] = 'Select...';