<?php

$lang['search.search'] = 'Search';
$lang['search.search_results'] = 'Search results';
$lang['search.about_the_company'] = 'About the Company';
$lang['search.distributors'] = 'Distributors';
$lang['search.institutional'] = 'Institutional';
$lang['search.no_results'] = 'No results were found with the term %s%s%s';
$lang['search.success'] = '%s%s result(s)%s were found with the term %s%s%s';
$lang['search.categories'] = 'Categories';
$lang['search.subcategories'] = 'Subcategories';
$lang['search.products'] = 'Products';
$lang['search.downloads'] = 'Downloads';
$lang['search.download'] = 'Download';
$lang['search.category'] = 'Category:';