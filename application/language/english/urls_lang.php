<?php

$lang['urls.home'] = '';
$lang['urls.about-the-company'] = 'about-the-company';
$lang['urls.contact'] = 'contact';
$lang['urls.downloads'] = 'downloads';
$lang['urls.distributors'] = 'distributors';
$lang['urls.distributors-rossi'] = 'distributors-rossi';
$lang['urls.distributors-taurus'] = 'distributors-taurus';
$lang['urls.categories'] = 'categories';
$lang['urls.subcategories'] = 'subcategories';
$lang['urls.products/(:wildcard)'] = 'products/%s';
$lang['urls.products/(:wildcard)/(:wildcard)'] = 'products/%s/%s';
$lang['urls.products/(:wildcard)/(:wildcard)/(:wildcard)'] = 'products/%s/%s/%s';

