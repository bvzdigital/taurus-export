<?php

$lang['downloads.category'] = 'Categoría:';
$lang['downloads.Downloads'] = 'Downloads';
$lang['downloads.Download'] = 'Descargar';
$lang['downloads.options-posters'] = 'Posters';
$lang['downloads.options-catalog'] = 'Catálogo';
$lang['downloads.options-news'] = 'Noticias';
$lang['downloads.options-select'] = 'Seleccionar...';