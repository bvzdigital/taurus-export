<?php

$lang['urls.home'] = '';
$lang['urls.about-the-company'] = 'acerca-de-la-compania';
$lang['urls.contact'] = 'contacto';
$lang['urls.downloads'] = 'downloads';
$lang['urls.distributors'] = 'distributors';
$lang['urls.distributors-rossi'] = 'distributors-rossi';
$lang['urls.distributors-taurus'] = 'distributors-taurus';
$lang['urls.categories'] = 'categories';
$lang['urls.subcategories'] = 'subcategories';
$lang['urls.products/(:wildcard)'] = 'productos/%s';
$lang['urls.products/(:wildcard)/(:wildcard)'] = 'productos/%s/%s';
$lang['urls.products/(:wildcard)/(:wildcard)/(:wildcard)'] = 'productos/%s/%s/%s';

