<?php

	$lang['contact.contact'] = 'Contacto';
	$lang['contact.name'] = 'Nome (obligatorio)';
	$lang['contact.email'] = 'Email (obligatorio)';
	$lang['contact.phone'] = 'Teléfono (obligatorio)';
	$lang['contact.region'] = 'Región';
	$lang['contact.message'] = 'Mensaje (obligatorio)';
	$lang['contact.send'] = 'Enviar';
	$lang['contact.text'] = 'Siéntase libre de utilizar este espacio para ponerse en contacto con nosotros.';
	$lang['contact.ver_maps'] = 'Ver en Google Maps';
	$lang['contact.enviado_com_sucesso'] = 'Formulario de contacto enviado!';
	$lang['contact.enviar_outra'] = 'Quiero enviar otro mensaje.';
	$lang['contact.falha_no_envio'] = 'No se pudo enviar el formulario de contacto!';

	$lang['contact_news.enviar_outra'] = 'Quiero enviar otro email.';
	$lang['contact_news.enviado_com_sucesso'] = 'Formulario enviado!';
	$lang['contact_news.falha_no_envio'] = 'No se pudo enviar el formulario!';
?>
