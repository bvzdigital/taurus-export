<?php

$lang['footer.Submit'] = 'Enviar';
$lang['footer.name_mandatory'] = 'Nome (obligatorio)';
$lang['footer.email_mandatory'] = 'Email (obligatorio)';
$lang['footer.see_on_maps'] = 'Ver en Google Maps';
$lang['footer.phone'] = 'Teléfono';
$lang['footer.copyright'] = 'Todos Los Derechos Reservados - Empresas Taurus.';
$lang['footer.horario_atendimento'] = 'Horario de funcionamiento: Lunes - Viernes  9:00 - 12:00, 13:00 - 17:00.';