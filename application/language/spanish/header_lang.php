<?php

$lang['header.Search'] = 'Búsqueda';
$lang['header.menu-company'] = 'Empresa';
$lang['header.menu-distributors'] = 'Distribuidores';
$lang['header.menu-products'] = 'Productos';
$lang['header.menu-downloads'] = 'Downloads';
$lang['header.menu-contact'] = 'Contacto';
$lang['header.logo'] = 'Taurus Export';

$lang['header.mother_menu-institutional'] = 'Institutional';
$lang['header.mother_menu-guns_and_acessories'] = 'Guns and Acessories';
$lang['header.mother_menu-bulletproof_vests'] = 'Bulletproof Vests';
$lang['header.mother_menu-helmets_and_acessories'] = 'Helmet and Acessories';
$lang['header.mother_menu-plastic_containers'] = 'Plastic Containers';
$lang['header.mother_menu-mim'] = 'MIM';