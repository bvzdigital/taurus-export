<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

/* CONFIG LOCAL PADRÃO */
$config['protocol'] 	= 'smtp';
$config['smtp_host']	= 'ssl://smtp.gmail.com';
$config['smtp_port']	= '465';
$config['smtp_user'] 	= 'taurusarmasofficial@gmail.com';
$config['smtp_pass']	= 'g00gl3t4uru52k21';
$config['wordwrap'] 	= true;
$config['charset'] 		= 'utf-8';
$config['crlf'] 		= "\r\n";
$config['newline'] 		= "\r\n";
$config['mailtype'] 	= 'html';
$config['validation']	= true;


/* CONFIG PADRÃO - SEM NECESSIDADE DE AUTENTICAÇÃO
$config['protocol'] 	= 'mail';
$config['smtp_host']	= '';
$config['smtp_port']	= '';
$config['smtp_user'] 	= '';
$config['smtp_pass']	= '';
$config['wordwrap'] 	= true;
$config['charset'] 		= 'utf-8';
$config['crlf'] 		= "\r\n";
$config['newline'] 		= "\r\n";
$config['mailtype'] 	= 'html';
//$config['validation']	= true;
*/

/* End of file email.php */
/* Location: ./application/config/email.php */

