<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
*/
$active_group = 'local';
$active_record = TRUE;

$db['default']['hostname'] = 'mysql04-farm55.uni5.net';
$db['default']['username'] = 'taurusexport01';
$db['default']['password'] = 'eR08dbMvrw';
$db['default']['database'] = 'taurusexport01';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

/* Local db config */
$db['local']['hostname'] = 'localhost';
$db['local']['username'] = 'root';
$db['local']['password'] = 'root';
$db['local']['database'] = 'taurus_export';
$db['local']['dbdriver'] = 'mysqli';
$db['local']['dbprefix'] = '';
$db['local']['pconnect'] = FALSE;
$db['local']['db_debug'] = TRUE;
$db['local']['cache_on'] = FALSE;
$db['local']['cachedir'] = '';
$db['local']['char_set'] = 'utf8';
$db['local']['dbcollat'] = 'utf8_general_ci';
$db['local']['swap_pre'] = '';
$db['local']['autoinit'] = TRUE;
$db['local']['stricton'] = FALSE;

$db['optiplex']['hostname'] = 'localhost';
$db['optiplex']['username'] = 'root';
$db['optiplex']['password'] = 'baraoDX891@';
$db['optiplex']['database'] = 'taurus_export';
$db['optiplex']['dbdriver'] = 'mysqli';
$db['optiplex']['dbprefix'] = '';
$db['optiplex']['pconnect'] = FALSE;
$db['optiplex']['db_debug'] = TRUE;
$db['optiplex']['cache_on'] = FALSE;
$db['optiplex']['cachedir'] = '';
$db['optiplex']['char_set'] = 'utf8';
$db['optiplex']['dbcollat'] = 'utf8_general_ci';
$db['optiplex']['swap_pre'] = '';
$db['optiplex']['autoinit'] = TRUE;
$db['optiplex']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */