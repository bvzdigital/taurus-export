<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/*
|-------------------------------------------------------------------------
| Site
|-------------------------------------------------------------------------
*/
$route['404_override'] = 'home';
$route['default_controller'] = 'home';
$route['^(en|es)$'] = $route['default_controller'];

$route['en/about-the-company'] = 'institutional';
$route['es/acerca-de-la-compania'] = 'institutional';

$route['en/contact'] = 'contact';
$route['es/contacto'] = 'contact';

$route['en/contact/create'] = 'contact/create';
$route['es/contacto/create'] = 'contact/create';
$route['en/contact/create-news'] = 'contact/create_news';
$route['es/contacto/create-news'] = 'contact/create_news';

$route['en/downloads'] = 'downloads';
$route['es/downloads'] = 'downloads';

$route['en/distributors'] = 'distributors';
$route['es/distributors'] = 'distributors';

$route['en/distributors-rossi'] = 'distributors';
$route['es/distributors-rossi'] = 'distributors';
$route['en/distributors-taurus'] = 'distributors';
$route['es/distributors-taurus'] = 'distributors';

$route['en/distributors-rossi/(:any)'] = 'distributors';
$route['es/distributors-rossi/(:any)'] = 'distributors';
$route['en/distributors-taurus/(:any)'] = 'distributors';
$route['es/distributors-taurus/(:any)'] = 'distributors';

$route['en/distributors/(:any)'] = 'distributors';
$route['es/distributors/(:any)'] = 'distributors';

$route['en/categories'] = 'categories';
$route['es/categories'] = 'categories';

$route['en/subcategories'] = 'subcategories';
$route['es/subcategories'] = 'subcategories';

$route['en/products'] = 'products/all';
$route['es/productos'] = 'products/all';

$route['en/products/(:any)/(:any)/(:any)/download'] = 'products/download/$1/$2/$3';
$route['es/productos/(:any)/(:any)/(:any)/download'] = 'products/download/$1/$2/$3';

$route['en/products/(:any)/(:any)/(:any)'] = 'products/show/$1/$2/$3';
$route['es/productos/(:any)/(:any)/(:any)'] = 'products/show/$1/$2/$3';

$route['en/products/(:any)/(:any)'] = 'categories/show/$1';
$route['es/productos/(:any)/(:any)'] = 'categories/show/$1';

$route['en/products/(:any)'] = 'categories/show/$1';
$route['es/productos/(:any)'] = 'categories/show/$1';

$route['en/search'] = 'search';
$route['es/search'] = 'search';

$route['en/news'] = 'news/index';
$route['en/news/page/(:num)/(:any)'] = 'news/page/$1/$2';
$route['en/news/(:any)'] = 'news/detalhe/$1';

$route['en/safety'] = 'safety/index';
$route['en/safety/page/(:num)/(:any)'] = 'safety/page/$1/$2';
$route['en/safety/(:any)'] = 'safety/page/$1';

$route['en/support'] = 'support/faq';
$route['en/support/faq'] = 'support/faq';
$route['en/support/trademark'] = 'support/trademark';
$route['en/support/media'] = 'support/media';
$route['en/support/spare-parts'] = 'support/spareParts';

$route['en/corporate-videos'] = 'videos_corporativos';
$route['en/product-catalog'] = 'catalogo_produto';

//MANAGER
//TELA DE INSERÇÃO
$route['admin/modulo/(:any)/adiciona'] = 'admin/modulo/adiciona/$1';
$route['admin/modulo/insert/(:any)'] = 'admin/modulo/insert/$1';
$route['admin/modulo/(:any)/(:any)/adiciona'] = 'admin/modulo/adiciona/$1/$2';
$route['admin/modulo/insert/(:any)/(:any)'] = 'admin/modulo/insert/$1/$2';

//REGRAS PARA EDIÇÃO
$route['admin/modulo/editar/(:any)/(:any)/(:num)'] = 'admin/modulo/editar/$1/$2/$3';
$route['admin/modulo/editar/(:any)/(:num)'] = 'admin/modulo/editar/$1/sem/$2';
$route['admin/modulo/update/(:any)/(:any)/(:num)'] = 'admin/modulo/update/$1/$2/$3';
$route['admin/modulo/update/(:any)/(:num)'] = 'admin/modulo/update/$1/sem/$2';
$route['admin/modulo/conteudo_padrao/sub/(:any)'] = 'admin/modulo/conteudo_padrao/$1';
$route['admin/modulo/update_conteudo_padrao/(:any)/(:num)'] = 'admin/modulo/update_conteudo_padrao/$1/$2';

//REGRAS PARA EXCLUSAO
$route['admin/modulo/deletar/(:any)/(:any)/(:num)'] = 'admin/modulo/deletar/$1/$2/$3';
$route['admin/modulo/deletar/(:any)/(:num)'] = 'admin/modulo/deletar/$1/sem/$2';
$route['admin/modulo/remove_selected/(:any)/(:any)'] = 'admin/modulo/remove_selected/$1/$2';
$route['admin/modulo/remove_selected/(:any)'] = 'admin/modulo/remove_selected/$1/sem/$2';

// REGRAS PARA BUSCA DE MENUS POR GRUPOS E USUÁRIOS
$route['admin/modulo/get_modules_groups/(:any)/(:any)/(:num)'] = 'admin/modulo/get_modules_groups/$1/$2/$3';
$route['admin/modulo/get_modules_groups/(:num)'] = 'admin/modulo/get_modules_groups/$1';

//EXCLUI IMAGENS
$route['admin/modulo/remove_img/(:any)/(:any)/(:num)'] = 'admin/modulo/remove_img/$1/$2/$3';
$route['admin/modulo/remove_img/(:any)/(:num)/(:num)'] = 'admin/modulo/remove_img/$1/sem/$2';
$route['admin/modulo/remove_img/(:any)/(:num)'] = 'admin/modulo/remove_img/$1/sem/$2';
$route['admin/modulo/remove_img2/(:any)/(:num)'] = 'admin/modulo/remove_img/$1/sem/$2';

//EXCLUI ARQUIVOS
$route['admin/modulo/remove_arq/(:any)/(:any)/(:num)'] = 'admin/modulo/remove_arq/$1/$2/$3';
$route['admin/modulo/remove_arq/(:any)/(:num)'] = 'admin/modulo/remove_arq/$1/sem/$2';

//TELA DE UPLOAD
$route['admin/modulo/(:any)/upload'] = 'admin/modulo/upload/$1';
// Tela de exportacao
$route['admin/modulo/(:any)/exportar'] = 'admin/modulo/exportar/$1';

//REGRAS PARA EXPORTACAO
$route['admin/modulo/exportar/(:any)'] = 'admin/modulo/exportar/$1';

//EXCLUI ARQUIVOS
$route['admin/modulo/remove_file/(:any)/(:any)'] = 'admin/modulo/remove_file/$1/$2';

//EXCLUI IMAGENS GALERIA
$route['admin/upload/remove/(:any)'] = 'admin/upload/remove/$1';
//CROP DE IMAGEM
$route['admin/modulo/crop/(:any)/(:any)'] = 'admin/modulo/crop/$1/$2';
$route['admin/modulo/crop2/(:any)/(:any)'] = 'admin/modulo/crop/$1/$2';
$route['admin/modulo/crop3/(:any)/(:any)'] = 'admin/modulo/crop/$1/$2';

$route['admin/home/login'] = 'admin/home/login';
$route['admin/home/senha'] = 'admin/home/senha';
$route['admin'] = 'admin/home';

$route['admin/modulo/news/filtro'] = 'admin/modulo/noticias_categoria_filtro';
$route['admin/modulo/news/add'] = 'admin/modulo/noticias_add_arma';

// 1° SUPER - 2° SUB
$route['admin/modulo/(:any)/sub/(:any)'] = 'admin/modulo/sub/$1/$2';
// 1° SUPER
$route['admin/modulo/(:any)'] = 'admin/modulo/index/$1';

//SITE
//$route['404_override'] = 'home/not_found/';
$route['default_controller'] = 'home';
$route['assessoria-imprensa'] = 'assessoria_imprensa/index';

/* End of file routes.php */
/* Location: ./application/config/routes.php */