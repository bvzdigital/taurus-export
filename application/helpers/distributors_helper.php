<?php

function get_region($id)
{
	$ci =& get_instance();

	$ci->db->select();
	$ci->db->from('regions');
	$ci->db->where(['id' => $id]);

	return $ci->db->get()->row()->name;
}