<?php

function pr($a){ echo '<pre>', print_r($a,1) ,'</pre>'; }

function getContent($tbl, $id){
	$ci=& get_instance();
	$ci->load->database();
	$ci->db->select('*');
	$query = $ci->db->get_where($tbl , array('id' => $id));
	return $query->row();
}

function fileExists($secao, $imagem){
	if (file_exists('assets/img/content/'.$secao.'/tn_'.@$imagem)) {
		$img = 'assets/img/content/'.$secao.'/tn_'.@$imagem;
	} else if (file_exists('assets/img/content/'.$secao.'/'.@$imagem)) {
		$img = 'assets/img/content/'.$secao.'/'.@$imagem;
	} else {
		$img = '';
		//continue;
	}
	return $img;
}

function formatTextSearch ($texto, $busca) {
	//$texto = str_ireplace('</p>', ' ', $texto);
	$texto = strip_tags($texto);
	$pos = strripos($texto, $busca);
	$lenStr = strlen($texto);

	if ($pos !== false) {
		$pos = 0;
		if($lenStr > 100) {
			if ($pos > 70){
		 		$pos = $pos - 70;
			} else if ($pos > 20){
				$pos = $pos - 20;
			} else if ($pos > 10) {
				$pos = $pos - 10;
			} else if ($pos > 1) {
				$pos = 0;
			}
		}
		return '<p>'.str_ireplace($busca, '<strong>'.$busca.'</strong>', substr($texto, $pos, 300)).'</p>';
	} else {
		false;
	}

}

function get_img_list_news($news_id) {
	$ci=& get_instance();
	$ci->load->database();

	$ci->db->select('url');
	$ci->db->from('noticias_galeria');
	$ci->db->where('noticia_id', $news_id);
	$ci->db->order_by('ordem', 'ASC');
	$ci->db->limit(1);
	$query = $ci->db->get();
	$img = $query->row();
	return $img->url;
}


function searchSubMenu($id, $admin_unidade_id){
	$ci=& get_instance();
	$ci->load->database();

	$table_name = "ci_modulo";
	$table_name2 = 'admin_permissao_unidades';

	$ci->db->select('*');
	$ci->db->from($table_name);
	$ci->db->join($table_name2, $table_name.'.id = '.$table_name2.'.ci_modulo_id');
	if(!empty($admin_unidade_id)){
		$ci->db->where($table_name2.'.admin_unidade_id', $admin_unidade_id);
	}else{
		$ci->db->where($table_name2.'.admin_unidade_id', $ci->session->userdata('UNIDADE'));
	}
	$ci->db->where($table_name.'.pai_id', $id);
	$ci->db->group_by($table_name.'.id');
	$ci->db->order_by($table_name.'.ordem ASC');
	$query = $ci->db->get();

	return $query->result();
}

function validaCPF($cpf){	// Verifiva se o número digitado contém todos os digitos
    $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);

	// Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
		return false;
    } else {   // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }
}

function isCNPJ($cnpj) {
	if (strlen($cnpj) <> 14) return 0;
   $soma1 = ($cnpj[0] * 5) +
    ($cnpj[1] * 4) +
    ($cnpj[2] * 3) +
    ($cnpj[3] * 2) +
    ($cnpj[4] * 9) +
    ($cnpj[5] * 8) +
    ($cnpj[6] * 7) +
    ($cnpj[7] * 6) +
    ($cnpj[8] * 5) +
    ($cnpj[9] * 4) +
    ($cnpj[10] * 3) +
    ($cnpj[11] * 2);
   $resto = $soma1 % 11;
   $digito1 = $resto < 2 ? 0 : 11 - $resto;
   $soma2 = ($cnpj[0] * 6) +
    ($cnpj[1] * 5) +
    ($cnpj[2] * 4) +
    ($cnpj[3] * 3) +
    ($cnpj[4] * 2) +
    ($cnpj[5] * 9) +
    ($cnpj[6] * 8) +
    ($cnpj[7] * 7) +
    ($cnpj[8] * 6) +
    ($cnpj[9] * 5) +
    ($cnpj[10] * 4) +
    ($cnpj[11] * 3) +
    ($cnpj[12] * 2);
   $resto = $soma2 % 11;
   $digito2 = $resto < 2 ? 0 : 11 - $resto;
  return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
}

function get_user_browser(){
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = '';
    if(preg_match('/MSIE/i',$u_agent)) {
        $ub = "ie";
    } elseif(preg_match('/Firefox/i',$u_agent)) {
        $ub = "firefox";
    } elseif(preg_match('/Safari/i',$u_agent)) {
        $ub = "safari";
    } elseif(preg_match('/Chrome/i',$u_agent)) {
        $ub = "chrome";
    } elseif(preg_match('/Flock/i',$u_agent)) {
        $ub = "flock";
    } elseif(preg_match('/Opera/i',$u_agent)) {
        $ub = "opera";
    }

    return $ub;
}


function pegaThumb($img){
	$thumb = explode('.',$img);
	$thumb = $thumb[0].'_thumb.'.$thumb[1];
	return $thumb;
}

function dateToMysql($date){
	$data = implode('-', array_reverse(explode('/',$date)));
	return $data;
}

function dateToPt($date){
	$data = implode('/', array_reverse(explode('-',$date)));
	return $data;
}

function dateHourToPt($date, $div){
	$hora = explode(' ', $date);
	$data = implode($div, array_reverse(explode('-', $hora[0])));
	return $data.' '.$hora[1];
}

function showStatus($status){
	switch($status){
		case 1:
			return '<img src="assets/admin/images/icon-checked.png" alt="Ativo" title="Ativo" />';
		break;
		case 0:
			return '<img src="assets/admin/images/icon-unchecked.png" alt="Inativo" title="Inativo" />';
		break;
	}
}

function showLanguageFlag($language){
	switch($language){
		case 'en':
			return '<img src="assets/admin/images/icon-mixed-us-uk-flag.png" alt="Bandeira RU/EUA" title="Inglês" />';
		case 'pt':
			return '<img src="assets/admin/images/icon-brazil-flag.png" alt="Bandeira do Brasil" title="Português" />';
		case 'es':
			return '<img src="assets/admin/images/icon-spain-flag.png" alt="Bandeira da Espanha" title="Português" />';
		case 'de':
			return '<img src="assets/admin/images/icon-germany-flag.png" alt="Bandeira da Alemanha" title="Alemão" />';
	}
}

function ultimoDiaMes($newData){
    list($newAno, $newMes, $newDia) = explode("-", $newData);
    return date("Y-m-d", mktime(0, 0, 0, $newMes+1, 0, $newAno));
  }

function showNivel($nivel){
	switch($nivel){
		case 1:
			return '<img src="assets/admin/images/icon-admin.png" alt="Administrador" title="Administrador" />';
		break;
		case 2:
			return '<img src="assets/admin/images/icon-user.png" alt="Usuário" title="Usuário" />';
		break;
	}
}

function dateMes($date){
	$d = explode('-', $date);
	switch($d[1]){
		case '01': $mes = 'Jan'; break;
		case '02': $mes = 'Fev'; break;
		case '03': $mes = 'Mar'; break;
		case '04': $mes = 'Abr'; break;
		case '05': $mes = 'Mai'; break;
		case '06': $mes = 'Jun'; break;
		case '07': $mes = 'Jul'; break;
		case '08': $mes = 'Ago'; break;
		case '09': $mes = 'Set'; break;
		case '10': $mes = 'Out'; break;
		case '11': $mes = 'Nov'; break;
		case '12': $mes = 'Dez'; break;
	}
	return $mes;
}

function dateEvent($data){
	$d 					= explode("-", $data);
	$date['diames'] 	= $d[2].".".$d[1];
	$date['ano'] 		= $d[0];
	return $date;
}

function limit_text($text, $limit) {
	$retorna = "";
	$text=str_replace("<p>","",$text);
	$text=str_replace("</p>","",$text);
	$text=str_replace("<em>","",$text);
	$text=str_replace("</em>","",$text);
	$text=str_replace("<strong>","",$text);
	$text=str_replace("</strong>","",$text);
	$text=str_replace("<b>","",$text);
	$text=str_replace("</b>","",$text);
	$text = explode(" ", $text);

	foreach ($text as $pos => $valor_d_cada_pos ){
		if ($pos <= $limit)
			$retorna .= $valor_d_cada_pos ." ";
	}
	return $retorna . "[...]";
}

function removeAccents($var){
	$var = strtolower($var);

	$letters1 = array(
			"/(?i)á|ã|â|Á|Ã|Â/",
			"/(?i)é|ê|É|Ê/",
			"/(?i)í|î|Í|Î/",
			"/(?i)ó|õ|ô|Ó|Ô|Õ/",
			"/(?i)ú|û|Ú|Û/",
			"/(?i)ç|Ç/",
			"/(?i)º|ª|'|`/");

	$letters2 = array(
			"a",
			"e",
			"i",
			"o",
			"u",
			"c",
			"");

	return preg_replace($letters1, $letters2, $var);
}

function formatUrl($text){
	$text = utf8_decode($text);
	$text = strtolower($text);
	$text = str_replace(" ", "-", $text);

	//$a = array(
		//"/[âãàáä]/"	  =>'a',
	//	"/[êèéë]/"	  =>'e',
	//	"/[îíìï]/" 	  =>'i',
	//	"/[ôõòóö]/"   =>'o',
	//	"/[ûúùü]/"    =>'u',
	//	"/ç/"	      =>'c',
	//	"/[:;.,!?']/" =>''
	//);

	$a = array("á", "ã", "à", "â", "ä", "ê", "è", "é", "ë", "î", "í", "ì", "ï", "ô", "õ", "ò", "ó", "ö", "û", "ú", "ù", "ü", "ç", ":", ";", ".", ",", "!", "?", "'");
	$b = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "", "", "", "", "", "", "");

	$text = str_replace($a, $b, $text);

	echo $text;

	return $text;

	//return preg_replace(array_keys($a), array_values($a), $text);

}

function removeAcentos($string, $slug = false) {
	    $string = utf8_decode($string);
		$string = strtolower($string);
		$string = str_replace(" ", "-", $string);
		$string = str_replace(",", "", $string);
		$string = str_replace("!", "", $string);
		$string = str_replace("?", "", $string);
		$string = str_replace("'", "", $string);
		$string = str_replace('"', "", $string);
		$string = str_replace("`", "", $string);
		$string = str_replace("´", "", $string);
		$string = str_replace(":", "", $string);
		$string = str_replace("/", "", $string);
		$string = str_replace("-", "", $string);

	    // Código ASCII das vogais
	    $ascii['a'] = range(224, 230);
	    $ascii['e'] = range(232, 235);
	    $ascii['i'] = range(236, 239);
	    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	    $ascii['u'] = range(249, 252);

	    // Código ASCII dos outros caracteres
	    $ascii['b'] = array(223);
	    $ascii['c'] = array(231);
	    $ascii['d'] = array(208);
	    $ascii['n'] = array(241);
	    $ascii['y'] = array(253, 255);

	    foreach ($ascii as $key=>$item) {
	        $acentos = '';
	        foreach ($item AS $codigo) $acentos .= chr($codigo);
	        $troca[$key] = '/['.$acentos.']/i';
	    }

	    $string = preg_replace(array_values($troca), array_keys($troca), $string);

	    // Slug?
	    if ($slug) {
	        // Troca tudo que não for letra ou número por um caractere ($slug)
	        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
	        // Tira os caracteres ($slug) repetidos
	        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
	        $string = trim($string, $slug);
	    }

	    return $string;
}

function normalizar($texto) {
    $patterns = array('/á|ã|à/', '/é/', '/í/', '/ó|õ/', '/ú|ü/', '/ç/', '/ |-/');
    $subs = array('a', 'e', 'i', 'o', 'u', 'c', '-');
    if (preg_match('/(%.+)/', $texto)):
      $texto = urldecode($texto);
    endif;
    return preg_replace($patterns, $subs, strtolower($texto));
}

function RecursivePost($p) {
    $post = array();
    foreach ($p as $variable => $value):
      if (!get_magic_quotes_gpc()):
        if (!is_array($value)):
          $post[$variable] = mysql_real_escape_string($value);
        else:
          $post[$variable] = RecursivePost($value);
        endif;
      else:
        $post[$variable] = $value;
      endif;
    endforeach;
    return $post;
}


function tratarPost($p) {
    if (isset($p)):
      return RecursivePost($p);
    else:
      return false;
    endif;
}

function cleanup_string($string) {
    $string = utf8_encode($string);
    $chars = array("á;à;â;ã;Á;À;Â;Ã;" => "a", "é;è;ê;É;È;Ê" => "e", "í;ì;í;Í;Ì" => "i", "ó;ò;ô;õ;º;Ó;Ò;Ô;Õ" => "o", "ú;ù;û;Ú;Ù;Û;ü;Ü" => "u", "ñ;Ñ"=>"n", "$;%;#;@;*;/;(;);,;.;?;+;º" => "");
    foreach ($chars as $key => $char):
        $cs = explode(";", $key);
        foreach ($cs as $c):
            $string = str_replace($c, $char, $string);
        endforeach;
    endforeach;
    $string = trim($string);
    $string = str_replace("ç", "c", $string);
    $string = str_replace("Ç", "C", $string);
    $string = str_replace(" ", "-", $string);
    $string = str_replace("---", "-", $string);
    $string = str_replace("--", "-", $string);
    $string = strtolower($string);
    return $string;
}

function clean_file_name($var){

	$var = strtolower($var);
	$var = ereg_replace("[áàâãª]","a",$var);
	$var = ereg_replace("[éèê]","e",$var);
	$var = ereg_replace("[óòôõº]","o",$var);
	$var = ereg_replace("[úùû]","u",$var);
	$var = str_replace("ç","c",$var);
	$var = str_replace("","-",$var);

	return $var;

}

function array2object($array = array()) {
    if (!empty($array)) {
      $data = false;
      foreach ($array as $akey => $aval) {
      	if(!is_array($aval)){
      		$data->{$akey} = $aval;
      	}else{
      		$data->{$akey} = array2object($aval);
      	}
      }
      return $data;
    }
    return false;
}

function validaEmail($email){
	return (bool)preg_match('/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD',(string )$email);
}

function tamanhoArquivo($arquivo){
	$tamanho = filesize($arquivo);
	$kb = 1024;
	$mb = 1048576;
	$gb = 1073741824;
	$tb = 1099511627776;

	if($tamanho<$kb){
		echo($tamanho." bytes");
	}else if($tamanho>=$kb&&$tamanho<$mb){
		$kilo = number_format($tamanho/$kb,2);
		echo($kilo." KB");
	}else if($tamanho>=$mb&&$tamanho<$gb){
		$mega = number_format($tamanho/$mb,2);
		echo($mega." MB");
	}else if($tamanho>=$gb&&$tamanho<$tb){
		$giga = number_format($tamanho/$gb,2);
		echo($giga." GB");
	}
}

function formatDate($date, $sign) {
	$d = explode('-', $date);
	if(empty($sign)) { $sign = '/'; }
	return $d[2].$sign.$d[1].$sign.$d[0];
}

function assets_url()
{
	return base_url().'assets/';
}

function imgs_url($path = '')
{
	return base_url().'assets/img/content/'.$path;
}

?>