<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class safety extends MY_Controller {

    // private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();

        $this->data['title'] = 'Taurus Export | Safety';
        $this->load->model('safety_model');
        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
    }

    public function index()
    {
        $this->template('safety/index', $this->data);
    }

    public function page($slug)
    {
        $this->data['content'] = $this->safety_model->find_by_slug($slug);

        $this->template('safety/page', $this->data);
    }
}