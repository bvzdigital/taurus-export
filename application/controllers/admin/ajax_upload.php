<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajax_upload extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		//$this->output->enable_profiler(true);		
	}		
	function index(){
	
		$path = $this->input->post('path');			
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpg|gif|png';
		$config['max_size'] = '2048';
		$config['max_width'] = '10240';
		$config['max_height'] = '76800';
		$config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('file'))
		{
			$error = $this->upload->display_errors();
			echo '{"status":"error","message":"'.$error .'"}';
		}
		else
		{
			$data = $this->upload->data();
			echo '{"status":"success","name":"'.$data['file_name'].'","type":"'.$data['file_type'].'","size":"'.$data['file_size'].'"}';
		}
		
	}

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */