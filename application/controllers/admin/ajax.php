<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends CI_Controller {
	
	function __construct(){
		parent::__construct();	
		$this->load->helper('functions');
		$this->load->helper('text_helper');
		$this->load->model('modulo_model');
		$this->load->model('unidade_model');
	}
	
	function index($modulo){
		$retorno = $this->modulo_model->get_modulo($modulo);
		$modulo_tbl  = $retorno->nome_tbl.'_model';		
		$modulo_nome = $retorno->nome_modulo;
		$modulo_menu = $retorno->nome_menu;
		$this->load->model($modulo_tbl);
		$campos = $this->$modulo_tbl->campos_grid();
		
		foreach($campos as $campo => $valor){
			$valid_fields[] = $campo;
		}
		
		$this->flexigrid->validate_post('id','asc',$valid_fields);
		$records = $this->$modulo_tbl->get();
		
		foreach ($records['records']->result() as $row){
			$record_items[] = $row->id;
			$dados = $this->$modulo_tbl->edit($row->id);
			foreach($dados as $dado => $v){
				if($dado == 'status'){
					switch($v){
					case 1:
						$status = '<img src="'.$this->config->item('base_url').'/assets/admin/images/flexgrid/greenled.png" title="Ativo" />';
						break;
					default:
						$status = '<img src="'.$this->config->item('base_url').'/assets/admin/images/flexgrid/redled.png" title="Inativo" />';
						break;
					}
					$record_items[] = $status;
				}elseif($dado == 'destaque'){
					switch($v){
					case 1:
						$destaque = '<img src="'.$this->config->item('base_url').'/assets/admin/images/flexgrid/confirm.png" title="Sim" />';
						break;
					default:
						$destaque = '';
						break;
					}
					$record_items[] = $destaque;
				}elseif($dado == 'data' && $modulo <> 'cadastro'){
					$data = dateToPt($v,'/');
					$record_items[] = $data;
				}elseif($dado == 'data' && $modulo == 'cadastro'){
					$data = dateToPtCompleta($v,'/');
					$record_items[] = $data;				
				}elseif($dado == 'texto'){
					$record_items[] = word_limiter(strip_tags($v), 5, '...');
				}elseif($dado == 'password'){
					//$record_items[] = '';
				}else{	
					$record_items[] = $v;
				}
			}
			if ($modulo == "destaque"){
				$record_items[] = '<a href=\'admin/modulo/crop/'.$modulo.'/'.$row->id.'\'><img border=\'0\' src=\''.$this->config->item('base_url').'/assets/admin/images/flexgrid/lin_agt_wrench.png\'></a> ';
			}
			$record_items[] = '<a href=\'admin/modulo/editar/'.$modulo.'/'.$row->id.'\'><img border=\'0\' src=\''.$this->config->item('base_url').'/assets/admin/images/flexgrid/edit.gif\'></a> ';
			$record_items[] = '<a onclick="return excluir();" href=\'admin/modulo/deletar/'.$modulo.'/'.$row->id.'\'><img border=\'0\' src=\''.$this->config->item('base_url').'/assets/admin/images/flexgrid/close.png\'></a> ';
			$final_records[] = $record_items;
			$record_items = '';
		}
		
		$this->output->set_header($this->config->item('json_header'));
		$this->output->set_output($this->flexigrid->json_build($records['record_count'],$final_records));
		
	}
	
	function deletec($modulo){
	
		$model = $modulo.'_model';	
		$item = explode(",",$this->input->post('items'));
		
		foreach($item as $index => $id){
			if (is_numeric($id) && $id > 1) {
				$this->load->model($model);
				$this->$model->delete($id);
			}
			
		}
		
		$error = "itens selecionados (codigo(s): ".$this->input->post('items').") deletados com sucesso";
		$this->output->set_output($error);
	}
}
?>