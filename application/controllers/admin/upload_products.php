<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_products extends CI_Controller{

    public function __construct(){
        parent::__construct();
		$this->load->model('products_model');
		$this->load->model('products_gallery_model');
		$this->load->helper('text');
    }

    public function index() {
		if (!empty($_FILES)) {
			$type = explode('/', $_FILES['filename']['type']);
			$type = '.'.$type[1];
			$id = $this->input->post('id');

			$product = $this->products_model->edit($id);
			$idProduct = url_title(convert_accented_characters($product->id));

			$count = $this->products_gallery_model->count_this($id);
			$count++;

			$ext = explode('.', $_FILES['filename']['name']);
			$ext = array_pop($ext);

 			$title = 'product-'.$idProduct.'-photo-'. $count . '.' . $ext;

			$config['upload_path']   	= 'assets/img/content/products/';
			$config['allowed_types'] 	= 'png|jpg|jpeg';
			$config['max_size']         = '8000';
			$config['max_width']      	= '4000';
			$config['max_height']       = '4000';
			$config['file_name']		= $title;
			$this->load->library('upload', $config);

			if($this->upload->do_upload('filename')){
				$data = $this->upload->data();
				$this->products_gallery_model->insert($id, $data['file_name']);

				//list($width, $height, $type, $attr) = getimagesize($data['full_path']);
				$config['image_library']    = 'GD2';
				$config['source_image']     = $data['full_path'];
				$config['new_image'] 		= 'assets/img/content/products/tn_'.$data['file_name'];
				$config['maintain_ratio']   = TRUE;
				$config['width']            = 690;
				$config['height']           = 460;

				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				//if($this->image_lib->resize()){
				//	$this->crop($config['new_image']);
				//}

				$return = '<li class="thumb" rel="'.$this->db->insert_id().'">';
				$return .= '<span rel="event" id="'.$this->db->insert_id().'" class="removeItemProducts">X</span>';
				$return .= '<img src="'. $config['new_image'] .'" alt="Foto"/>';
				$return .= '<span class="caption">'. $title .'</span>';
				$return .= '</li>';

				echo $return;
			}else{
				//echo $this->upload->display_errors();
				return false;
			}
		}
    }

	private function crop($img){
		list($width, $height, $type, $attr) = getimagesize($img);
		$cropada = $height/4;

        $config['image_library'] = 'gd2';
        $config['source_image'] = $img;
        $config['x_axis'] = '0';
		$config['y_axis'] = $cropada;
        $config['maintain_ratio'] = false;
        $config['width'] = 690;
        $config['height'] = 460;

        $this->load->library('image_lib', $config);

        $this->image_lib->initialize($config);
        if (!$this->image_lib->crop()){
            echo $this->image_lib->display_errors();
        }
	}

	public function remove($modulo){
		$object_id = $this->input->post('id');
		$image = $this->products_gallery_model->get_line($object_id);
		$this->products_gallery_model->remove_item($object_id);
		if (file_exists('assets/img/content/'.$modulo.'/tn_'.$img->imagem)){
			unlink('assets/img/content/products/tn_'.$image->url);
		}
		if (file_exists('assets/img/content/'.$modulo.'/'.$img->imagem)){
			echo unlink('assets/img/content/products/'.$image->url);
		}
	}

	public function order(){
		$orderString = $this->input->post('order');
		$orderArray = explode('/', $orderString);
		echo '<pre>' . print_r($orederArray, 1) . '</pre>';
		foreach($orderArray as $item){
			$dataItens = explode('=', $item);
			$this->products_gallery_model->update_order($dataItens[0], $dataItens[1]);
		}
		echo 1;
	}
}
?>