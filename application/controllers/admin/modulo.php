<?php
class Modulo extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('functions');
		$this->load->helper('text');

		$this->load->model('admin_modules_model');
		$this->load->model('products_model');

		error_reporting(E_ALL);
		ini_set('memory_limit', '256M');
	}

	function index($uri_super = null) {
		$data = array();
		if($this->session->userdata('logged')){
			if (!empty($uri_super)) {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				if ($super) {
					$module = $super->menu_super.'_model';
					$this->load->model($module);

					$fields	= $this->$module->fields_grid();
					foreach($fields as $key => $value){
						$data['fields'][$key] = $value;
					}
					$data['list_content'] = $this->$module->get_list();

					$this->load->model('admin_users_model', 'user');
            		$data['edit_module'] = $this->user->validate_edit($super->id);
				}
			}

			$header['menu']		 = $this->admin_modules_model->get_menu(null, 0);
			$header['title']   	 = $super->menu;
			$header['section'] 	 = 'Lista';
			$header['uri_super'] = $uri_super;
			$header['pag_type']  = 'list_content';

			$this->load->view('admin/header', $header);
			$this->load->view('admin/'.$uri_super.'/lista', $data);
			$this->load->view('admin/footer');
		}else{
			$this->load->view('admin/login', array('title' => 'Login'));
		}
	}

	function sub($uri_super = null, $uri_sub = null) {
		$data = array();
		if($this->session->userdata('logged')){
			$header['menu'] = $this->admin_modules_model->get_menu(null, 0);

			if (!empty($uri_super)) {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				if ($super) {
					$module_super = $super->menu_super.'_model';
					//$this->load->model($module_super);
				}
			}

			if (!empty($uri_sub)) {
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module_sub = $uri_super.'_'.$sub->menu_sub.'_model';
					$this->load->model($module_sub);

					$fields	= $this->$module_sub->fields_grid();
					foreach($fields as $key => $value){
						$data['fields'][$key] = $value;
					}
					$data['list_content'] = $this->$module_sub->get_list();

					$this->load->model('admin_users_model', 'user');
            		$data['edit_module'] = $this->user->validate_edit($sub->id);
				}
			}

			$header['menu']		 = $this->admin_modules_model->get_menu(null, 0);
			$header['title']   	 = $super->menu;
			$header['sub_title'] = $sub->menu;
			$header['section'] 	 = 'Lista';
			$header['uri_super'] = $uri_super;
			$header['uri_sub']   = $uri_sub;
			$header['pag_type']  = 'list_content';

			$this->load->view('admin/header', $header);
			$this->load->view('admin/' . $uri_super . '/' . $uri_sub . '/lista', $data);
			$this->load->view('admin/footer');
		}
	}

	function adiciona($uri_super = null, $uri_sub = null) {
		$this->editar($uri_super, $uri_sub, null);
	}

	function editar($uri_super = null, $uri_sub = null, $id = null) {
		$data = array();
		if($this->session->userdata('logged')){
			if (!empty($uri_super)) {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				if ($super) {
					$module = $super->menu_super.'_model';
					$this->load->model($module);
					$permissions = $this->admin_modules_model->get_permissions_user($super->id);
					if ((count($permissions) > 0) || ($this->session->userdata('ID') == $id)) {
						$data['title']   	 = $super->menu;
						$data['uri_super'] 	 = $uri_super;
						$load_view = 'admin/' . $uri_super . '/edita';
					}else{
						$load_view = 'admin/permission_denied';
					}
				}
			}

			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module =$super->menu_super.'_'.$sub->menu_sub.'_model';
					$this->load->model($module);
					$permissions = $this->admin_modules_model->get_permissions_user($sub->id);
					if ((count($permissions) > 0) || ($this->session->userdata('ID') == $id)) {
						$data['sub_title'] = $sub->menu;
						$data['uri_sub']   = $uri_sub;
						$load_view = 'admin/' . $uri_super . '/' . $uri_sub . '/edita';
					}else{
						$load_view = 'admin/permission_denied';
					}
				}
			}

			$data['menu']		 = $this->admin_modules_model->get_menu(null, 0);
			$data['pag_type']    = 'edit_content';

			if (is_null($id)) {
				$data['section'] 	 	= 'Adicionar';
				$data['action_content'] = 'insert';
			} else {
				$data['section'] 	 	= 'Editar';
				$data['action_content'] = 'update';
				$data['dado'] = $this->$module->edit($id);
			}

			switch ($uri_super) {
				case 'contact_messages':
					$this->load->model('contact_messages_model');
					$data['countries'] = $this->contact_messages_model->get_list_countries();
					break;
				case 'admin_groups':
					$data['arrIdsMenusSel'] = $this->admin_modules_model->get_ids_permissions_group($id);
					break;
				case 'admin_users':
					$this->load->model('admin_groups_model');
					$data['groups'] = $this->admin_groups_model->get_list();
					$data['menuUser'] = $this->admin_modules_model->get_menu(@$data['dado']->admin_group_id, 0);
					$data['arrIdsMenusSel'] = $this->admin_modules_model->get_ids_permissions_user($id);
					break;
				case 'subcategories':
					$this->load->model('categories_model');
					$data['categories'] = $this->categories_model->get_active();
					break;
				case 'categories':
					$this->load->model('brands_model');
					$data['brands'] = $this->brands_model->get_active();
					break;
				case 'products':
					$this->load->model('subcategories_model');
					$this->load->model('products_gallery_model');
					$this->load->model('awards_model');
					$this->load->model('products_awards_model');
					$data['subcategories'] = $this->subcategories_model->get_active();
					$data['images'] = $this->products_gallery_model->get_galeria($id);
					$data['awards'] = $this->awards_model->get_active();
					$products_awards = $this->products_awards_model->find_by_product($id);
					$data['product_awards'] = [];

					foreach ($products_awards as $product_award) {
						array_push($data['product_awards'], $product_award->award_id);
					}

					break;
				case 'distributors':
					$this->load->model('regions_model');
					$data['regions'] = $this->regions_model->get();
					break;
				case 'news':
					$this->load->model('subcategories_model');
					$data['products_categories'] = $this->subcategories_model->get_active();
					$data['newsGuns'] = $this->products_model->find_by_news($id, 'all');
					break;
				case 'safety':
					$this->load->model('safety_model');
					$data['safety_pages'] = $this->safety_model->get_active();
					break;
			}

		}else{
			$load_view = 'admin/permission_denied';
		}
		$this->load->view($load_view, $data);
	}

	function conteudo_padrao($uri_sub = null) {
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module = 'conteudo_padrao_model';
					$this->load->model($module);
					$permissions = $this->admin_modules_model->get_permissions_user($sub->id);
					if ((count($permissions) > 0) || ($this->session->userdata('ID') == $id)) {
						$data['title']     = 'Sobre';
						$data['uri_super'] = 'conteudo_padrao';
						$data['sub_title'] = $sub->menu;
						$data['uri_sub']   = $uri_sub;
						$load_view = 'admin/conteudo_padrao/' . $uri_sub . '/edita';
					}else{
						$load_view = 'admin/permission_denied';
					}
				}
			}

			$data['menu']		 	= $this->admin_modules_model->get_menu(null, 0);
			$data['pag_type']    	= 'edit_content';
			$data['section'] 	 	= 'Editar';
			$data['action_content'] = 'update_conteudo_padrao/' . $uri_sub;
			$data['dado'] 			= $this->$module->edit_module($sub->id);
		}

		$this->load->view($load_view, $data);
	}

	function update_conteudo_padrao($uri_sub = null, $id = null) {
		//$data = array();
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module = 'conteudo_padrao_model';
					$this->load->model($module);
					$this->$module->update($id);
					$this->conteudo_padrao($sub->menu_sub);
				}
			}
		}
	}

	function noticias_categoria_filtro() {
		$dados['produtos_categoria'] = $this->products_model->find_by_subcategory($this->input->post('id'));
		echo json_encode($dados['produtos_categoria']);
	}

	function insert($uri_super = null, $uri_sub = null) {
		$data = array();
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module_sub =$super->menu_super.'_'.$sub->menu_sub.'_model';
					$this->load->model($module_sub);
					$this->$module_sub->insert();
					$this->sub($super->menu_super, $sub->menu_sub);
				}
			} else if (!empty($uri_super)) {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				if ($super) {
					$module_super = $super->menu_super.'_model';
					$this->load->model($module_super);
					$this->$module_super->insert();
					$this->index($super->menu_super);
				}
			}
		}
	}

	function update($uri_super = null, $uri_sub = null, $id = null) {
		//$data = array();
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module_sub =$super->menu_super.'_'.$sub->menu_sub.'_model';
					$this->load->model($module_sub);
					$this->$module_sub->update($id);
					$this->sub($super->menu_super, $sub->menu_sub);
				}
			} else if (!empty($uri_super)) {
				$super = $this->admin_modules_model->get_module_by_super($uri_super);
				if ($super) {
					$module_super = $super->menu_super.'_model';
					$this->load->model($module_super);
					$this->$module_super->update($id);
					$this->index($super->menu_super);
				}
			}
		}
	}

	function deletar($uri_super = null, $uri_sub = null, $id = null) {
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$module = $uri_super.'_'.$uri_sub.'_model';
			} else if (!empty($uri_super)) {
				$module = $uri_super.'_model';
			}
			$this->load->model($module);
			echo $this->$module->delete($id);
		}
	}

	function remove_selected($uri_super = null, $uri_sub = null) {
		$data = array();
		if($this->session->userdata('logged')){
			$super = $this->admin_modules_model->get_module_by_super($uri_super);
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$sub = $this->admin_modules_model->get_module_by_sub($uri_sub);
				if ($sub) {
					$module =$super->menu_super.'_'.$sub->menu_sub.'_model';
				}
			} else if (!empty($uri_super)) {
				if ($super) {
					$module = $super->menu_super.'_model';
				}
			}
			$idsArray = explode(',',$this->input->post('idList'));
			$this->load->model($module);
			foreach($idsArray as $key => $objectId){
				$delete =  $this->$module->delete($objectId);
				if($delete === true){
					$data[$key] = $objectId;
				}
			}
		}
		echo json_encode($data);
	}

	function remove_img($uri_super = null, $uri_sub = null, $id = null){
		if($this->session->userdata('logged')){
			$campo = 'image';
			if ($this->uri->segment(3) == 'remove_img2'){
				$campo = 'imagem2';
			} elseif ($this->uri->segment(3) == 'remove_img3') {
				$campo = 'imagem3';
			}

			if ($this->uri->segment(4) == 'brands') {
				$campo = 'logo';
			}

			$this->db->select($campo);
        	$query = $this->db->get_where($uri_super, array('id' => $id));
        	$content = $query->row();

			if ($content->$campo) {
				if (file_exists('assets/img/content/' . $uri_super . '/' . $content->$campo)){
					unlink('assets/img/content/' . $uri_super . '/' . $content->$campo);
				}

				if (file_exists('assets/img/content/' . $uri_super . '/tn_' . $content->$campo)){
					unlink('assets/img/content/' . $uri_super . '/tn_' . $content->$campo);
				}
			}

			$data = array($campo  => '');
			$this->db->where('id' , $id);
			$this->db->update($uri_super , $data);

			if ($uri_super != 'conteudo_padrao') {
				$this->editar($uri_super, $uri_sub, $id);
			} else {
				$this->conteudo_padrao($uri_sub);
			}
		}
	}

	function remove_arq($uri_super = null, $uri_sub = null, $id = null){
		if($this->session->userdata('logged')){
			$campo = 'arquivo';

			$this->db->select($campo);
        	$query = $this->db->get_where($uri_super, array('id' => $id));
        	$content = $query->row();

			if ($content->$campo) {
				if (file_exists('assets/files/content/' . $uri_super . '/' . $content->$campo)){
					unlink('assets/files/content/' . $uri_super . '/' . $content->$campo);
				}
			}

			$data = array($campo  => '');
			$this->db->where('id' , $id);
			$this->db->update($uri_super , $data);

			$this->editar($uri_super, $uri_sub, $id);
		}
	}

	function remove_calendarios($uri_super = null, $uri_sub = null, $id = null){
		if($this->session->userdata('logged')){
			if (!empty($uri_sub) && $uri_sub != 'sem') {
				$module = $uri_super.'_'.$uri_sub.'_model';
			} else if (!empty($uri_super)) {
				$module = $uri_super.'_model';
			}
			$this->load->model($module);
			echo $this->$module->delete_calendarios($id, NULL);
		}
	}

	function get_modules_groups($id = null) {
		if($this->session->userdata('logged')){
			$result = $this->admin_modules_model->get_menu($id, 0);
			$menus = array();
			if(count($result) > 0){
				foreach ($result as $key => $row) {
					$totalSub = 0;
					$submenus = array();
					$submenus = $this->admin_modules_model->get_menu($id, $row->id);
					$totalSub = count($submenus);
					$menus[$key] = array(
						'id'              => $row->id,
						'menu_super'      => $row->menu_super,
						'menu_sub'        => $row->menu_sub,
						'menu'			  => $row->menu,
						'module_super_id' => $row->module_super_id,
						'qnt_sub'		  => $totalSub,
						'submenu'         => $submenus
					);
				}

				echo json_encode($menus);
			} else {
				echo json_encode(false);
			}
		}
	}

	function crop($modulo, $id = null){
		$dados['menu']	 = $this->admin_modules_model->get_menu(null, 0);
		if ($modulo != 'conteudo_padrao') {
			$retorno 	 = $this->admin_modules_model->get_module_by_super($modulo);
			if (!$retorno) {
				$arraySuperSub = explode('_', $modulo);
				$retorno 	 = $this->admin_modules_model->get_module_by_super_sub($arraySuperSub[0], $arraySuperSub[1]);
			}
			$modulo_tbl  = $retorno->menu_super.'_model';
			$modulo_nome = $retorno->menu;
			$modulo_menu = $retorno->menu_super;
			$this->load->model($modulo_tbl);
			$dados['dado']  = $this->$modulo_tbl->crop($id);
		} else {
			$modulo_tbl = 'conteudo_padrao_model';
			$modulo_menu = 'conteudo_padrao';
			$this->load->model($modulo_tbl);
			$dados['dado']  = $this->$modulo_tbl->crop($id);
			$dados['module'] = $this->$modulo_tbl->get_module($dados['dado']->admin_module_id);
		}
		$dados['uri_super'] = $modulo;
		$dados['title'] = 'Edi&ccedil;&atilde;o de imagem';
		$this->load->view('admin/' . $modulo_menu . '/crop', $dados);
	}
}
?>