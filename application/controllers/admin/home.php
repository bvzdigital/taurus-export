<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

    public function __construct(){
        parent::__construct();
		$this->load->model('admin_modules_model');
    }

    public function index() {		
		if($this->session->userdata('logged')){ 
			$data['menu'] 	= $this->admin_modules_model->get_menu(null, 0);
			$data['title'] 	= 'Principal';
			$this->load->view('admin/home',$data);			
		}else{ 
			$this->load->view('admin/login', array('title' => 'Login'));
		}
    }

    public function login() {
        if (!$this->session->userdata('logged')) {
            $this->load->model('Admin_Users_model','user');
            $query = $this->user->validate();
            
            if (!$query) { 
                $this->session->set_userdata('logged', false);
            } else {
                $data = array( 
                    'ID' => $query->id,
                    'EMAIL' => $query->email,
                    'NAME' => $query->name,
                    'GRUPO' => $query->admin_group_id,                    
                    'logged' => true
                );

                $this->session->set_userdata($data);
            }
        }

        $this->index('erro');
    }

    public function logout() {
		$this->session->sess_destroy();		
        redirect(base_url().'admin/home/login');
    }

    public function senha() {
        $this->load->model('Admin_Users_model','user');
        $data['nova_senha'] = $this->user->nova_senha();           

        $data['title'] = 'Esqueci minha senha';
        $this->load->view('admin/senha', $data);   
    }
}
?>