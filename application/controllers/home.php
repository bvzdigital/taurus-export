<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->l_load('home');
        $this->lang->l_load('products');

        $this->data['title'] = 'Taurus Export';
        $this->data['description'] = '';

        $this->load->model('institutional_contents_model');
        $this->load->model('featured_contents_model');
        $this->load->model('categories_model');
        $this->load->model('safety_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['safety_pages'] = $this->safety_model->get_active();
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
        $this->load->model('modals_model');
        $this->load->model('news_model');
    }

    public function index()
    {
        $this->data['all_news'] = array();
        $this->data['text'] = $this->institutional_contents_model->find_by_page('home')->text;
        $this->data['text_line'] = $this->institutional_contents_model->find_by_page('home')->text_line;
        $this->data['featured_contents'] = $this->featured_contents_model->get_active();
        $this->data['all_news'] = $this->news_model->get_active();

        $this->data['banners'] = $this->featured_contents_model->get_active();

        $modals = $this->modals_model->get_modal();        

        if(count($modals) > 1) {
            $com_data = false;
            foreach ($modals as $modal) {
                //se tiver mais de um pega o primeiro com data
                if($modal->start){
                    $this->data['modal'] = $modal;
                    $com_data = true;
                    break;
                }
            }
            //se não tiver com data pega o primeiro
            if(!$com_data){
                $this->data['modal'] = $modals[0];
            }
        } else {
            //se não tiver mais de um
            $this->data['modal'] = (isset($modals[0]))? $modals[0] : false;
        }

        $this->template('home', $this->data);
    }

}