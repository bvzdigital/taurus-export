<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contact extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->l_load('contact');

        $this->data['title'] = 'Taurus Export - '.lang('contact.contact');
        $this->data['description'] = '';

        $this->load->model('categories_model');
        $this->load->model('contact_messages_model');
        $this->load->model('newsletter_registrations_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function index()
    {
        $this->data['countries'] = $this->contact_messages_model->get_list_countries();

        $this->template('contact/home', $this->data);
    }

    public function create()
    {
        $response = new stdclass;

        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Invalid email address.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';
            
            echo json_encode($response);

            return false;
        }

        if (empty($this->input->post('name'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Inform your name.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);
            
            return false;
        }

        if (empty($this->input->post('phone'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Inform your phone number.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if (empty($this->input->post('region'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Inform your location.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if (empty($this->input->post('country'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Inform your country.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if (empty($this->input->post('message'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Leave a message.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if($this->contact_messages_model->insert()){
            $response->status = 1;
            $response->mensagem = lang('contact.enviado_com_sucesso').'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            $res = $this->send_mail('contact');

            echo json_encode($response);
        }
        else {
            $response->status = 0;
            $response->mensagem = lang('contact.falha_no_envio').'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);
        }
    }

    public function create_news()
    {
        $response = new stdclass;

        if (!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Invalid email address.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if (empty($this->input->post('name'))) {
            $response->status = 0;
            $response->mensagem = '<p style="color: red; text-transform: uppercase;">Inform your name.</p>'.'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact.enviar_outra').'</a>';

            echo json_encode($response);

            return false;
        }

        if($this->newsletter_registrations_model->insert()){
            $response->status = 1;
            $response->mensagem = lang('contact_news.enviado_com_sucesso').'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact_news.enviar_outra').'</a>';

            // $res = $this->send_mail('news');

            echo json_encode($response);
        }
        else {
            $response->status = 0;
            $response->mensagem = lang('contact_news.falha_no_envio').'<br><a class="js-form-clear-message js-form-reset-fields">'.lang('contact_news.enviar_outra').'</a>';

            echo json_encode($response);
        }
    }

    private function send_mail($tipo = NULL)
    {
        $this->load->library('email');

        $this->email->from($this->input->post('email'), $this->input->post('name'));

        $data['pagina'] = site_url('contact');
        $data['admin'] = base_url('admin');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['region'] = $this->input->post('region');
        $data['country'] = $this->input->post('country');
        $data['message'] = $this->input->post('message');

        if ($tipo == 'contact'){
            $this->email->subject('Contato via Taurus Export');
            switch($data['region']) {
                case 'europa':
                    $this->email->to('gustavo.coronet@taurus.com.br');
                break;
                case 'america-latina':
                    $this->email->to('andre.azambuja@taurus.com.br');
                break;
                case 'africa':
                    $this->email->to('emerson.souza@taurus.com.br');
                break;
                case 'oriente-medio':
                    $this->email->to('emerson.souza@taurus.com.br');
                break;
                case 'asia':
                    $this->email->to('leonardo.sperry@taurus.com.br');
                break;
                case 'oceania':
                    $this->email->to('leonardo.sperry@taurus.com.br');
                break;
            }
            $mensagem = $this->load->view('email/contato', $data, TRUE);
        } else if ($tipo == 'news') {
            $this->email->subject('Cadastro news Taurus Export');
            $this->email->to('paula.cavalheiro@taurus.com.br');
            $mensagem = $this->load->view('email/news', $data, TRUE);
        }

        $this->email->message($mensagem);

        $this->email->send();
    }

}