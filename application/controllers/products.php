<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class products extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->load('products');

        $this->data['title'] = 'Taurus Export';
        $this->data['description'] = '';

        $this->load->model('products_model');
        $this->load->model('products_gallery_model');
        $this->load->model('awards_model');
        $this->load->model('categories_model');
        $this->load->model('subcategories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function show($category_slug, $subcategory_slug, $slug)
    {
        $this->data['category'] = $this->categories_model->find_by_slug($category_slug);
        $this->data['subcategory'] = $this->subcategories_model->find_by_slug($category_slug, $subcategory_slug);
        $this->data['product'] = $this->products_model->find_by_slug($slug);
        $this->data['awards'] = $this->awards_model->find_by_product($this->data['product']->id);

        $this->data['product']->gallery =
            $this->products_gallery_model->get_galeria($this->data['product']->id);

        $this->data['title'] = $this->data['product']->name.' - '.$this->data['subcategory']->title.' - '.$this->data['category']->title.' - Taurus Export';
        $this->template('products/show', $this->data);
    }

    public function download($slug)
    {
        $this->load->library('zip');
        $slug = $this->uri->segment(5);
        $product = $this->products_model->find_by_slug($slug);
        $images = $this->products_gallery_model->get_galeria($product->id);
        foreach ($images as $lista) {
            $this->zip->read_file("assets/img/content/products/".$lista->url);
        }

        $this->zip->download($slug.'.zip');
    }

    public function all()
    {
        $this->data['category'] = $this->categories_model->get_site();
        $this->template('products/all', $this->data);
    }
}