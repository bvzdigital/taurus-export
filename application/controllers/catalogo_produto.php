<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class catalogo_produto extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->load->helper('path');
    }

    public function index()
    {
        $this->data['title'] = 'Product Catalog | Taurus Export';
        $this->data['description'] = 'Encontre aqui todos os nossos catalgos de produto';
        $this->data['meta_url'] = 'https://www.taurusarmas.com.br/pt/catalogo-produto';

        $this->data['schema'] = $this->getSchema();

        $this->data['catalogos'] = [
            [
                'title' => '2019 Catalog',
                'url' => "https://taurusarmas.com.br/assets/catalogo-produtos/Taurus-Catalogo-Exp-2019-TATICO_Duplo_baixa.pdf",
                'thumb' =>  base_url().'assets/img/thumb-2019.jpg'
            ],
            [
                'title' => '2020 Catalog',
                'url' => "https://taurusarmas.com.br/assets/catalogo-produtos/Taurus-Catalogo-Exp-2020-duplo-baixa.pdf",
                'thumb' => base_url().'assets/img/thumb-2020.jpg'
            ]
        ];

        $this->template('catalogo_produto/index', $this->data);
    }
    
    public function getSchema()
    {
        return [
            "@context" => "http://schema.org",
            "@type" => "Organization",
            "url" => "https://www.taurusarmas.com.br",
            "name" => "Taurus Armas S.A.",
            "description" => "A Taurus é uma empresa estratégica de defesa, com 80 anos de história e sólida reputação. Atualmente atua nos segmentos de armas, capacetes e M.I.M. (metal injection molding).",
            "logo" => base_url() . "assets/img/logo-schema.png",
            "address" => [
                "@type" => "PostalAddress",
                "streetAddress" => "Avenida São Borja, n° 2181, Prédio A, Fazenda São Borja - São Leopoldo",
                "addressLocality" => "Rio Grande do Sul",
                "postalCode" => "93035-411",
                "addressCountry" => "BR"
            ],
            "contactPoint" => [
                [
                    "@type" => "ContactPoint",
                    "contactType" => "Atendimento ao Cliente",
                    "telephone" => "+55 (51) 3081-7900",
                    "email" => "sac@taurus.com.br"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Recepção",
                    "telephone" => "+55 (51) 3021-3000"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Assistência Técnica",
                    "telephone" => "+55 (51) 3021-3132"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Suprimentos",
                    "telephone" => "+55 (51) 3021-3142"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Recursos Humanos",
                    "telephone" => "+55 (51) 3021-3103"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Financeiro",
                    "telephone" => "+55 (51) 3021-3047"
                ], [
                    "@type" => "ContactPoint",
                    "contactType" => "Marketing",
                    "telephone" => "+55 (51) 3021-3081"
                ]
            ]
        ];
    }
}