<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends MY_Controller {

	private $data;

	function __construct()
    {
		parent::__construct();
        $this->data['uri_set'] = set_uris();
		$this->lang->l_load('home');
        $this->lang->l_load('search');

        $this->data['title'] = 'Taurus Export - '.lang('search.search_results');
        $this->data['description'] = '';

        $this->load->helper('form');
        $this->load->helper('distributors');

        $this->load->model('products_gallery_model');
        $this->load->model('subcategories_model');
        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
	}

	public function index()
    {
        $this->load->model('distributors_model');
        $this->load->model('institutional_contents_model');
        $this->load->model('products_model');
        $this->load->model('downloads_model');
        $this->load->model('subcategories_model');
        $this->load->model('featured_contents_model');
        $this->load->model('awards_model');

        $this->loadSearchData();

        $this->sliceResultTexts($this->data['termo'], $this->data['products'], 'description', false);

        $this->highlightResults($this->data['termo'], $this->data['distributors_taurus'], 'name');
        $this->highlightResults($this->data['termo'], $this->data['distributors_rossi'], 'name');
        $this->highlightResults($this->data['termo'], $this->data['categories'], 'title');
        $this->highlightResults($this->data['termo'], $this->data['subcategories'], 'title');
        $this->highlightResults($this->data['termo'], $this->data['products'], 'name');
        $this->highlightResults($this->data['termo'], $this->data['downloads'], 'name');

        $this->template('search', $this->data);
    }

    private function highlightResults($pattern, $data, $col_name)
    {
    	foreach($data as $val){
    		$str = $this->highlight($pattern, $val->$col_name);
    		$val->$col_name = $str;
    	}
    }

    private function sliceResultTexts($pattern, $data, $col_name, $has_tags)
    {
    	foreach($data as $val){
    		$aux = $this->sliceText($pattern, $val->$col_name, 0, 255, $has_tags);
    		$val->$col_name = $aux;
    	}
    }

    private function highlight($pattern, $text)
    {
        $op_tag = '<span class="media-box__highlight">';
        $cl_tag = '</span>';
        $replace = $op_tag . $pattern . $cl_tag;

        //suppress warnings in case of unescaped characters missed by preg_quote
        @preg_match_all('/'.preg_quote($pattern, '/', '-').'+/i', $text, $matches);

        if (is_array($matches[0]) && count($matches[0]) >= 1) {
            foreach ($matches[0] as $match) {
                $text = str_replace($match, $op_tag.$match.$cl_tag, $text);
            }
        }

        return $text;
    }

    private function sliceText($pattern, $text, $start, $length, $has_tags)
    {
    	$position = 0;
    	$pattern_len = 0;
    	$slice_indicator = '';
        $formatted_text = str_replace('</strong><br />', '. ', $text);
        $stripped_text = strip_tags($formatted_text);

    	if(!empty($pattern)){
    		$position = stripos($stripped_text, $pattern);
    		$pattern_len = strlen($pattern);
    	}

    	if($position > $length){
    		$difference = $position - $length;
    		$shift = $difference + $pattern_len;
    		$start += $shift;
    		$length += $shift;
    		$slice_indicator = '[...] ';
    	}

    	$sliced_text = $slice_indicator . substr($stripped_text, $start, $length) . ' [...]';

    	return $sliced_text;
    }

    private function loadSearchData()
    {
    	$this->data['termo'] = $this->input->post('search');

    	$distributors = $this->distributors_model->search($this->data['termo']);
        $this->data['distributors_rossi'] = [];
        $this->data['distributors_taurus'] = [];

        foreach ($distributors as $distributor) {
            if ($distributor->brand == 'Taurus') {
                $this->data['distributors_taurus'][] = $distributor;
            } elseif($distributor->brand == 'Rossi') {
                $this->data['distributors_rossi'][] = $distributor;
            }
        }

        $this->data['products'] = $this->products_model->search($this->data['termo']);
        $this->data['categories'] = $this->categories_model->search($this->data['termo']);
        $this->data['subcategories'] = $this->subcategories_model->search($this->data['termo']);
        $this->data['downloads'] = $this->downloads_model->search($this->data['termo']);

        foreach ($this->data['products'] as $product) {
            $product->gallery = $this->products_gallery_model->get_galeria($product->id);
            $product->subcategory = $this->subcategories_model->edit($product->subcategory_id);
            $product->category = $this->categories_model->edit($product->subcategory->category_id);
        }

        foreach ($this->data['subcategories'] as $subcategory) {
            $subcategory->category = $this->categories_model->edit($subcategory->category_id);
        }

    	$this->data['mensagem'] = '';
    	$this->data['resultados'] = '';

    	$resultados = sizeof($this->data['distributors_taurus']) +
                      sizeof($this->data['distributors_rossi']) +
    				  sizeof($this->data['products']) +
                      sizeof($this->data['categories']) +
                      sizeof($this->data['subcategories']) +
                      sizeof($this->data['downloads']);

    	if($resultados > 0 && $resultados < 10){
    		$this->data['resultados'] = str_pad($resultados, 2, '0', STR_PAD_LEFT);
    	}
    	elseif($resultados >= 10) {
    		$this->data['resultados'] = (string) $resultados;
    	}

    }

}
