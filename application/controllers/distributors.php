<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class distributors extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->load('distributors');

        $this->data['title'] = 'Taurus Export - Dealers';
        $this->data['description'] = 'Taurus Dealers around the world.';

        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->load->helper('countries');
    }

    public function index()
    {
        $countries = getCountryArray();

        $this->load->model('distributors_model');
        $this->load->model('regions_model');

        $this->data['actualRegion'] = $this->uri->segment(3);
        $actualBrand = $this->uri->segment(2);
        $p = explode('-', $actualBrand);
        $this->data['actualBrand'] = $p[1];

        $this->data['regions'] = $this->regions_model->get_site();

        $distributors = $this->distributors_model->get_site(ucfirst($this->data['actualBrand']));

        foreach($distributors as $dist) {
            $this->data['distributors'][$dist->country][] = $dist;
            $dist->country_sigla = array_search($dist->country, $countries);

            if( array_search($dist->country, $countries) == false ) {
                $dist->country_sigla = $dist->country;
            }
        }

        $this->template('distributors/index', $this->data);
    }
}