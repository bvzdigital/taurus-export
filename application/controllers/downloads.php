<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class downloads extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->l_load('downloads');

        $this->data['title'] = 'Taurus Export - '.lang('downloads.Downloads');
        $this->data['description'] = '';

        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function index()
    {

        $this->load->model('downloads_model');
        $this->data['downloads'] = $this->downloads_model->get();

        $this->template('downloads/index', $this->data);
    }

}