<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class categories extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->load('categories');

        $this->data['title'] = 'Taurus Export';
        $this->data['description'] = '';

        $this->load->model('products_model');
        $this->load->model('products_gallery_model');
        $this->load->model('subcategories_model');
        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function show($slug,  $sub_slug = NULL)
    {
        $this->data['category'] = $this->categories_model->find_by_slug($slug);
        $this->data['subcategories'] = ($sub_slug)? $this->subcategories_model->find_by_slug($this->data['category']->slug, $sub_slug) : $this->subcategories_model->find_by_category($this->data['category']->id);
        
        $this->data['subcategories'] = $this->sortSubcategories($this->data['subcategories'], $slug);

        if(is_array($this->data['subcategories'])){
            foreach ($this->data['subcategories'] as $key => $sub) {
                $this->popule_products($sub);
            }            
        } else {
            $this->popule_products($this->data['subcategories']);
        }

        $this->data['title'] = ($sub_slug && !is_array($this->data['subcategories']))? $this->data['category']->title.' - '.$this->data['subcategories']->title.' - Taurus Export' : $this->data['category']->title.' - Taurus Export';
        $this->template('categories/show', $this->data);
    }

    private function popule_products($sub)
    {
        $sub->products = $this->products_model->find_by_subcategory($sub->id);
        foreach ($sub->products as $product) {
            $product->gallery = $this->products_gallery_model->get_galeria($product->id);
        }
    }

    private function sortSubcategories($subcategories, $slug) 
    {
        $customOrder = [
            'pistols' => ['G3', 'G3C', 'G2C', 'TS Series', 'TH Series', 'Metallic', '92', '57', '1911 series', 'G2 series'],
            'revolvers' => ['Large Frame', 'Medium Frame', 'Small Frame', '856', 'Raging Hunter', 'Judge'],
            'law-enforcement' => ['T4 Series', 'Submachine Guns', 'Carabines', 'Shotguns']
        ];

        $order = $customOrder[$slug];

        usort($subcategories, function ($a, $b) use ($order) {
            $pos_a = array_search(trim($a->title), $order);
            $pos_b = array_search(trim($b->title), $order);
            return $pos_a - $pos_b;
        });

        return $subcategories;
    }
}