<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class institutional extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->l_load('about_the_company');

        $this->data['title'] = 'Taurus Export - '.lang('about_the_company.about');
        $this->data['description'] = '';

        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function index()
    {
        $this->load->model('institutional_contents_model');

        $this->data['content'] = $this->institutional_contents_model->find_by_page('about');
        $this->data['content']->image = base_url()
            .'assets/img/content/institutional_contents/'
            .$this->data['content']->image;

        $this->template('institutional/index', $this->data);
    }

}