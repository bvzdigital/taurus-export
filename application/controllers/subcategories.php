<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class subcategories extends MY_Controller {

    private $data = array();

    public function __construct() {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->load('subcategories');

        $this->data['title'] = 'Taurus Export';
        $this->data['description'] = '';

        $this->load->model('products_model');
        $this->load->model('products_gallery_model');
        $this->load->model('subcategories_model');
        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
        $this->data['categories_rossi'] = $this->categories_model->find_by_brand('Rossi');
    }

    public function show($category_slug, $slug)
    {
        $this->data['category'] = $this->categories_model->find_by_slug($category_slug);
        $this->data['subcategory'] = $this->subcategories_model->find_by_slug($this->data['category']->slug, $slug);

        $this->data['products'] = $this->products_model->find_by_subcategory($this->data['subcategory']->id);

        foreach ($this->data['products'] as $product) {
            $product->gallery = $this->products_gallery_model->get_galeria($product->id);
        }

        $this->data['title'] = $this->data['category']->title.' - '.$this->data['subcategory']->title.' - Taurus Export';
        $this->template('subcategories/show', $this->data);
    }

}