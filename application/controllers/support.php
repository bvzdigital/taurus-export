<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class support extends MY_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();
        $this->lang->l_load('downloads');

        $this->data['title'] = 'Taurus Export - Support';
        $this->data['description'] = 'Taurus is giving you many information and promotional stuff in these pages.';

        $this->load->model('categories_model');
        $this->load->model('frequently_asked_questions_model');
        $this->load->model('downloads_model');

        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
    }

    public function faq()
    {
        $this->data['faq'] = $this->frequently_asked_questions_model->get_active();
        $this->data['title'] = 'Taurus Export - Support | FAQ';
        $this->template('support/faq', $this->data);
    }

    public function media()
    {
        $this->data['title'] = 'Taurus Export - Support | Media';
        $this->data['media_downloads'] = $this->downloads_model->find_by_type('media');
        $this->template('support/media', $this->data);
    }

    public function spareParts()
    {
        $this->data['title'] = 'Taurus Export - Support | Spare Parts';
        $this->template('support/spareParts', $this->data);
    }

    public function trademark()
    {
        $this->data['title'] = 'Taurus Export - Support | Trademark';
        $this->data['trademark_downloads'] = $this->downloads_model->find_by_type('trade-mark');
        $this->template('support/trademark', $this->data);
    }
}