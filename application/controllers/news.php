<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news extends MY_Controller {

    //private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->data['uri_set'] = set_uris();

        $this->data['title'] = 'Taurus Export | News & Updates';
        $this->data['description'] = '';
        $this->load->model('news_model');

        $this->load->model('products_model');
        $this->load->model('products_gallery_model');
        $this->load->model('subcategories_model');
        $this->load->model('categories_model');
        $this->data['categories_taurus'] = $this->categories_model->find_by_brand('Taurus');
    }

    public function index()
    {
        $this->data['all_news'] = array();
        $this->data['news_list'] = array();
        
        $this->data['all_news'] = $this->news_model->get_site();
        $this->data['news_list'] = $this->news_model->get_news_guns();

        $this->template('news/index', $this->data);
    }

    public function detalhe($slug) {
        $thisNews = base_url() . $slug;

        $this->data['news'] = $this->news_model->find_by_slug($slug);
        $this->data['related_guns'] = $this->products_model->find_by_news($this->data['news']->id);

        foreach ($this->data['related_guns'] as $gun) {
            $subcategoryArray = $this->subcategories_model->find_by_ID($gun->subcategory_id);
            $gun->subcategory_slug = $subcategoryArray[0]->slug;
        }

        $newsTitle = $this->data['news']->title;
        $description = strip_tags($this->data['news']->description);

        $this->data['share_urls']['facebook'] = 'http://www.facebook.com/sharer.php?u='.$thisNews;
        $this->data['share_urls']['wpp'] = 'https://api.whatsapp.com/send?text='.$thisNews;
        $this->data['share_urls']['twitter'] = 'https://twitter.com/intent/tweet/?url='.$thisNews;
        $this->data['share_urls']['linkedin'] = 'https://www.linkedin.com/shareArticle?mini=true&url='.urlencode($thisNews).'&title='.urlencode($newsTitle).'&summary='.urlencode($description).'&source='.urlencode("Taurus Export | News");

        $this->template('news/detalhe', $this->data);
    }

    public function armas() {
        $this->data['news'] = $this->news_model->find_by_gun($this->input->post('id'));
        echo json_encode($this->data['news']);
    }

    public function page($page_number = 1, $type = null)
    {   
        $limit = 9;
        $offset = $page_number > 1 ? ($page_number-1)*$limit : 0;

        $this->data['page'] = 'news';
        $this->data['all_news'] = $this->news_model->find_for_pagination($limit, $offset);

        $config['page_query_string'] = false;
        $config['use_page_numbers']  = false;
        $config['num_links']         = 3;
        $config['first_url']         = site_url().'news';
        $config['first_link']        = false;
        $config['last_link']         = false;
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';
        $config['cur_tag_open']      = '<li class="active">';
        $config['cur_tag_close']     = '</li>';
        $config['prev_tag_open']     = '<li class="previous">';
        $config['prev_tag_close']    = '</li>';
        $config['next_tag_open']     = '<li class="next">';
        $config['next_tag_close']    = '</li>';
        $config['prev_link']         = 'Anterior';
        $config['next_link']         = 'Próximo';
        $config['base_url']          = site_url().'news/page/';
        $config['uri_segment']       = 3;
        $config['total_rows']        = $this->news_model->count();
        $config['per_page']          = $limit;

        $this->pagination->initialize($config);
        $this->data['pagination_links'] = $this->pagination->create_links();

        if ($type == 'json') {
            echo json_encode($this->data['all_news']);
        } else {
            $this->template('news/index', $this->data);
        }
    }
}