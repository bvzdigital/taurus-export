<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//function pr($a){ echo '<pre>', print_r($a,1) ,'</pre>'; }

class MY_Controller extends CI_Controller {

   public $js_default = array(    
		'jquery-1.7.1.min.js',
		'jquery.scrollTo-1.4.2.js',
		'fancybox/jquery.fancybox-1.3.4.pack.js',
   );   


	
   public $end_js = 'main.js';
   public $css_default = array(
   	'css/normalize.css', 
   	'css/main.css',
	'js/fancybox/jquery.fancybox-1.3.4.css',  );

   public $js_code = array();
   public $metatags;

	function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
	}

   function model($name){
		$name = $name;
		if(!isset($this->{$name})) $this->load->model($name);
		return $this->{$name};
	}

	function template($content, $data){

		$data['scripts'] = $this->get_js();
		$data['css'] = $this->get_css();
		$data['login_cookie'] = unserialize(get_cookie('glocal_login'));
		$this->load->view('header',$data);
		$this->load->view($content,$data);

		$brow = $this->agent->browser();
		$ver = $this->agent->version(); 
		/*
		if($brow == 'Internet Explorer' && $ver == '7.0'){
	
			$this->load->view('footer_ie');
		}else{
		
			$this->load->view('footer');
		}
		*/
		$this->load->view('footer');
		
	}
	
	

	function stripAccents($str, $enc = 'UTF-8'){
		$accents = array(
			'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
			'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
			'C' => '/&Ccedil;/',
			'c' => '/&ccedil;/',
			'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
			'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
			'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
			'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
			'N' => '/&Ntilde;/',
			'n' => '/&ntilde;/',
			'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
			'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
			'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
			'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
			'Y' => '/&Yacute;/',
			'y' => '/&yacute;|&yuml;/',
			'a.' => '/&ordf;/',
			'o.' => '/&ordm;/'
		);

        return preg_replace($accents, array_keys($accents), htmlentities($str, ENT_NOQUOTES, $enc));
	}

   function add_js($js_file){
      $this->js_default[] = trim($js_file);
   }

   function add_css($css_file){
   	$this->css_default[] = trim($css_file);
   }

   function get_css(){
      $links = array();
      $time = (DEVELOPMENT_STAGE)? '?' . time() : '';
      foreach($this->css_default as $css_file){
         $css_file = (preg_match('/^(http:\/\/)/',$css_file)) ? $css_file : base_url() . 'assets/'. $css_file;
         $media = (preg_match('/(print\.)/',$css_file))? 'print' : 'all';
   		$links[] = '<link href="'. $css_file . $time.'" rel="stylesheet" type="text/css" media="'. $media .'" />';
      }

         return join(PHP_EOL,$links);

   }

   function get_js(){
      $scripts = array();
      $time = time();

      $this->js_default[] = $this->end_js;
      if(!empty($this->js_code)){
         array_unshift($this->js_default, get_class($this) . '.js');
         write_file('assets/js/'. get_class($this) . '.js', join(PHP_EOL,$this->js_code));
      }

      foreach($this->js_default as $js_file){
 	      $js_file = (preg_match('/^(http:\/\/)/',$js_file)) ? $js_file : base_url() . 'assets/js/'. $js_file;
         $glue = (DEVELOPMENT_STAGE)? (preg_match('/\?/',$js_file))? '&' : '?' . $time : '';
   		$scripts[] = '<script src="'. $js_file . $glue .'" type="text/javascript"></script>';
      }

    
         $ret = join(PHP_EOL,$scripts);
      
      return $ret;
   }

   function add_js_code($code){
      $this->js_code[] = trim($code);
   }

   function add_metatag($name,$content){
      foreach(func_get_args() as $arg){
          if(empty($arg)) return false;
      }
      $this->metatags[] = array($name => $content);
   }

   function metatags(){
      $meta = array();
		if(!empty($this->metatags)){
		   foreach($this->metatags as $metatag){
		   	if(is_array($metatag)){
				   foreach($metatag as $name => $content){
				      $meta[] = '<meta name="'. $name .'" content="'. $content .'" />';
				   }
		      }
		   }
      }
      return join(PHP_EOL,$meta);

   }

	


}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */

